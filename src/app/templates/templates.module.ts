import { TemplatesRoutingModule } from './templates-routing.module';
import { ClientsComponent } from './clients/clients.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
    MatFormFieldModule, MatTableModule,
    MatDialogModule, MatSelectModule, MatInputModule,
    MatAutocompleteModule, MatDatepickerModule
} from '@angular/material';
import { ClientsService } from '../services/clients.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NewClientPanelComponent } from './clients/new-client-panel/new-client-panel.component';
import { RepresentativesPanelComponent } from './clients/representatives-panel/representatives-panel.component';
import { NewRepresentativePanelComponent } from './clients/representatives-panel/new-representative-panel/new-representative-panel.component';
import { TemplatesPanelComponent } from './templates-panel/templates-panel.component';
import { NewTemplateComponent } from './templates-panel/new-template/new-template.component';
import { SharedModule } from '../shared/shared.module';
import { TemplatesComponent } from './templates.component';
import { TaskTemplatesPanelComponent } from './task-templates-panel/task-templates-panel.component';
import { NewTaskTemplateComponent } from './task-templates-panel/new-task-template/new-task-template.component';

@NgModule({
    declarations: [
        ClientsComponent,
        NewClientPanelComponent,
        RepresentativesPanelComponent,
        NewRepresentativePanelComponent,
        TemplatesPanelComponent,
        TemplatesComponent,
        TaskTemplatesPanelComponent,
        NewTemplateComponent,
        NewTaskTemplateComponent,
    ],
    imports: [
        CommonModule,
        TemplatesRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        FontAwesomeModule,
        SharedModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatAutocompleteModule,
        FormsModule,
        MatDatepickerModule,
        MatTableModule
    ],
    providers: [ClientsService],
    entryComponents: [
        NewClientPanelComponent,
        RepresentativesPanelComponent,
        NewRepresentativePanelComponent,
        NewTaskTemplateComponent,
        NewTemplateComponent],
    bootstrap: []
})
export class TemplatesModule { }

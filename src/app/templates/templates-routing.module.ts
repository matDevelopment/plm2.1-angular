import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientsComponent } from './clients/clients.component';
import { RolesModel } from '../models/roles-model';
import { AuthGuard } from '../guards/auth.guard';
import { TemplatesComponent } from './templates.component';

const appRoutes: Routes = [
    { path: 'clients', component: ClientsComponent,
    data: { roles: [RolesModel.Admin, RolesModel.ProjectManager]}, canActivate: [AuthGuard]},
    { path: 'templates', component: TemplatesComponent,
     data: { roles: [RolesModel.Admin, RolesModel.ProjectManager]}, canActivate: [AuthGuard]}
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TemplatesRoutingModule {  }

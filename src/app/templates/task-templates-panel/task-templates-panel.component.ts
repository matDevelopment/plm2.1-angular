import { Component, OnInit, OnDestroy } from '@angular/core';
import { TemplateModel } from 'src/app/models-database/template.model';
import { TemplateService } from 'src/app/services/template.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { faPlus, faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { NewTaskTemplateComponent } from './new-task-template/new-task-template.component';
import { TaskTemplateModel } from 'src/app/models-database/task-template.model';

@Component({
  selector: 'app-task-templates-panel',
  templateUrl: './task-templates-panel.component.html',
  styleUrls: ['./task-templates-panel.component.scss']
})
export class TaskTemplatesPanelComponent implements OnInit, OnDestroy {

  templates: TaskTemplateModel[] = [];

  constructor(private templateService: TemplateService, private dialog: MatDialog) { }

  dataSource: MatTableDataSource<TaskTemplateModel>;

  faPlus = faPlus; faTrash = faTrashAlt;
  faEdit = faEdit;

  displayedColumns: string[] = ['Name', 'TaskName', 'Case', 'Description', 'TimeEstimated', 'TaskTagMain',
  'TaskTagAccessory', 'TaskTagAccessoryMinor', 'Edit', 'Remove'];

  newTemplateComponent = NewTaskTemplateComponent;
  addTemplateSubscription = new Subscription();
  editTemplateSubscription = new Subscription();

  ngOnInit() {
    this.getTemplates();
    this.onTemplateAdd();
    this.onTemplateEdit();
  }

  getTemplates() {
    this.templateService.GetTaskTemplates().subscribe(
      templates => { this.templates = templates.filter(template => template.CaseId != null);
        this.dataSource = new MatTableDataSource(this.templates); }
    );
  }

  onTemplateAdd() {
    this.addTemplateSubscription = this.templateService.AddTaskTemplateSubject.subscribe(
      temp => { this.templates.push(temp); this.dataSource.data = this.templates;
      });
  }

  onTemplateEdit() {
    this.editTemplateSubscription = this.templateService.EditTaskTeamplateSubject.subscribe(
      temp => {
        this.templates[this.templates.findIndex(t => t.Id === temp.Id)] = temp;
        this.dataSource.data = this.templates;
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addTemplate() {
    this.dialog.open(this.newTemplateComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '80vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'new'}
    });
  }

  editTemplate(template: TemplateModel) {
    this.dialog.open(this.newTemplateComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '80vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'edit', Template: template }
    });
  }

  removeTemplate(template: TaskTemplateModel) {
    this.templateService.RemoveTaskTemplate(template).subscribe(
      () => { this.templates.splice(this.templates.indexOf(template), 1),
         this.dataSource.data = this.templates; }
    );
  }

  ngOnDestroy(): void {
    this.addTemplateSubscription.unsubscribe();
    this.editTemplateSubscription.unsubscribe();
  }
}

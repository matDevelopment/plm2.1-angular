import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ClientsService } from 'src/app/services/clients.service';
import { FilterService } from 'src/app/services/filter.service';
import { TagService } from 'src/app/services/tag.service';
import { TemplateService } from 'src/app/services/template.service';
import { NewTemplateComponent } from '../../templates-panel/new-template/new-template.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TemplateModel } from 'src/app/models-database/template.model';
import { Subscription } from 'rxjs';
import { TaskTemplateModel } from 'src/app/models-database/task-template.model';
import { CaseService } from 'src/app/services/case.service';
import { TagModel } from 'src/app/models-database/tag.model';

@Component({
  selector: 'app-new-task-template',
  templateUrl: './new-task-template.component.html',
  styleUrls: ['./new-task-template.component.scss']
})
export class NewTaskTemplateComponent implements OnInit, OnDestroy {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private clientService: ClientsService,
    private filterService: FilterService, private tagService: TagService, private templateService: TemplateService,
    private newTemplateDialogRef: MatDialogRef<NewTemplateComponent>, private caseService: CaseService) { }

  NewTemplateForm: FormGroup;
  newTemplate: TaskTemplateModel = new TaskTemplateModel();
  tagSubscription = new Subscription();
  caseSubscription = new Subscription();
  representativeSubscription = new Subscription();
  prioritySubscription = new Subscription();
  statusSubscription = new Subscription();
  templates = [] as TemplateModel[];
  filteredTemplates: Promise<TemplateModel[]>;
  showCasePicker = true;
  caseTagParentId;

  alertMessage = '';

  ngOnInit() {
    this.initForm();
    this.getTemplates();
    this.changeTags();
    this.changePriority();
    this.changeCase();
  }

  initForm() {
    if (this.data.Type !== 'edit') {
      this.NewTemplateForm = new FormGroup({
        'name': new FormControl(null, Validators.required),
        'taskName': new FormControl(null, Validators.required),
        'case': new FormControl(null),
        'template': new FormControl(null),
        'description': new FormControl(null, Validators.required),
        'timeEstimated': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
      if (this.data.CaseTemplate != null) {
        this.showCasePicker = false;
        this.NewTemplateForm.controls['template'].setValue(this.data.CaseTemplate.Name);
        this.caseTagParentId = this.data.CaseTemplate.CaseTagParentId;
        this.newTemplate.TemplateId = this.data.CaseTemplate.Id;
      }
    } else {
      this.newTemplate = this.data.Template;
      this.NewTemplateForm = new FormGroup({
        'name': new FormControl(this.data.Template.Name, Validators.required),
        'taskName': new FormControl(this.data.Template.TaskName, Validators.required),
        'template': new FormControl(null),
        'case': new FormControl(null),
        'description': new FormControl(this.data.Template.Description, Validators.required),
        'timeEstimated': new FormControl(this.data.Template.TimeEstimated, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
      this.initTemplateTask();
    }
  }

  initTemplateTask() {
    if (this.data.Template.Case == null) {
      this.showCasePicker = false;
      this.caseTagParentId = this.data.Template.Template.CaseTagParentId;
      this.newTemplate.TemplateId = this.data.Template.Template.Id;
      this.NewTemplateForm.controls['template'].setValue(this.data.Template.Template.Name);
    } else {
      this.NewTemplateForm.controls['case'].setValue(this.data.Template.Case.Name);
      this.caseTagParentId = this.newTemplate.Case.TemplateId;
    }
  }

  changePriority() {
    this.prioritySubscription = this.filterService.prioritySubject.subscribe(
      priority => this.newTemplate.PriorityId = priority.Id
    );
  }

  getTemplates() {
    this.templateService.GetTemplates().subscribe(
      res => {
        this.templates = res;
        this.filteredTemplates = new Promise((resolve, reject) => {
          resolve(this.templates);
        });
      }
    );
  }

  onFilterTemplates(name: string) {
    this.filteredTemplates = new Promise((resolve, reject) => {
      resolve(this.templates.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  changeCase() {
    this.caseSubscription = this.caseService.PickNewCaseSubject.subscribe(caseModel => {
      this.newTemplate.Case = caseModel, this.newTemplate.CaseId = caseModel.Id;
      this.tagService.addTagsToCaseSubject.next({ ParentTagId: caseModel.CaseTagParentId, ChildTagId: null });
    });
  }

  changeTags() {
    this.tagSubscription = this.tagService.addTagsToTaskSubject.subscribe(
      tags => {
        tags.MainTag !== null ? this.newTemplate.TaskTagMainId = tags.MainTag.Id : this.newTemplate.TaskTagMainId = null;
        tags.AccessoryTag !== null ? this.newTemplate.TaskTagAccessoryId = tags.AccessoryTag.Id :
          this.newTemplate.TaskTagAccessoryId = null;
        tags.AccessoryMinorTag !== null ? this.newTemplate.TaskTagAccessoryMinorId = tags.AccessoryMinorTag.Id
          : this.newTemplate.TaskTagAccessoryMinorId = null;
      }
    );
  }

  onSubmit() {
    this.createTemplate();
    if (this.checkCase(this.newTemplate)) {
      if (this.data.Type !== 'edit') {
        this.templateService.AddTaskTemplate(this.newTemplate).subscribe(temp => {
          alert('Szablon Dodany');
          this.newTemplateDialogRef.close();
          this.templateService.AddTaskTemplateSubject.next(temp);
        });
      } else {
        this.templateService.EditTaskTemplate(this.newTemplate).subscribe(temp => {
          alert('Szablon Edytowany');
          this.newTemplateDialogRef.close();
          this.templateService.EditTaskTeamplateSubject.next(temp);
        });
      }
    } else {
      alert(this.alertMessage);
    }

  }

  createTemplate() {
    this.newTemplate.Name = this.NewTemplateForm.get('name').value;
    this.newTemplate.TaskName = this.NewTemplateForm.get('taskName').value;
    this.newTemplate.TimeEstimated = this.NewTemplateForm.get('timeEstimated').value;
    this.newTemplate.NameCount = 0;
    this.data.Type !== 'edit' ? this.newTemplate.Id = 0 : this.newTemplate.Id = this.data.Template.Id;
  }

  checkCase(newTemplate: TaskTemplateModel): boolean {
    let allow = true;
    this.alertMessage = '';
    if (!this.NewTemplateForm.get('name').valid) {
      allow = false;
      this.alertMessage += 'Zła nazwa; ';
    }
    if (newTemplate.PriorityId === null || newTemplate.PriorityId === undefined) {
      allow = false;
      this.alertMessage += 'Brak Priotytetu; ';
    }
    if (!this.NewTemplateForm.get('timeEstimated').valid) {
      this.alertMessage += 'Zły Czas Przewidywany: ';
      allow = false;
    }
    if (newTemplate.TaskTagMainId === null || newTemplate.TaskTagMainId === undefined) {
      this.alertMessage += 'Brak Etapu Zadania; ';
      allow = false;
    }
    return allow;
  }

  onClose() {
    this.newTemplateDialogRef.close();
  }

  ngOnDestroy(): void {
    this.tagSubscription.unsubscribe();
    this.caseSubscription.unsubscribe();
    this.prioritySubscription.unsubscribe();
    this.statusSubscription.unsubscribe();
    this.representativeSubscription.unsubscribe();
  }
}

import { ClientModel } from './../../models-database/client.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { ClientsService } from 'src/app/services/clients.service';
import { faPlus, faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NewClientPanelComponent } from './new-client-panel/new-client-panel.component';
import { RepresentativesPanelComponent } from './representatives-panel/representatives-panel.component';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {

  constructor(private clientService: ClientsService, private dialog: MatDialog) { }

  faPlus = faPlus;
  faTrash = faTrash;
  faEdit = faEdit;
  newClientComponent = NewClientPanelComponent;
  representativesComponent = RepresentativesPanelComponent;

  @ViewChild(MatSort) sort: MatSort;

  clients: ClientModel[] = [];
  displayedColumns: string[] = ['Name', 'Email', 'PhoneNumber', 'Nip', 'Remove', 'Edit'];
  dataSource: MatTableDataSource<ClientModel>;
  ngOnInit() {
    this.clientService.GetClients().subscribe(
      clients => {
        this.clients = clients;
        this.dataSource = new MatTableDataSource(this.clients);
      }
    );
    this.clientService.AddClientSubject.subscribe(
      client => {this.clients.push(client); this.dataSource = new MatTableDataSource(this.clients); }
    );
    this.clientService.EditClientSubject.subscribe(
      client => { this.clients[this.clients.indexOf(this.clients.find(c => c.Id === client.Id))] = client;
        console.log(this.clients);
        this.dataSource = new MatTableDataSource(this.clients);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addClient() {
    this.dialog.open(this.newClientComponent, {
      width: '60%',
      maxWidth: '100vw',
      height: '40vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'new'}
    });
  }

  editClient(client) {
    this.dialog.open(this.newClientComponent, {
      width: '60%',
      maxWidth: '100vw',
      height: '40vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'edit', Client: client}
    });
  }

  removeClient(client: ClientModel) {
    this.clientService.RemoveClient(client).subscribe(
      res => {this.clients.splice(this.clients.indexOf(client), 1); this.dataSource = new MatTableDataSource(this.clients); },
      error => alert('Błąd! Upewnij się że klient nie jest przypisany do żadnego projektu')
    );
  }

  showRepresentatives(client: ClientModel) {
    this.dialog.open(this.representativesComponent, {
      width: '75%',
      maxWidth: '100vw',
      height: '60vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'edit', Client: client}
    });
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ClientsService } from 'src/app/services/clients.service';
import { NewClientPanelComponent } from '../../new-client-panel/new-client-panel.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RepresentativeModel } from 'src/app/models-database/representative.model';

@Component({
  selector: 'app-new-representative-panel',
  templateUrl: './new-representative-panel.component.html',
  styleUrls: ['./new-representative-panel.component.scss']
})
export class NewRepresentativePanelComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private clientService: ClientsService,
  private newClientDialogRef: MatDialogRef<NewClientPanelComponent>) { }

  NewClientForm;

  ngOnInit() {
    switch (this.data.Type) {
      case 'new' :
      this.NewClientForm = new FormGroup({
        'name' : new FormControl({value: null, disabled: false}, Validators.required),
        'surname' : new FormControl({value: null, disabled: false}, [Validators.required]),
        'email' : new FormControl({value: null, disabled: false}, [Validators.required, Validators.email]),
        'phone' : new FormControl({disabled: false},
          [Validators.required, Validators.pattern('^[0-9]*$')]),
      });
      break;
      case 'edit' :
      this.NewClientForm = new FormGroup({
        'name' : new FormControl({value: this.data.Representative.Name, disabled: false}, Validators.required),
        'surname' : new FormControl({value: this.data.Representative.Surname, disabled: false}, [Validators.required]),
        'email' : new FormControl({value: this.data.Representative.Email, disabled: false}, [Validators.required, Validators.email]),
        'phone' : new FormControl({value: this.data.Representative.Phone, disabled: false},
          [Validators.required, Validators.pattern('^[0-9]*$')]),
      });
      break;
    }
  }

  addClient() {
    const newRepresentative = new RepresentativeModel();
    newRepresentative.Name = this.NewClientForm.get('name').value;
    newRepresentative.Surname = this.NewClientForm.get('surname').value;
    newRepresentative.Email = this.NewClientForm.get('email').value;
    newRepresentative.Phone = this.NewClientForm.get('phone').value;
    newRepresentative.ClientId = this.data.Client.Id;
    switch (this.data.Type) {
      case 'new' :
        this.clientService.AddRepresentative(newRepresentative).subscribe(
          representative => { this.clientService.AddRepresentativeSubject.next(representative); this.newClientDialogRef.close(); }
        );
      break;
      case 'edit' :
      newRepresentative.Id = this.data.Representative.Id;
      this.clientService.EditRepresentative(newRepresentative).subscribe(
        representative => { this.clientService.EdtitRepresentativeSubject.next(representative);  this.newClientDialogRef.close(); }
      );
      break;
    }
  }

}

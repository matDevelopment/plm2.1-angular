import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ClientsService } from 'src/app/services/clients.service';
import { MatDialog, MatSort, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { faPlus, faTrash, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NewClientPanelComponent } from '../new-client-panel/new-client-panel.component';
import { ClientModel } from 'src/app/models-database/client.model';
import { RepresentativeModel } from 'src/app/models-database/representative.model';
import { NewRepresentativePanelComponent } from './new-representative-panel/new-representative-panel.component';

@Component({
  selector: 'app-representatives-panel',
  templateUrl: './representatives-panel.component.html',
  styleUrls: ['./representatives-panel.component.scss']
})
export class RepresentativesPanelComponent implements OnInit {

  constructor(private clientService: ClientsService, private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) private data: any) { }

  faPlus = faPlus;
  faTrash = faTrash;
  faEdit = faEdit;
  NewRepresentativePanel = NewRepresentativePanelComponent;

  @ViewChild(MatSort) sort: MatSort;

  representatives: RepresentativeModel[];
  displayedColumns: string[] = ['Name', 'Email', 'PhoneNumber', 'Nip', 'Remove', 'Edit'];
  dataSource: MatTableDataSource<RepresentativeModel>;
  ngOnInit() {
    this.clientService.GetRepresentatives(this.data.Client.Id).subscribe(
      representatives => {
        this.representatives = representatives;
        this.dataSource = new MatTableDataSource(this.representatives);
      }
    );
    this.clientService.AddRepresentativeSubject.subscribe(
      client => {this.representatives.push(client); this.dataSource = new MatTableDataSource(this.representatives); }
    );
    this.clientService.EdtitRepresentativeSubject.subscribe(
      representative => {
        this.representatives[this.representatives.indexOf(this.representatives.find(c => c.Id === representative.Id))] = representative;
        this.dataSource = new MatTableDataSource(this.representatives);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addRepresentative() {
    this.dialog.open(this.NewRepresentativePanel, {
      width: '60%',
      maxWidth: '100vw',
      height: '40vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'new', Client: this.data.Client}
    });
  }

  editClient(representative) {
    this.dialog.open(this.NewRepresentativePanel, {
      width: '60%',
      maxWidth: '100vw',
      height: '40vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'edit', Representative: representative, Client: this.data.Client }
    });
  }

  removeClient(representative: RepresentativeModel) {
    this.clientService.RemoveRepresentative(representative).subscribe(
      res => {
        this.representatives.splice(this.representatives.indexOf(representative), 1);
        this.dataSource = new MatTableDataSource(this.representatives);
      }
    );
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ClientsService } from 'src/app/services/clients.service';
import { ClientModel } from 'src/app/models-database/client.model';

@Component({
  selector: 'app-new-client-panel',
  templateUrl: './new-client-panel.component.html',
  styleUrls: ['./new-client-panel.component.scss']
})
export class NewClientPanelComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private clientService: ClientsService,
  private newClientDialogRef: MatDialogRef<NewClientPanelComponent>) { }

  NewClientForm;

  ngOnInit() {
    switch (this.data.Type) {
      case 'new' :
      this.NewClientForm = new FormGroup({
        'name' : new FormControl({value: null, disabled: false}, Validators.required),
        'email' : new FormControl({value: null, disabled: false}, [Validators.required, Validators.email]),
        'phoneNumber' : new FormControl({value: null, disabled: false},
          [Validators.required, Validators.pattern('^[0-9]*$')]),
        'nip' : new FormControl({disabled: true}, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
      break;
      case 'edit' :
      this.NewClientForm = new FormGroup({
        'name' : new FormControl({value: this.data.Client.Name, disabled: false}, Validators.required),
        'email' : new FormControl({value: this.data.Client.Email, disabled: false}, [Validators.required, Validators.email]),
        'phoneNumber' : new FormControl({value: this.data.Client.PhoneNumber, disabled: false},
          [Validators.required, Validators.pattern('^[0-9]*$')]),
        'nip' : new FormControl({value: this.data.Client.Nip, disabled: false}, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
      break;
    }
  }

  addClient() {
    const newClient = new ClientModel();
    newClient.Id = 0;
    newClient.Name = this.NewClientForm.get('name').value;
    newClient.Email = this.NewClientForm.get('email').value;
    newClient.PhoneNumber = this.NewClientForm.get('phoneNumber').value;
    newClient.Nip = this.NewClientForm.get('nip').value;
    switch (this.data.Type) {
      case 'new' :
        console.log(newClient);
        this.clientService.AddClient(newClient).subscribe(
          client => { this.clientService.AddClientSubject.next(client); this.newClientDialogRef.close(); }
        );
      break;
      case 'edit' :
      newClient.Id = this.data.Client.Id;
      this.clientService.EditClient(newClient).subscribe(
        client => { this.clientService.EditClientSubject.next(client);  this.newClientDialogRef.close(); }
      );
      break;
    }
  }

}

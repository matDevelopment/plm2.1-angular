import { Component, OnInit, OnDestroy } from '@angular/core';
import { TemplateService } from 'src/app/services/template.service';
import { TemplateModel } from 'src/app/models-database/template.model';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { faPlus, faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { NewTemplateComponent } from './new-template/new-template.component';
import { Subscription } from 'rxjs';
import { NewTaskTemplateComponent } from '../task-templates-panel/new-task-template/new-task-template.component';
import { TablesService } from 'src/app/services/tables.service';

@Component({
  selector: 'app-templates-panel',
  templateUrl: './templates-panel.component.html',
  styleUrls: ['./templates-panel.component.scss']
})
export class TemplatesPanelComponent implements OnInit, OnDestroy {

  templates: any[] = [];

  constructor(private templateService: TemplateService, private dialog: MatDialog, private tableService: TablesService) { }

  dataSource: MatTableDataSource<TemplateModel>;

  faPlus = faPlus; faTrash = faTrashAlt;
  faEdit = faEdit;

  tableObjects: string[] = ['Name', 'CaseName', 'Description', 'TimeEstimated', 'CaseTagParentName',
    'CaseTagChildName', 'faEdit', 'faTrash', 'faPlus'];
  tableNames: string[] = ['Nazwa', 'Nazwa Projektu', 'Opis', 'Czas Przewidywany', 'Typ Projektu', 'Rodzaj Projektu', 'Edytuj',
    'Usuń', 'Dodaj'];

  tableSubObjects: string[] = ['Name', 'TaskName', 'TaskTagMainName', 'TaskTagAccessoryName',
    'TaskTagAccessoryMinorName', 'TimeEstimated', 'faEdit', 'faTrash'];
  tableSubNames = [
    'Nazwa Szablonu', 'Nazwa Zadania', 'Typ Zadania', 'Etap Zadania', 'Etap Podzadania', 'Czas Przewidywany', 'Edytuj', 'Usuń'];

  newTemplateComponent = NewTemplateComponent;
  addTemplateSubscription = new Subscription();
  addTemplateTableSubscription = new Subscription();
  editTemplateSubscription = new Subscription();

  removeTemplateSubscription = new Subscription();
  addTaskTemplateSubscription = new Subscription();
  removeTaskTemplateSubscription = new Subscription();

  ngOnInit() {
    this.getTemplates();
    this.onTemplateAdd();
    this.onTemplateEdit();
    this.addTemplateTableSubscription = this.tableService.addSubject.subscribe(
      res => this.addTaskTemplate(res)
    );
    this.editTemplateSubscription = this.tableService.editSubject.subscribe(
      res => this.editTemplate(res)
    );
    this.onTaskTemplateAdd();
    this.onTemplateRemove();
    this.onTaskTemplateRemove();
  }

  getTemplates() {
    this.templateService.GetTemplates().subscribe(
      templates => {
        this.templates = templates;
        this.templates.forEach(temp => {
          this.prepareTemplateForTable(temp);
        });

      }
    );
  }

  prepareTemplateForTable(temp) {
    temp.CaseTagParentName = temp.CaseTagParent.Name;
    temp.CaseTagChildName = temp.CaseTagChildId != null ? temp.CaseTagChild.Name : '';
    if (temp.TaskTemplates != null) {
      temp.TaskTemplates.forEach(taskTemp => {
        this.prepareTaskTemplateForTable(taskTemp);
      });
    }
  }

  prepareTaskTemplateForTable(taskTemp) {
    taskTemp.TaskTagMainName = taskTemp.TaskTagMain != null ? taskTemp.TaskTagMain.Name : '';
    taskTemp.TaskTagAccessoryName = taskTemp.TaskTagAccessory != null ? taskTemp.TaskTagAccessory.Name : '';
    taskTemp.TaskTagAccessoryMinorName = taskTemp.TaskTagAccessoryMinor != null ? taskTemp.TaskTagAccessoryMinor.Name : '';
  }

  onTaskTemplateAdd() {
    this.addTaskTemplateSubscription = this.templateService.AddTaskTemplateSubject.subscribe(taskTemplate => {
      const mainTemplate = this.templates.find(t => t.Id === taskTemplate.TemplateId);
      if (mainTemplate != null) {
        this.prepareTaskTemplateForTable(taskTemplate);
        mainTemplate.TaskTemplates.push(taskTemplate);
      }
    });
  }

  onTaskTemplateRemove() {
    this.removeTaskTemplateSubscription = this.tableService.subRemoveSubject.subscribe(temp =>
      this.templateService.RemoveTaskTemplate(temp).subscribe(() => {
        const taskTemplates = this.templates.find(t => t.Id === temp.TemplateId).TaskTemplates;
        taskTemplates.splice(taskTemplates.findIndex(t => t.Id === temp.Id), 1);
      }));
  }

  onTemplateAdd() {
    this.addTemplateSubscription = this.templateService.AddTemplateSubject.subscribe(
      temp => this.templates.push(temp));
  }

  onTemplateRemove() {
    this.removeTemplateSubscription = this.tableService.removeSubject.subscribe(temp =>
      this.templateService.RemoveTemplate(temp).subscribe(res => {
        this.templates.splice(this.templates.findIndex(t => t.Id === temp.Id), 1);
      },
        () => alert('Błąd! Szablon jest używany przez projekt')
      )
    );
  }

  onTemplateEdit() {
    this.editTemplateSubscription = this.templateService.EditTeamplateSubject.subscribe(
      temp => {
        this.prepareTemplateForTable(temp);
        this.templates[this.templates.findIndex(t => t.Id === temp.Id)] = temp;
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addTemplate() {
    this.dialog.open(this.newTemplateComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '80vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'new' }
    });
  }

  editTemplate(template: TemplateModel) {
    this.dialog.open(this.newTemplateComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '80vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'edit', Template: template }
    });
  }

  ngOnDestroy(): void {
    this.addTemplateSubscription.unsubscribe();
    this.editTemplateSubscription.unsubscribe();
    this.removeTaskTemplateSubscription.unsubscribe();
    this.removeTemplateSubscription.unsubscribe();
    this.addTemplateTableSubscription.unsubscribe();
  }

  addTaskTemplate(data) {
    this.dialog.open(NewTaskTemplateComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '80vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'new', CaseTemplate: data  }
    });
  }

}

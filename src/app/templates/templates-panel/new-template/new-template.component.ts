import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TemplateModel } from 'src/app/models-database/template.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Subscription } from 'rxjs';
import { ClientsService } from 'src/app/services/clients.service';
import { FilterService } from 'src/app/services/filter.service';
import { TagService } from 'src/app/services/tag.service';
import { TemplateService } from 'src/app/services/template.service';
import { template } from '@angular/core/src/render3';

@Component({
  selector: 'app-new-template',
  templateUrl: './new-template.component.html',
  styleUrls: ['./new-template.component.scss']
})
export class NewTemplateComponent implements OnInit, OnDestroy {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private clientService: ClientsService,
    private filterService: FilterService, private tagService: TagService, private templateService: TemplateService,
    private newTemplateDialogRef: MatDialogRef<NewTemplateComponent>) { }

  NewTemplateForm: FormGroup;
  newTemplate: TemplateModel = new TemplateModel();
  tagSubscription = new Subscription();
  clientSubscription = new Subscription();
  representativeSubscription = new Subscription();
  prioritySubscription = new Subscription();
  statusSubscription = new Subscription();

  alertMessage = '';

  ngOnInit() {
    if (this.data.Type !== 'edit') {
      this.NewTemplateForm = new FormGroup({
        'name': new FormControl(null, Validators.required),
        'caseName': new FormControl(null, Validators.required),
        'order': new FormControl(null, Validators.required),
        'client': new FormControl(null, Validators.required),
        'representative': new FormControl(null),
        'description': new FormControl(null, Validators.required),
        'timeEstimated': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
    } else {
      this.newTemplate = this.data.Template;
      this.NewTemplateForm = new FormGroup({
        'name': new FormControl(this.data.Template.Name, Validators.required),
        'caseName': new FormControl(this.data.Template.CaseName, Validators.required),
        'order': new FormControl(this.data.Template.Order, Validators.required),
        'client': new FormControl(this.data.Template.Client.Name, Validators.required),
        'representative': new FormControl(null),
        'description': new FormControl(this.data.Template.Description, Validators.required),
        'timeEstimated': new FormControl(this.data.Template.TimeEstimated, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
    }
    this.changeRepresentative();
    this.changeTags();
    this.changeClient();
    this.changePriority();
  }

  changeRepresentative() {
    this.representativeSubscription = this.clientService.AddRepresentativeSubject.subscribe(
      representative => this.newTemplate.RepresentativeId = representative.Id
    );
  }

  changeClient() {
    this.clientSubscription = this.clientService.AddClientSubject.subscribe(
      client => this.newTemplate.ClientId = client.Id
    );
  }

  changePriority() {
    this.prioritySubscription = this.filterService.prioritySubject.subscribe(
      priority => this.newTemplate.PriorityId = priority.Id
    );
  }

  changeTags() {
    this.tagSubscription = this.tagService.addTagsToCaseSubject.subscribe(
      tags => { this.newTemplate.CaseTagParentId = tags.ParentTagId; this.newTemplate.CaseTagChildId = tags.ChildTagId; }
    );
  }


  onSubmit() {
    this.createTemplate();
    if (this.checkCase(this.newTemplate)) {
      this.data.Type !== 'edit' ?
        this.templateService.AddTemplate(this.newTemplate).subscribe(temp => {
          this.newTemplateDialogRef.close();
          this.templateService.AddTemplateSubject.next(temp);
        }) :
        this.templateService.EditTemplate(this.newTemplate).subscribe(temp => {
          this.newTemplateDialogRef.close();
          console.log(temp);
          this.templateService.EditTeamplateSubject.next(temp);
        });
    } else {
      alert(this.alertMessage);
    }
  }

  createTemplate() {
    this.newTemplate.Name = this.NewTemplateForm.get('name').value;
    this.newTemplate.CaseName = this.NewTemplateForm.get('caseName').value;
    this.newTemplate.TimeEstimated = this.NewTemplateForm.get('timeEstimated').value;
    this.newTemplate.Order = this.NewTemplateForm.get('order').value;
    this.data.Type !== 'edit' ? this.newTemplate.CasesCount = 0 : this.newTemplate.CasesCount = this.newTemplate.CasesCount;
    this.data.Type !== 'edit' ? this.newTemplate.Id = 0 : this.newTemplate.Id = this.data.Template.Id;
  }

  checkCase(newTemplate: TemplateModel): boolean {
    let allow = true;
    this.alertMessage = '';
    if (!this.NewTemplateForm.get('order').valid) {
      allow = false;
      this.alertMessage += 'Zły numer projektu: ';
    }
    if (newTemplate.PriorityId === null || newTemplate.PriorityId === undefined) {
      allow = false;
      this.alertMessage += 'Brak Priotytetu; ';
    }
    if (!this.NewTemplateForm.get('timeEstimated').valid) {
      this.alertMessage += 'Zły Czas Przewidywany: ';
      allow = false;
    }
    if (newTemplate.ClientId === null || newTemplate.ClientId === undefined) {
      this.alertMessage += 'Brak Klineta; ';
      allow = false;
    }
    if (newTemplate.CaseTagParentId === null || newTemplate.CaseTagParentId === undefined) {
      allow = false;
      this.alertMessage += 'Brak Typu Pracowni; ';
    }
    return allow;
  }

  onClose() {
    this.newTemplateDialogRef.close();
  }

  ngOnDestroy(): void {
    this.tagSubscription.unsubscribe();
    this.clientSubscription.unsubscribe();
    this.prioritySubscription.unsubscribe();
    this.statusSubscription.unsubscribe();
    this.representativeSubscription.unsubscribe();
  }
}

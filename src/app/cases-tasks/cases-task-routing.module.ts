import { CasesComponent } from './cases/cases.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TasksComponent } from './tasks/tasks.component';
import { RolesModel } from '../models/roles-model';
import { AuthGuard } from '../guards/auth.guard';
import { ForgeDirectorySettingsComponent } from './forge/forge-directory-settings/forge-directory-settings.component';

const appRoutes: Routes = [
    { path: 'cases/:count', component: CasesComponent,
    data: { roles: [RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader, RolesModel.User]}, canActivate: [AuthGuard] },
    { path: 'tasks/:count', component: TasksComponent,
    data: { roles: [RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader, RolesModel.User]}, canActivate: [AuthGuard] },
    { path: 'forge-directory-settings', component: ForgeDirectorySettingsComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class CasesTasksRoutingModule {  }

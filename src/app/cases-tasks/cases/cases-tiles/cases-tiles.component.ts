import { Component, OnInit, Input } from '@angular/core';
import { faUserAlt, faPlus, faClone, faFile, faPlusSquare,
        faEllipsisH, faExclamation, faStar, faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { MatDialog} from '@angular/material';
import { CaseModel } from '../../../models-database/case.model';
import { DescriptionComponent } from '../../../shared/description/description.component';

@Component({
  selector: 'app-cases-tiles',
  templateUrl: './cases-tiles.component.html',
  styleUrls: ['./cases-tiles.component.scss']
})
export class CasesTilesComponent implements OnInit {

  faUserAlt = faUserAlt;
  faPlus = faPlus;
  faCopy = faClone;
  faFile = faFile;
  faEllipsisH = faEllipsisH;
  faPlusSquare = faPlusSquare;
  faExclamation = faExclamation;
  faCertificate = faStar;
  buttonExpanded = false;
  faChevronRight = faChevronRight;
  faChevronLeft = faChevronLeft;
  shownStart = 0;
  shownEnd = 9;
  constructor(private dialog: MatDialog) { }

  @Input() Cases: CaseModel[] = [];

  ngOnInit() {
  }

  showDescriptionDialog(caseModel: CaseModel) {
    const dialogRef = this.dialog.open(DescriptionComponent, {
      width: '40%',
      data: { Description: caseModel.Description, Name: caseModel.Name  }
    });
  }

  addPage() {
    this.shownStart += 9;
    this.shownEnd += 9;
  }

  decresePage() {
    if ( this.shownStart > 0) {
      this.shownStart -= 9;
      this.shownEnd -= 9;
    }
  }

}

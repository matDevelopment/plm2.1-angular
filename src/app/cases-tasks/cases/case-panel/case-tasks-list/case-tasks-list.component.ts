import { Component, OnInit, Input, OnDestroy, Inject, AfterViewInit } from '@angular/core';
import { TaskModel } from 'src/app/models-database/task.model';
import { TaskService } from 'src/app/services/task.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FilterModel } from 'src/app/models/filter.model';
import { Subscription } from 'rxjs';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-case-tasks-list',
  templateUrl: './case-tasks-list.component.html',
  styleUrls: ['./case-tasks-list.component.scss']
})
export class CaseTasksListComponent implements OnInit, OnDestroy {

  @Input() Tasks: TaskModel[] = [];
  @Input() CaseId;
  @Input() TasksType = false;

  NewTasks: TaskModel[] = [];
  InProgressTasks: TaskModel[] = [];
  EndedTasks: TaskModel[] = [];
  editTaskSubscription = new Subscription();
  RemoveTaskSubscription = new Subscription();
  tagSubscription = new Subscription();
  tasksCopy: TaskModel[] = [];
  filterModel = new FilterModel();
  constructor(private taskService: TaskService, private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
    private caseDialogRef: MatDialogRef<CaseTasksListComponent>, private tagService: TagService) { }

  ngOnInit() {
    this.filterModel.CaseId = this.CaseId;
    this.getFilteredTasks();
    this.editTask();
    this.removeTask();
    this.tagSubscription = this.tagService.addTagsToTaskSubject.subscribe(
      tag => {
        tag.MainTag !== null ? this.filterModel.TaskTagMainId = tag.MainTag.Id : this.filterModel.TaskTagMainId = null;
        tag.AccessoryTag !== null ? this.filterModel.TaskTagAccessoryId = tag.AccessoryTag.Id : this.filterModel.TaskTagAccessoryId = null;
        tag.AccessoryMinorTag !== null ?
          this.filterModel.TaskTagAccessoryMinorId = tag.AccessoryMinorTag.Id : this.filterModel.TaskTagAccessoryMinorId = null;
        this.getFilteredTasks();
      });
  }

  getFilteredTasks() {
    this.NewTasks = []; this.InProgressTasks = []; this.EndedTasks = [];
    this.taskService.GetFilteredTasks(this.filterModel).subscribe(
      tasks => { this.Tasks = tasks; this.divideTasks(); }
    );
  }

  divideTasks() {
    this.NewTasks = [];
    this.InProgressTasks = [];
    this.EndedTasks = [];
    this.Tasks.forEach(
      task => {
        switch (task.Status.Id) {
          case 1:
            this.NewTasks.push(task);
            break;
          case 2:
            this.InProgressTasks.push(task);
            break;
          case 3:
            this.EndedTasks.push(task);
            break;
        }
      });
    this.sendEnded();
    this.sendInProgress();
    this.sendNew();
  }

  editTask() {
    this.taskService.EdtiTaskSubject.subscribe(
      task => {
        this.Tasks[this.Tasks.indexOf(task)] = task;
        this.divideTasks();
      }
    );
  }

  addTask() {
    this.taskService.AddNewTaskSubject.subscribe(
      task => {
        this.Tasks.push(task);
        this.divideTasks();
      }
    );
  }

  sendNew() {
    this.taskService.ChangeTaskTableSubjec.next({ Tasks: this.NewTasks, Type: 'New' });
  }

  sendInProgress() {
    this.taskService.ChangeTaskTableSubjec.next({ Tasks: this.InProgressTasks, Type: 'InProgress' });
  }

  sendEnded() {
    this.taskService.ChangeTaskTableSubjec.next({ Tasks: this.EndedTasks, Type: 'Ended' });
  }

  removeTask() {
    this.RemoveTaskSubscription = this.taskService.RemoveTaskSubject.subscribe(
      task => {
        switch (task.Status.Id) {
          case 1:
            this.removeFromArray(task, this.NewTasks);
            break;
          case 2:
            this.removeFromArray(task, this.InProgressTasks);
            break;
          case 3:
            this.removeFromArray(task, this.EndedTasks);
            break;
        }
      }
    );
    this.sendEnded();
    this.sendInProgress();
    this.sendNew();
  }

  removeFromArray(task: TaskModel, array: TaskModel[]) {
    array.forEach(
      taskModel => {
        if (task.Id === taskModel.Id) {
          array.splice(array.indexOf(taskModel), 1);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.editTaskSubscription.unsubscribe();
    this.RemoveTaskSubscription.unsubscribe();
    this.tagSubscription.unsubscribe();
  }

}

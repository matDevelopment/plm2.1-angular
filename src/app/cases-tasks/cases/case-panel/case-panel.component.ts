import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {
  faUserAlt, faPlus, faClone, faFile, faPlusSquare,
  faEllipsisH, faExclamation, faStar, faEdit, faTrash, faAngleDown, faAngleUp, faLayerGroup, faCopy, faCalculator
}
  from '@fortawesome/free-solid-svg-icons';
import { TaskModel } from 'src/app/models-database/task.model';
import { NewCaseComponent } from '../new-case/new-case.component';
import { Subscription } from 'rxjs';
import { CaseService } from 'src/app/services/case.service';
import { NewMemoComponent } from '../../memos/new-memo/new-memo.component';
import { RolesModel } from 'src/app/models/roles-model';
import { AuthService } from 'src/app/services/auth.service';
import { MemoListComponent } from '../../memos/memo-list/memo-list.component';
import { LastSeenService } from 'src/app/services/last-seen.service';
import { LastSeenModel } from 'src/app/models-database/last-seen.model';
import { Router } from '@angular/router';
import { TemplateService } from 'src/app/services/template.service';
import { NewTaskComponent } from '../../tasks/new-task/new-task.component';
import { ForgeComponent } from '../../forge/forge.component';

@Component({
  selector: 'app-case-panel',
  templateUrl: './case-panel.component.html',
  styleUrls: ['./case-panel.component.scss']
})
export class CasePanelComponent implements OnInit, OnDestroy {

  faUserAlt = faUserAlt;
  faPlus = faPlus;
  faFile = faFile;
  faCopy = faCopy;
  faEllipsisH = faEllipsisH;
  faPlusSquare = faPlusSquare;
  faExclamation = faExclamation;
  faCertificate = faStar;
  faCalculator = faCalculator;
  faEdit = faEdit;
  faTrash = faTrash;
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faLayerGroup = faLayerGroup;
  Tasks: TaskModel[] = [];
  rolesModel = RolesModel;
  casePanelComponent = CasePanelComponent;
  memoListComponent = MemoListComponent;
  EditCaseSubscription = new Subscription();
  tasksType = false;

  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any, public authService: AuthService,
    private caseDialogRef: MatDialogRef<CasePanelComponent>, private caseService: CaseService, private lastSeenService: LastSeenService,
    private router: Router, public templateService: TemplateService) { }

  newTaskComponent = NewTaskComponent;
  newMemoComponent = NewMemoComponent;
  forgeViewerComponent = ForgeComponent;

  ngOnInit() {
    this.onEditCase();
    this.addLastSeen();
  }

  addLastSeen() {
    const lastSeen = new LastSeenModel();
    lastSeen.CaseId = this.data.Case.Id;
    lastSeen.TaskId = null;
    lastSeen.UserId = this.authService.decodedToken.nameid;
    this.lastSeenService.AddLastSeen(lastSeen).subscribe(() => console.log('success'));
  }

  editCase() {
    this.dialog.open(NewCaseComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '98vw',
      data: { Case: this.data.Case, Type: 'edit' }
    });
  }

  removeCase() {
    this.caseService.RemoveCase(this.data.Case).subscribe(
      caseModel => {
        this.caseService.RemoveCaseSubject.next(caseModel);
        this.caseDialogRef.close();
      }
    );
  }

  onEditCase() {
    this.EditCaseSubscription = this.caseService.EdtiCaseSubject.subscribe(
      editedCase => this.data.Case = editedCase
    );
  }


  ngOnDestroy(): void {
    this.router.navigateByUrl('/cases/0');
    this.EditCaseSubscription.unsubscribe();
  }

  copyCaseToTemplate() {
    this.templateService.CopyCaseToTemplate(this.data.Case).subscribe(
      res => alert('Szablon został zapisany'),
      err => alert(err)
    );
  }
}

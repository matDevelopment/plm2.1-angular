import { DateService } from './../../../services/date.service';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { CaseModel } from 'src/app/models-database/case.model';
import { CaseService } from 'src/app/services/case.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { TagService } from 'src/app/services/tag.service';
import { NewGroupComponent } from 'src/app/groups-users/groups/new-group/new-group.component';
import { Subscription } from 'rxjs';
import { ClientsService } from 'src/app/services/clients.service';
import { FilterService } from 'src/app/services/filter.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { GroupService } from 'src/app/services/group.service';
import { RepresentativeModel } from 'src/app/models-database/representative.model';
import { TemplateService } from 'src/app/services/template.service';
import { TagModel } from 'src/app/models-database/tag.model';
import { faTimes, faCalculator } from '@fortawesome/free-solid-svg-icons';
import { TemplateModel } from 'src/app/models-database/template.model';
import { NewTaskComponent } from '../../tasks/new-task/new-task.component';

@Component({
  selector: 'app-new-case',
  templateUrl: './new-case.component.html',
  styleUrls: ['./new-case.component.scss'],
})

export class NewCaseComponent implements OnInit, OnDestroy {

  constructor(private caseService: CaseService, private dateService: DateService, private tagService: TagService,
    @Inject(MAT_DIALOG_DATA) public data: any, private newCaseDialogRef: MatDialogRef<NewGroupComponent>,
    private clientService: ClientsService, private userService: UserService, private groupService: GroupService,
    private filterService: FilterService, private templateService: TemplateService, private dialog: MatDialog,) { }

  newCase: CaseModel = new CaseModel();
  alertMessage = '';
  faTimes = faTimes;
  employesNr = 1;
  hoursForOneEmploye = 1;
  faCalculator = faCalculator;
  pickedTemplate: TemplateModel = null;

  tagSubscription = new Subscription();
  clientSubscription = new Subscription();
  representativeSubscription = new Subscription();
  prioritySubscription = new Subscription();
  statusSubscription = new Subscription();
  userSubscription = new Subscription();
  groupSubscription = new Subscription();
  templateSubscription = new Subscription();

  NewCaseForm: FormGroup;

  ngOnInit() {
    this.changeTags();
    this.changeClient();
    this.changePriority();
    this.changeStatus();
    this.changeUser();
    this.changeGroup();
    this.changeRepresentative();
    this.createForm();
    this.changeTemplate();
  }

  editCase() {
    if (this.checkCase(this.newCase)) {
      this.createCase();
      this.caseService.EditCase(this.newCase).subscribe(
        editedCase => {
          alert('Projekt został edytowany');
          this.caseService.EdtiCaseSubject.next(editedCase);
          this.newCaseDialogRef.close();
          }
      );
    } else {
      alert(this.alertMessage);
    }
  }

  checkCase(newCase: CaseModel): boolean {
    let allow = true;
    this.newCase.StatusId = 1;
    this.alertMessage = '';
    if (newCase.PriorityId === null || newCase.PriorityId === undefined) {
      allow = false;
      this.alertMessage += 'Brak Priotytetu; ';
    }
    if (this.NewCaseForm.get('startDate').value === null || this.NewCaseForm.get('startDate').value === undefined ||
    this.NewCaseForm.get('startDate').value.getTime() >=  this.NewCaseForm.get('endDate').value.getTime()) {
      this.alertMessage += 'Zła Data; ';
      allow = false;
    }
    if (this.NewCaseForm.get('endDate').value === null || this.NewCaseForm.get('endDate').value === undefined) {
      this.alertMessage += 'Zła Data Końca; ';
      allow = false;
     }
    if ( (newCase.UserId === null || newCase.UserId === undefined) && (newCase.GroupId === null || newCase.UserId === undefined)) {
      this.alertMessage += 'Zła Grupa lub Użytkownik: ';
      allow = false;
     }
    if (!this.NewCaseForm.get('estimatedTime').valid) {
      this.alertMessage += 'Zły Czas Przewidywany: ';
      allow = false;
     }
    if (newCase.ClientId === null || newCase.ClientId === undefined) {
      this.alertMessage += 'Brak Klineta; ';
      allow = false;
    }
    if (newCase.CaseTagParentId === null || newCase.CaseTagParentId === undefined) {
      allow = false;
      this.alertMessage += 'Brak Typu Pracowni; ';
    }
    return allow;
  }

  createForm() {
    if (this.data.Type === 'edit') {
      this.data.Case.RepresentativeId === null ? this.data.Case.Representative = new RepresentativeModel() :
      this.newCase.RepresentativeId = this.data.Case.RepresentativeId;
      this.data.Representative = this.data.Representative;
      this.newCase = this.data.Case;
      let user = null;
      let group = null;
      let userState = true;
      let groupState = true;
      const startDate = new Date(this.data.Case.StartDate);
      const endDate = new Date(this.data.Case.EndDate);
      if (this.data.Case.User === null) {
        group = this.data.Case.Group.Name;
        groupState = false;
      } else {
        user = this.data.Case.User.Name;
        userState = false;
      }
      this.NewCaseForm = new FormGroup({
        'name' : new FormControl({value: this.data.Case.Name, disabled: false}),
        'order' : new FormControl({value: this.data.Case.Order, disabled: true}),
        'user' : new FormControl({value: user, disabled: userState}, Validators.required),
        'group' : new FormControl({value: group, disabled: groupState}, Validators.required),
        'startDate' : new FormControl({value: startDate, disabled: true}, Validators.required),
        'endDate' : new FormControl({value: endDate, disabled: true}, Validators.required),
        'client' : new FormControl({value: this.data.Case.Client.Name, disabled: false}, Validators.required),
        'representative' : new FormControl({value: this.data.Case.Representative.Name + ' ' + this.data.Case.Representative.Surname,
         disabled: false}),
        'template': new FormControl(null),
        'description' : new FormControl({value: this.data.Case.Description, disabled: false}, Validators.required),
        'estimatedTime' : new FormControl({value: this.data.Case.TimeEstimated, disabled: false},
        [Validators.required, Validators.pattern('^[0-9]*$')])
      });
    } else {
      this.NewCaseForm = new FormGroup({
        'name' : new FormControl(null),
        'order' : new FormControl({value: null, disabled: true}),
        'user' : new FormControl({value: null, disabled: false}, Validators.required),
        'group' : new FormControl({value: null, disabled: true}, Validators.required),
        'startDate' : new FormControl({value: new Date(), disabled: true}, Validators.required),
        'endDate' : new FormControl({value: new Date(), disabled: true}, Validators.required),
        'client' : new FormControl(null, Validators.required),
        'representative' : new FormControl(null),
        'template': new FormControl(null),
        'description' : new FormControl(null, Validators.required),
        'estimatedTime' : new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')])
      });
    }
  }

  createCase() {
    this.newCase.Name = this.NewCaseForm.get('name').value;
    this.newCase.Order = this.NewCaseForm.get('order').value;
    this.newCase.StartDate = this.dateService.convertToCDate(this.NewCaseForm.get('startDate').value);
    this.newCase.EndDate = this.dateService.convertToCDate(this.NewCaseForm.get('endDate').value);
    this.newCase.TimeEstimated = this.NewCaseForm.get('estimatedTime').value;
    this.newCase.Description = this.NewCaseForm.get('description').value;
  }

  changeDate() {
    this.hoursForOneEmploye = this.dateService.GetWorkHoursBetweenDates(new Date(this.NewCaseForm.get('startDate').value),
    new Date(this.NewCaseForm.get('endDate').value));
    this.NewCaseForm.get('estimatedTime').setValue(this.hoursForOneEmploye * this.employesNr);
  }

  changeTemplate() {
    this.templateSubscription = this.templateService.PickTemplateSubject.subscribe(
      template => {
        this.pickedTemplate = template;
        this.newCase.TemplateId = template.Id;
        this.hoursForOneEmploye = template.TimeEstimated;
        this.NewCaseForm.get('name').setValue(template.CaseName);
        this.NewCaseForm.get('order').setValue(
          template.CasesCount >= 10 ? template.Order + '-' + template.CasesCount : template.Order + '-' + '0' + template.CasesCount);
        this.NewCaseForm.get('estimatedTime').setValue(template.TimeEstimated);
        this.NewCaseForm.get('client').setValue(template.Client.Name);
        if (template.RepresentativeId !== null) {
          this.NewCaseForm.get('representative').setValue(template.Representative.Name);
          this.newCase.RepresentativeId = template.RepresentativeId;
        }
        if (template.CaseTagParentId !== null && template.CaseTagChildId === null) {
          this.tagService.addTagsToCaseSubject.next({ ParentTagId : template.CaseTagParentId, ChildTagId : null });
        } else if (template.CaseTagChildId !== null) {
          this.tagService.addTagsToCaseSubject.next({ ParentTagId : template.CaseTagParentId, ChildTagId : template.CaseTagChildId });
        }
        this.newCase.ClientId = template.ClientId;
        this.newCase.PriorityId = template.PriorityId;
        this.newCase.Priority = template.Priority;
      }
    );
  }

  changeRepresentative() {
    this.representativeSubscription = this.clientService.AddRepresentativeSubject.subscribe(
      representative => this.newCase.RepresentativeId = representative.Id
    );
  }

  changeClient() {
    this.clientSubscription = this.clientService.AddClientSubject.subscribe(
      client => this.newCase.ClientId = client.Id
    );
  }

  changePriority() {
    this.prioritySubscription = this.filterService.prioritySubject.subscribe(
      priority => this.newCase.PriorityId = priority.Id
    );
  }

  changeTags() {
    this.tagSubscription = this.tagService.addTagsToCaseSubject.subscribe(
      tags => { this.newCase.CaseTagParentId = tags.ParentTagId; this.newCase.CaseTagChildId = tags.ChildTagId; }
    );
  }

  changeStatus() {
    this.statusSubscription = this.filterService.statusSubject.subscribe(
      status => this.newCase.StatusId = status.Id
    );
  }

  changeUser() {
    this.userService.AddUserSubject.subscribe(
      user => {
        this.newCase.UserId = user.Id;
        this.newCase.GroupId = null;
      }
    );
  }

  changeHours(event) {
    this.employesNr = parseInt(event.target.value, 10);
    const time = this.hoursForOneEmploye * this.employesNr;
    this.NewCaseForm.get('estimatedTime').setValue(time);
  }

  changeGroup() {
    this.groupService.AddGroupSubject.subscribe(
      group => {
        this.newCase.GroupId = group.Id;
        this.newCase.UserId = null;
      }
    );
  }

  closeDialog() {
    if (confirm('Czy na pewno chcesz anulować tworzenie Zlecenia?')) {
      this.newCaseDialogRef.close();
    }
  }

  openNewTaskPanel(newCase) {
    this.pickedTemplate.TaskTemplates.forEach(temp => {
      temp.Case = newCase;
    });
    this.dialog.open(NewTaskComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '98vw',
      panelClass: 'full-screen-modal',
      data: { Case: newCase, Template: this.pickedTemplate, Type : 'New', IsFromTemplate : true }
    });
  }

  addNewCase() {
  if (this.checkCase(this.newCase)) {
    this.createCase();
    this.caseService.AddNewCase(this.newCase).subscribe(
      newCase => {
        alert('Projekt Został Dodany');
        if (this.pickedTemplate != null && this.pickedTemplate.TaskTemplates.length > 0) {
          this.openNewTaskPanel(newCase);
          this.newCaseDialogRef.close();
        } else {
          this.newCaseDialogRef.close();
        }
        this.caseService.AddNewCaseSubject.next(newCase);
      }
    );
  } else {
      alert(this.alertMessage);
    }
  }

  onSubmit() {
    this.NewCaseForm.markAsPending();
    if (this.data.Type === 'edit') {
      this.editCase();
    } else {
      this.addNewCase();
    }
  }

  ngOnDestroy(): void {
    this.tagSubscription.unsubscribe();
    this.clientSubscription.unsubscribe();
    this.prioritySubscription.unsubscribe();
    this.statusSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.groupSubscription.unsubscribe();
    this.representativeSubscription.unsubscribe();
    this.templateSubscription.unsubscribe();
  }
}

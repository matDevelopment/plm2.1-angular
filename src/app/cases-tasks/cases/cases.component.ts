import { CasePanelComponent } from './case-panel/case-panel.component';
import { CaseModel } from './../../models-database/case.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faTh, faList, faPlus, faQrcode, faCheck, faFilter } from '@fortawesome/free-solid-svg-icons';
import { NewCaseComponent } from './new-case/new-case.component';
import { MatDialog } from '@angular/material';
import { CaseService } from 'src/app/services/case.service';
import { FilterModel } from 'src/app/models/filter.model';
import { DateService } from 'src/app/services/date.service';
import { FilterService } from 'src/app/services/filter.service';
import { Subscription } from 'rxjs';
import { RolesModel } from 'src/app/models/roles-model';
import { AuthService } from 'src/app/services/auth.service';
import { FilterPanelComponent } from 'src/app/shared/filter-panel/filter-panel.component';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cases',
  templateUrl: './cases.component.html',
  styleUrls: ['./cases.component.scss']
})
export class CasesComponent implements OnInit, OnDestroy {

  constructor(private dialog: MatDialog, private caseService: CaseService,
    private dateService: DateService, private filterService: FilterService, private authService: AuthService,
    private route: ActivatedRoute) { }

  faTh = faTh;
  faList = faList;
  faPlus = faPlus;
  faCheck = faCheck;
  faQrcode = faQrcode;
  rolesModel = RolesModel;
  faFilter = faFilter;

  cases: CaseModel[] = [];
  TodaysDate: Date = new Date();
  ShowList = false;
  FilterSubscription = new Subscription();
  EditCaseSubscription = new Subscription();
  RemoveCaseSubscription = new Subscription();
  pickedFilter = null;
  startDate: Date = new Date(this.TodaysDate.getFullYear(), this.TodaysDate.getMonth(), this.TodaysDate.getDay(), 0, 0, 0);
  endDate: Date = new Date(this.TodaysDate.getFullYear(), this.TodaysDate.getMonth(), this.TodaysDate.getDay(), 0, 0, 0);
  Filter: FilterModel = new FilterModel();

  ngOnInit() {
    if (localStorage.getItem('lastSeenCase') === 'true') {
      this.Filter.LastSeenUser = this.authService.decodedToken.nameid;
    } else {
      this.Filter.LastSeenUser = null;
    }
    this.Filter.StatusId = this.pickedFilter;
    this.changeUserFilter();
    this.caseService.CaseFiltersSubject.subscribe(
      model => {
        this.Filter.StatusId = model.Id;
        this.pickedFilter = model.Id;
        this.getFilteredCases();
      }
    );
    this.caseService.AddNewCaseSubject.subscribe(
      caseModel => {
        if (caseModel.StatusId === this.Filter.StatusId || this.Filter.StatusId === null) {
          this.cases.push(caseModel);
        }
      }
    );

    this.filters();
    this.editCase();
    this.removeCase();
    this.route.paramMap.subscribe(params => {
      const filter = new FilterModel();
      filter.CaseId = parseInt(params.get('count'), 10);
      if (params.get('count') != null && filter.CaseId !== 0) {
        this.caseService.GetFilteredCases(filter).subscribe(
          cases => this.showSingleCaseDialog(cases[0])
        );
      }
    });
  }

  getFilteredCases() {
    this.setFilterDates();
    this.caseService.GetFilteredCases(this.Filter).subscribe(
      allCases => {
        this.cases = allCases;
      }
    );
  }

  setFilterDates() {
    const date = new Date(this.TodaysDate.getFullYear(), this.TodaysDate.getMonth(), this.TodaysDate.getDay(), 0, 0, 0);
    if (this.startDate.getTime() === date.getTime() && this.endDate.getTime() === date.getTime()) {
      this.Filter.StartDate = null;
      this.Filter.EndDate = null;
    } else {
      this.Filter.StartDate = this.dateService.convertToCDate(this.startDate);
      this.Filter.EndDate = this.dateService.convertToCDate(this.endDate);
    }
  }

  showSingleCaseDialog(singleCase: CaseModel) {
    this.dialog.open(CasePanelComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '98vw',
      panelClass: 'full-screen-modal',
      data: { Case: singleCase }
    });
  }

  showNewCaseDialog() {
    this.dialog.open(NewCaseComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '98vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'new' }
    });
  }

  showFiltersPanelDialog() {
    this.dialog.open(FilterPanelComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '85vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'tile', Filter: this.Filter, ShowUsers: true, ShowCases: true, }
    });
  }

  editCase() {
    this.EditCaseSubscription = this.caseService.EdtiCaseSubject.subscribe(
      editedCase => {
        let caseForEdit = null;
        this.cases.forEach(
          caseModel => { if (caseModel.Id === editedCase.Id) { caseForEdit = caseModel; } }
        );
        if (this.Filter.StatusId !== editedCase.StatusId && this.Filter.StatusId !== null) {
          this.cases.splice(this.cases.indexOf(caseForEdit), 1);
        }
      }
    );
  }

  removeCase() {
    this.RemoveCaseSubscription = this.caseService.RemoveCaseSubject.subscribe(
      caseForRemove => {
        this.cases.forEach(
          caseModel => {
            if (caseForRemove.Id === caseModel.Id) {
              this.cases.splice(this.cases.indexOf(caseModel), 1);
            }
          }
        );
      }
    );
  }

  filters() {
    this.FilterSubscription = this.filterService.filterWithSubfilterSubject.subscribe(
      model => {
        this.Filter = model.Filter;
        this.startDate = this.startDate;
        this.endDate = this.endDate;
        this.changeUserFilter();
      }
    );
  }

  ngOnDestroy(): void {
    this.FilterSubscription.unsubscribe();
    this.EditCaseSubscription.unsubscribe();
    this.RemoveCaseSubscription.unsubscribe();
  }

  onSearchByUser() {
    if (this.Filter.LastSeenUser == null) {
      this.Filter.LastSeenUser = this.authService.decodedToken.nameid;
      localStorage.setItem('lastSeenCase', 'true');
    } else {
      this.Filter.LastSeenUser = null;
      localStorage.setItem('lastSeenCase', 'false');
    }
    this.changeUserFilter();
  }

  changeUserFilter() {
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.Filter.UserId = null;
      this.Filter.TeamLeaderId = this.authService.decodedToken.nameid;
    } else if (this.authService.roleMatch([RolesModel.User])) {
      this.Filter.TeamLeaderId = null;
      this.Filter.UserId = this.authService.decodedToken.nameid;
    }
    this.getFilteredCases();
  }
}

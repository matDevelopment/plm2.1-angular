import { Component, OnInit, Input } from '@angular/core';
import { CaseModel } from 'src/app/models-database/case.model';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { faExclamation, faStar } from '@fortawesome/free-solid-svg-icons';
import { DescriptionComponent } from 'src/app/shared/description/description.component';
import { ClientComponent } from 'src/app/shared/client/client.component';
import { CasePanelComponent } from '../case-panel/case-panel.component';

@Component({
  selector: 'app-case-list',
  templateUrl: './case-list.component.html',
  styleUrls: ['./case-list.component.scss']
})
export class CaseListComponent implements OnInit {

  faStar = faStar;
  faExclamation = faExclamation;

  constructor(private dialog: MatDialog) { }

  @Input() Cases: CaseModel[] = [];

  displayedColumns: string[] = ['Order', 'Name', 'Description', 'CaseTagParent', 'CaseTagChild', 'Company', 'StartDate', 'EndDate',
  'TimeTaken', 'TimeEstimated', 'PriorityId', 'StatusId'];

  dataSource: MatTableDataSource<CaseModel>;

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.Cases);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  showDescriptionDialog(caseModel: CaseModel) {
    const dialogRef = this.dialog.open(DescriptionComponent, {
      width: '40%',
      data: { Description: caseModel.Description, CaseName: caseModel.Name  }
    });
  }

  showClientPanel(caseModel: CaseModel) {
    this.dialog.open(ClientComponent, {
      width: '50%',
      data: { Client: caseModel.Client  }
    });
  }

  showCasePanel(caseModel: CaseModel) {
    this.dialog.open(CasePanelComponent, {
      width: '95%',
      height: '97%',
      minWidth: '95%',
      data: { Case: caseModel },
      autoFocus: false
    });
  }

}

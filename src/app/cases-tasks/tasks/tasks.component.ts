import { ActivatedRoute, Router } from '@angular/router';
import { DateService } from 'src/app/services/date.service';
import { TaskService } from './../../services/task.service';
import { TaskModel } from './../../models-database/task.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { faTh, faList, faPlus, faQrcode, faCheck, faFilter} from '@fortawesome/free-solid-svg-icons';
import { FilterModel } from 'src/app/models/filter.model';
import { MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { FilterService } from 'src/app/services/filter.service';
import { RolesModel } from 'src/app/models/roles-model';
import { AuthService } from 'src/app/services/auth.service';
import { FilterPanelComponent } from 'src/app/shared/filter-panel/filter-panel.component';
import { TaskPanelComponent } from './task-panel/task-panel.component';
import { NewTaskComponent } from './new-task/new-task.component';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit, OnDestroy {

  constructor(private dialog: MatDialog, private taskService: TaskService, private authService: AuthService,
    private dateService: DateService, private filterService: FilterService, private route: ActivatedRoute) { }
  faTh = faTh;
  faList = faList;
  faPlus = faPlus;
  faCheck = faCheck;
  faQrcode = faQrcode;
  faFilter = faFilter;
  UserId: number = null;

  FilterSubscription = new Subscription();
  EditTaskSubscription = new Subscription();
  AddNewTaskSubscription = new Subscription();
  RemoveTaskSubscription = new Subscription();
  rolesModel = RolesModel;

  pickedFilter = null;

  tasks: TaskModel[] = [];
  TodaysDate: Date = new Date();
  ShowList = false;

  startDate: Date = new Date(this.TodaysDate.getFullYear(), this.TodaysDate.getMonth(), this.TodaysDate.getDay(), 0, 0, 0);
  endDate: Date = new Date(this.TodaysDate.getFullYear(), this.TodaysDate.getMonth(), this.TodaysDate.getDay(), 0, 0, 0);

  Filter: FilterModel = new FilterModel();

  ngOnInit() {
    if (localStorage.getItem('lastSeenTask') === 'true') {
      this.Filter.LastSeenUser = this.authService.decodedToken.nameid;
    } else {
      this.Filter.LastSeenUser = null;
    };
    this.Filter.StatusId = this.pickedFilter;
    this.changeUserFilter();
    this.getFilteredTasks();
    this.taskService.FilterSubject.subscribe(
      id => {
        this.Filter.StatusId = id;
        this.pickedFilter = id;
        this.getFilteredTasks();
      }
    );
    this.filters();
    this.addTask();
    this.editTask();
    this.removeTask();

    this.route.paramMap.subscribe(params => {
      const filter = new FilterModel();
      filter.TaskId = parseInt(params.get('count'), 10);
      if (params.get('count') != null && filter.TaskId !== 0) {
        this.taskService.GetFilteredTasks(filter).subscribe(
          cases => this.showSingleTaskDialog(cases[0])
        );
      }
    });
  }

  getFilteredTasks() {
    this.setFilterDates();
    this.taskService.GetFilteredTasks(this.Filter).subscribe(
      tasks => {
        {
          this.tasks = tasks;
        }
      }
    );
  }

  setFilterDates() {
    const date = new Date(this.TodaysDate.getFullYear(), this.TodaysDate.getMonth(), this.TodaysDate.getDay(), 0, 0, 0);
    if (this.startDate.getTime() === date.getTime() && this.endDate.getTime() === date.getTime()) {
      this.Filter.StartDate = null;
      this.Filter.EndDate = null;
    } else {
      this.Filter.StartDate =  this.dateService.convertToCDate(this.startDate);
      this.Filter.EndDate  = this.dateService.convertToCDate(this.endDate);
    }
  }

  filters() {
    this.FilterSubscription = this.filterService.filterWithSubfilterSubject.subscribe(
      model => {
        this.Filter = model.Filter;
        this.startDate = this.startDate;
        this.endDate = this.endDate;
        this.changeUserFilter();
       }
    );
  }

  showSingleTaskDialog(singleCase: TaskModel) {
    this.dialog.open(TaskPanelComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '98vw',
      panelClass: 'full-screen-modal',
      data: { Task: singleCase }
    });
  }

  showNewTaskDialog() {
      this.dialog.open(NewTaskComponent, {
         width: '85%',
         maxWidth: '100vw',
         height: '85vw',
         data: { Type : 'new' }
       });
  }

  showFiltersPanelDialog() {
    this.dialog.open(FilterPanelComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '85vw',
      panelClass: 'full-screen-modal',
      data: { Type: 'tile', Filter: this.Filter, ShowUsers: true, ShowCases: true}
    });
  }

  addTask() {
    this.AddNewTaskSubscription = this.taskService.AddNewTaskSubject.subscribe(
      newTask => { if (newTask.StatusId === this.Filter.StatusId || this.Filter.StatusId === null) { this.tasks.push(newTask); }
    }
    );
  }

  editTask() {
    this.EditTaskSubscription = this.taskService.EdtiTaskSubject.subscribe(
      editedTask => {
        let taskForEdit = null;
        this.tasks.forEach(
          task => { if (task.Id === editedTask.Id) { taskForEdit = task; }}
        );
        if (this.Filter.StatusId !== editedTask.StatusId && this.Filter.StatusId !== null) {
          this.tasks.splice(this.tasks.indexOf(taskForEdit), 1);
        }
      }
    );
  }

  removeTask() {
    this.RemoveTaskSubscription = this.taskService.RemoveTaskSubject.subscribe(
      task => {
        this.tasks.forEach(
          taskModel => {
            if (task.Id === taskModel.Id) {
              this.tasks.splice(this.tasks.indexOf(task), 1);
            }
          }
        );
      }
    );
  }

  onSearchByUser() {
    if (this.Filter.LastSeenUser == null) {
      this.Filter.LastSeenUser = this.authService.decodedToken.nameid;
      localStorage.setItem('lastSeenTask', 'true');
    } else {
      this.Filter.LastSeenUser = null;
      localStorage.setItem('lastSeenTask', 'false');
    }
    this.changeUserFilter();
  }

  changeUserFilter() {
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.Filter.TeamLeaderId = this.authService.decodedToken.nameid;
    } else if (this.authService.roleMatch([RolesModel.User])) {
      this.Filter.TeamLeaderId = null;
      this.Filter.UserId = this.authService.decodedToken.nameid;
    }
    this.getFilteredTasks();
  }

  ngOnDestroy(): void {
    this.FilterSubscription.unsubscribe();
    this.EditTaskSubscription.unsubscribe();
    this.AddNewTaskSubscription.unsubscribe();
    this.RemoveTaskSubscription.unsubscribe();
  }
}

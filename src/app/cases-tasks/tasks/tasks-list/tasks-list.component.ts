import { Component, OnInit, Input, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { faExclamation, faStar } from '@fortawesome/free-solid-svg-icons';
import { DescriptionComponent } from 'src/app/shared/description/description.component';
import { TaskModel } from 'src/app/models-database/task.model';
import { TaskService } from 'src/app/services/task.service';
import { Subscription } from 'rxjs';
import { TaskPanelComponent } from '../task-panel/task-panel.component';

@Component({
  selector: 'app-tasks-list',
  templateUrl: './tasks-list.component.html',
  styleUrls: ['./tasks-list.component.scss']
})
export class TasksListComponent implements OnInit, OnDestroy {

  faStar = faStar;
  faExclamation = faExclamation;

  constructor(private dialog: MatDialog, private taskService: TaskService) { }

  @Input() Tasks: TaskModel[];
  @Input() Types = 'All';

  RemoveTaskSubscription = new Subscription();
  ChangeTaskTableSubscription = new Subscription();
  dataSource = new MatTableDataSource<TaskModel>();
  taskPanelComponent = TaskPanelComponent;

  displayedColumns: string[] = ['Order', 'Name', 'Description', 'CaseName', 'TaskTagMain', 'TaskTagAccessory', 'TaskTagAccessoryMinor',
    'StartDate', 'EndDate',
    'TimeTaken', 'TimeEstimated', 'PriorityId', 'StatusId'];

  ngOnInit() {
    this.removeTask();
    this.dataSource.data = this.Tasks;
    this.changeTaskTable();
  }

  showDescriptionDialog(taskModel: TaskModel) {
    const dialogRef = this.dialog.open(DescriptionComponent, {
      width: '40%',
      data: { Description: taskModel.Description, Type: 'case', CaseName: taskModel.Name }
    });
  }

  changeTaskTable() {
    this.taskService.ChangeTaskTableSubjec.subscribe(tasks => {
      if (this.Types === tasks.Type) {
        this.Tasks = tasks.Tasks;
        this.dataSource.data = this.Tasks;
      }
    });
  }

  removeTask() {
    this.taskService.RemoveTaskSubject.subscribe(
      task => {
        this.Tasks.forEach(
          taskModel => {
            if (task.Id === taskModel.Id) {
              this.Tasks.splice(this.Tasks.indexOf(task), 1);
            }
          }
        );
      }
    );
  }

  showTaskPanel(task) {
    this.dialog.open(TaskPanelComponent, {
      width: '95%',
      height: '90%',
      minWidth: '95%',
      data: { Task: task }
    });
  }

  ngOnDestroy(): void {
    this.RemoveTaskSubscription.unsubscribe();
    this.ChangeTaskTableSubscription.unsubscribe();
  }

}

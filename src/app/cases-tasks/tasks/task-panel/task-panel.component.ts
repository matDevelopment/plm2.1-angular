import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import {
  faUserAlt, faPlus, faClone, faFile, faEllipsisH, faPlusSquare,
  faExclamation, faStar, faEdit, faTrash, faCalendarAlt
} from '@fortawesome/free-solid-svg-icons';
import { TaskModel } from 'src/app/models-database/task.model';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CasePanelComponent } from '../../cases/case-panel/case-panel.component';
import { TaskService } from 'src/app/services/task.service';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';
import { LastSeenModel } from 'src/app/models-database/last-seen.model';
import { LastSeenService } from 'src/app/services/last-seen.service';
import { Router } from '@angular/router';
import { NewTaskComponent } from '../new-task/new-task.component';
import { NewMemoComponent } from '../../memos/new-memo/new-memo.component';
import { MemoListComponent } from '../../memos/memo-list/memo-list.component';
import { ForgeComponent } from '../../forge/forge.component';
import { EcpPanelNewComponent } from 'src/app/ecp/ecp-panel-new/ecp-panel-new.component';

@Component({
  selector: 'app-task-panel',
  templateUrl: './task-panel.component.html',
  styleUrls: ['./task-panel.component.scss']
})

export class TaskPanelComponent implements OnInit, OnDestroy {

  faUserAlt = faUserAlt;
  faPlus = faPlus;
  faCopy = faClone;
  faFile = faFile;
  faEllipsisH = faEllipsisH;
  faPlusSquare = faPlusSquare;
  faExclamation = faExclamation;
  faCertificate = faStar;
  faClock = faClock;
  faEdit = faEdit;
  faTrash = faTrash;
  faCalendarAlt = faCalendarAlt;
  roles = RolesModel;
  Tasks: TaskModel[] = [];

  newTaskComponent = NewTaskComponent;
  newMemoComponent = NewMemoComponent;
  memoListComponent = MemoListComponent;
  ecpPanelComponent = EcpPanelNewComponent;
  forgeViewerComponent = ForgeComponent;

  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
    private taskDialogRef: MatDialogRef<CasePanelComponent>, private taskService: TaskService,
    public authService: AuthService, private lastSeenService: LastSeenService, private router: Router) { }

  EditTaskSubscription = new Subscription();

  ngOnInit() {
    this.onEditTask();
    this.addLastSeen();
  }

  editTask() {
    this.dialog.open(NewTaskComponent, {
      width: '85%',
      maxWidth: '100vw',
      height: '98vw',
      data: { Task: this.data.Task, Type: 'edit' }
    });
  }

  addLastSeen() {
    const lastSeen = new LastSeenModel();
    lastSeen.TaskId = this.data.Task.Id;
    lastSeen.CaseId = null;
    lastSeen.UserId = this.authService.decodedToken.nameid;
    this.lastSeenService.AddLastSeen(lastSeen).subscribe( () => console.log('success') );
  }

  onEditTask() {
    this.EditTaskSubscription = this.taskService.EdtiTaskSubject.subscribe(
      editedTask => {
        if (this.data.Task.Id === editedTask.Id) {
          this.data.Task = editedTask;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.router.navigateByUrl('/tasks/0');
    this.EditTaskSubscription.unsubscribe();
  }

  changeStatus(id: number) {
    this.data.Task.StatusId = id;
    this.taskService.ChangeStatus(this.data.Task).subscribe(
      caseModel => {
        this.data.Task.Status = caseModel.Status;
      }
    );
  }

  removeTask() {
    this.taskService.RemoveTask(this.data.Task).subscribe(
      task => {
        this.taskService.RemoveTaskSubject.next(task);
        this.taskDialogRef.close();
      }
    );
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { DescriptionComponent } from 'src/app/shared/description/description.component';
import { faUserAlt, faPlus, faClone, faFile, faPlusSquare,
        faEllipsisH, faExclamation, faStar, faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { MatDialog } from '@angular/material';
import { TaskModel } from 'src/app/models-database/task.model';
@Component({
  selector: 'app-tasks-tiles',
  templateUrl: './tasks-tiles.component.html',
  styleUrls: ['./tasks-tiles.component.scss']
})
export class TasksTilesComponent implements OnInit {

  faUserAlt = faUserAlt;
  faPlus = faPlus;
  faCopy = faClone;
  faFile = faFile;
  faEllipsisH = faEllipsisH;
  faPlusSquare = faPlusSquare;
  faExclamation = faExclamation;
  faCertificate = faStar;
  faChevronRight = faChevronRight;
  faChevronLeft = faChevronLeft;
  shownStart = 0;
  shownEnd = 9;

  @Input() Tasks: TaskModel[] = [];

  constructor(private dialog: MatDialog) { }

  ngOnInit() {}

  showDescriptionDialog(task: TaskModel) {
    const dialogRef = this.dialog.open(DescriptionComponent, {
      width: '40%',
      data: { Description: task.Description, Name: task.Name  }
    });
  }

  addPage() {
    this.shownStart += 9;
    this.shownEnd += 9;
  }

  decresePage() {
    if ( this.shownStart > 0) {
      this.shownStart -= 9;
      this.shownEnd -= 9;
    }
  }
}

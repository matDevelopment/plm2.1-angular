import { DateService } from './../../../services/date.service';
import { Component, OnInit, Inject, OnDestroy, AfterViewInit, AfterContentInit, AfterViewChecked } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { TagService } from 'src/app/services/tag.service';
import { NewGroupComponent } from 'src/app/groups-users/groups/new-group/new-group.component';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { GroupService } from 'src/app/services/group.service';
import { TaskModel } from 'src/app/models-database/task.model';
import { TaskService } from 'src/app/services/task.service';
import { CaseService } from 'src/app/services/case.service';
import { CaseModel } from 'src/app/models-database/case.model';
import { FilterService } from 'src/app/services/filter.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TemplateService } from 'src/app/services/template.service';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FilterModel } from 'src/app/models/filter.model';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';
import { TaskTemplateModel } from 'src/app/models-database/task-template.model';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss']
})
export class NewTaskComponent implements OnInit, OnDestroy {

  constructor(private taskService: TaskService, private dateService: DateService, private tagService: TagService,
    @Inject(MAT_DIALOG_DATA) public data: any, private newCaseDialogRef: MatDialogRef<NewGroupComponent>,
    private caseService: CaseService, private userService: UserService, private groupService: GroupService,
    private filterService: FilterService, private templateService: TemplateService, private authService: AuthService) { }

  newTask: TaskModel = new TaskModel();
  pickedCase: CaseModel = null;
  faTimes = faTimes;

  alertMessage = '';
  nextTask = false;
  taskTemplatesLength = 0;

  cases: CaseModel[] = [];
  filteredCases: Promise<CaseModel[]>;

  NewTaskForm: FormGroup;

  tagSubscription = new Subscription();
  clientSubscription = new Subscription();
  userSubscritpion = new Subscription();
  groupSubscription = new Subscription();
  prioritySubscription = new Subscription();
  statusSubscription = new Subscription();
  templateSubscription = new Subscription();
  CaseTagId: number;

  ngOnInit() {
    if (this.data.Case != null) {
      this.CaseTagId = this.data.Case.CaseTagParentId;
    }
    this.changeUser();
    this.changeStatus();
    this.changePriority();
    this.getCases();
    this.newTask.Case = new CaseModel();
    this.initForm();
    if (this.data.Type === 'edit' && !this.data.IsFromTemplate) {
      this.initEditTask(this.data.Task);
      this.CaseTagId = this.data.Task.Case.CaseTagParentId;
    } else if (this.data.Type !== 'edit' && this.data.IsFromTemplate) {
      this.changeTemplate(this.data.Template.TaskTemplates[this.taskTemplatesLength]);
    }
    this.editTags();
    this.onChangeTemplate();
  }

  changeTemplate(template: TaskTemplateModel) {
    this.newTask.Name = template.TaskName;
    this.newTask.TaskTagMainId = template.TaskTagMainId;
    this.newTask.TaskTagAccessoryId = template.TaskTagAccessoryId;
    this.newTask.TaskTagAccessoryMinorId = template.TaskTagAccessoryMinorId;
    this.newTask.CaseId = template.CaseId;
    this.newTask.Case = template.Case;
    this.newTask.PriorityId = template.PriorityId;
    this.newTask.Priority = template.Priority;
    this.CaseTagId = template.CaseTagId;
    this.newTask.TimeEstimated = template.TimeEstimated;
    this.newTask.Users == null ? this.newTask.Users = [] : this.newTask.Users = this.newTask.Users;
    this.initEditTask(this.newTask);
  }

  initEditTask(task) {
    this.newTask = task;
    let groupState = true;
    groupState = false;
    this.NewTaskForm.get('name').setValue(this.newTask.Name);
    this.NewTaskForm.get('order').setValue(this.newTask.Order);
    this.NewTaskForm.get('case').setValue(this.newTask.Case.Name);
    this.NewTaskForm.get('startDate').setValue(new Date(this.newTask.StartDate));
    this.NewTaskForm.get('endDate').setValue(new Date(this.newTask.EndDate));
    this.NewTaskForm.get('description').setValue(this.newTask.Description);
    this.NewTaskForm.get('load').setValue((this.newTask.Users.length * 100) + '%');
    this.NewTaskForm.get('estimatedTime').setValue(this.newTask.TimeEstimated);
    this.NewTaskForm.get('maxTime').setValue(this.newTask.Order);
    this.NewTaskForm.get('nextTask').setValue(this.newTask.Order);
    this.pickCase(this.newTask.Case);
  }

  initForm() {
    this.NewTaskForm = new FormGroup({
      'name': new FormControl(null),
      'order': new FormControl({ value: null, disabled: true }),
      'user': new FormControl({ value: null, disabled: false }, Validators.required),
      'group': new FormControl({ value: null, disabled: true }, Validators.required),
      'case': new FormControl(null, Validators.required),
      'startDate': new FormControl({ value: new Date(), disabled: true }, Validators.required),
      'endDate': new FormControl({ value: new Date(), disabled: true }, Validators.required),
      'description': new FormControl(null),
      'taskTemplate': new FormControl({ value: null, disabled: false }),
      'estimatedTime': new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$')]),
      'load': new FormControl({ value: '100%', disabled: true }),
      'maxTime': new FormControl({ value: 0, disabled: true }),
      'nextTask': new FormControl({ value: true }),
    });
  }

  getCases() {
    const filter = new FilterModel();
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      filter.TeamLeaderId = this.authService.decodedToken.nameId;
    }
    this.caseService.GetFilteredCases(filter).subscribe(
      cases => {
        this.cases = cases; this.filteredCases = new Promise((resolve, reject) => {
          resolve(this.cases);
        });
      }
    );
  }

  onFilterCases(name) {
    this.filteredCases = new Promise((resolve, reject) => {
      resolve(this.cases.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  onPickCase(_event, caseModel: CaseModel) {
    if (_event.isUserInput) {
      this.pickCase(caseModel);
      this.tagService.addTagsToCaseSubject.next({ ParentTagId: caseModel.CaseTagParentId, ChildTagId: null });
    }
  }

  pickCase(caseModel: CaseModel) {
    this.newTask.CaseId = caseModel.Id;
    this.newTask.Case = caseModel;
    this.CaseTagId = caseModel.CaseTagParentId;
    this.NewTaskForm.get('order').setValue(caseModel.TasksCount < 10 ? caseModel.Order + '-0' + caseModel.TasksCount :
      caseModel.Order + '-' + caseModel.TasksCount);
    this.NewTaskForm.get('maxTime').setValue(caseModel.TimeEstimated - caseModel.TasksTime);
    this.NewTaskForm.get('startDate').setValue(this.dateService.converCDateToDate(caseModel.StartDate));
    this.NewTaskForm.get('endDate').setValue(this.dateService.converCDateToDate(caseModel.EndDate));
  }

  onChangeTemplate() {
    this.templateSubscription = this.templateService.PickTaskTemplateSubject.subscribe(
      template => {
        this.changeTemplate(template);
        this.tagService.pickedTaskTagsSubject.next({
          CaseTagId: this.newTask.CaseId,
          MainTagId: this.newTask.TaskTagMainId,
          AccessoryTagId: this.newTask.TaskTagAccessoryId,
          AccessoryMinorTagId: this.newTask.TaskTagAccessoryMinorId
        });
      }
    );
  }

  changeUser() {
    this.userSubscritpion = this.userService.AddUsersSubject.subscribe(
      users => { this.newTask.Users = users; this.NewTaskForm.get('load').setValue((this.newTask.Users.length * 100) + '%'); }
    );
  }

  changePriority() {
    this.prioritySubscription = this.filterService.prioritySubject.subscribe(
      priority => this.newTask.PriorityId = priority.Id
    );
  }

  changeStatus() {
    this.statusSubscription = this.filterService.statusSubject.subscribe(
      status => this.newTask.StatusId = status.Id
    );
  }

  changeDate() {
    let length = 0;
    this.newTask.Users != null ? length = this.newTask.Users.length : length = 0;
    this.NewTaskForm.get('estimatedTime').setValue(
      length * this.dateService.GetWorkHoursBetweenDates(new Date(this.NewTaskForm.get('startDate').value),
        new Date(this.NewTaskForm.get('endDate').value)));
  }

  editTags() {
    this.tagSubscription = this.tagService.addTagsToTaskSubject.subscribe(
      tags => {
        tags.MainTag !== null ? this.newTask.TaskTagMainId = tags.MainTag.Id : this.newTask.TaskTagMainId = null;
        tags.AccessoryTag !== null ? this.newTask.TaskTagAccessoryId = tags.AccessoryTag.Id : this.newTask.TaskTagAccessoryId = null;
        tags.AccessoryMinorTag !== null ? this.newTask.TaskTagAccessoryMinorId = tags.AccessoryMinorTag.Id
          : this.newTask.TaskTagAccessoryMinorId = null;
      }
    );
  }

  checkTask(newTask: TaskModel): boolean {
    let allow = true;
    this.newTask.StartDate = this.dateService.convertToCDate(this.NewTaskForm.get('startDate').value);
    this.newTask.EndDate = this.dateService.convertToCDate(this.NewTaskForm.get('endDate').value);
    this.newTask.Case.StartDate == null ? this.newTask.Case.StartDate =
      this.dateService.convertToCDate(this.NewTaskForm.get('startDate').value) :
      this.newTask.Case.StartDate = this.newTask.Case.StartDate;
    this.newTask.Case.EndDate == null ? this.newTask.Case.EndDate = this.dateService.convertToCDate(this.NewTaskForm.get('endDate').value) :
      this.newTask.Case.EndDate = this.newTask.Case.EndDate;
    this.alertMessage = '';
    this.data.Type === 'edit' ? this.newTask.StatusId = this.data.Task.StatusId : this.newTask.StatusId = 1;
    if (newTask.PriorityId === null || newTask.PriorityId === undefined) {
      this.alertMessage += 'Brak Priorytetu; ';
      allow = false;
    }
    if (newTask.StartDate === null || newTask.StartDate === undefined ||
      this.NewTaskForm.get('startDate').value.getTime() >= this.NewTaskForm.get('endDate').value.getTime()) {
      this.alertMessage += 'Zła Data Startu; ';
      allow = false;
    }
    if (newTask.EndDate === null || newTask.EndDate === undefined) {
      this.alertMessage += 'Zła Data Końca; ';
      allow = false;
    }
    if (newTask.Users != null) {
      if (newTask.Users.length < 1) {
        this.alertMessage += 'Brak Użytkownika';
        allow = false;
      }
    } else {
      this.alertMessage += 'Brak Użytkownika';
      allow = false;
    }
    if (!this.NewTaskForm.get('estimatedTime').valid &&
      this.NewTaskForm.get('estimatedTime').value > this.newTask.Case.TimeEstimated) {
      this.alertMessage += 'Zły Czas Przewidywany; ';
      allow = false;
    }
    if (this.newTask.TimeEstimated > parseInt(this.NewTaskForm.get('maxTime').value, 10)) {
      this.alertMessage += 'Za duży Przewidywany; ';
      allow = false;
    }
    if (newTask.CaseId === null || newTask.CaseId === undefined) {
      this.alertMessage += 'Brak Projektu; ';
      allow = false;
    }
    if (newTask.TaskTagMainId === null || newTask.TaskTagMainId === undefined) {
      this.alertMessage += 'Zły Etap Zadania; ';
      allow = false;
    }
    if (newTask.TimeEstimated > (this.newTask.Case.TimeEstimated - this.newTask.Case.TasksTime)) {
      this.alertMessage += 'Za duży czas przewidywany; ';
      allow = false;
    }
    if (this.dateService.converCDateToDate(newTask.StartDate).getTime()
      < this.dateService.converCDateToDate(newTask.Case.StartDate).getTime()) {
      this.alertMessage += 'Czas rozpoczęcia zadania nie może być mniejszy od czasu rozpoczęcia projektu';
    }

    if (this.dateService.converCDateToDate(newTask.EndDate).getTime()
      > this.dateService.converCDateToDate(newTask.Case.EndDate).getTime()) {
      this.alertMessage += 'Czas Zakończenia zadania nie może być większy od czasu zakończenia projektu';
    }

    return allow;
  }

  createTask() {
    this.newTask.Name = this.NewTaskForm.get('name').value;
    this.newTask.Description = this.NewTaskForm.get('description').value;
    this.newTask.TimeEstimated = this.NewTaskForm.get('estimatedTime').value;
    this.newTask.Order = this.NewTaskForm.get('order').value;
    this.newTask.Users != null ? this.newTask.DestinatedEmployees = this.newTask.Users.length : this.newTask.DestinatedEmployees = 1;
  }

  addNewTask() {
    if (this.checkTask(this.newTask)) {
      this.taskService.AddNewTask(this.newTask).subscribe(
        newTask => {
          alert('Zadanie Zostało Dodane');
          if (!this.nextTask) {
            this.newCaseDialogRef.close();
          }
          const maxTime = parseInt(this.NewTaskForm.get('maxTime').value, 10);
          this.NewTaskForm.get('maxTime').setValue((maxTime - this.newTask.TimeEstimated));
          this.taskService.AddNewTaskSubject.next(newTask);
        }
      );
    } else {
      alert(this.alertMessage);
    }
  }

  addNewTaskFromTemplate() {
    if (this.checkTask(this.newTask)) {
      this.taskService.AddNewTask(this.newTask).subscribe(
        newTask => {
          alert('Zadanie Zostało Dodane');
          this.taskTemplatesLength += 1;
          const maxTime = parseInt(this.NewTaskForm.get('maxTime').value, 10);
          this.NewTaskForm.get('maxTime').setValue((maxTime - this.newTask.TimeEstimated));
          if (this.data.Template.TaskTemplates[this.taskTemplatesLength] != null) {
            this.changeTemplate(this.data.Template.TaskTemplates[this.taskTemplatesLength]);
          } else {
            this.newCaseDialogRef.close();
          }
          this.taskService.AddNewTaskSubject.next(newTask);
        }
      );
    } else {
      alert(this.alertMessage);
    }
  }

  editTask() {
    if (this.checkTask(this.newTask)) {
      this.taskService.EditCase(this.newTask).subscribe(
        editedCase => {
          alert('Zadanie Zostało Edytowane');
          this.taskService.EdtiTaskSubject.next(editedCase);
          this.newCaseDialogRef.close();
        }
      );
    } else {
      alert(this.alertMessage);
    }
  }

  onSubmit() {
    this.NewTaskForm.markAsPending();
    this.createTask();
    if (this.data.Type === 'edit' && !this.data.IsFromTemplate) {
      this.editTask();
    } else if (this.data.Type !== 'edit' && this.data.IsFromTemplate) {
      this.addNewTaskFromTemplate();
    } else {
      this.addNewTask();
    }
  }

  closeDialog() {
    if (confirm('Czy na pewno chcesz anulować tworzenie Zlecenia?')) {
      this.newCaseDialogRef.close();
    }
  }

  ngOnDestroy(): void {
    this.tagSubscription.unsubscribe();
    this.clientSubscription.unsubscribe();
    this.userSubscritpion.unsubscribe();
    this.groupSubscription.unsubscribe();
    this.prioritySubscription.unsubscribe();
    this.statusSubscription.unsubscribe();
    this.templateSubscription.unsubscribe();
  }
}

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '../../../../../node_modules/@angular/material';
import { MemoModel } from '../../../models-database/memo.model';
import { CaseService } from '../../../services/case.service';
import { TaskService } from '../../../services/task.service';
import { CaseModel } from '../../../models-database/case.model';
import { TaskModel } from '../../../models-database/task.model';
import { faUserAlt } from '../../../../../node_modules/@fortawesome/free-solid-svg-icons';
import { IndividualCaseComponent } from '../individual-case/individual-case.component';
import { IndividualTaskComponent } from '../individual-task/individual-task.component';
import { FilterModel } from '../../../models/filter.model';

@Component({
  selector: 'app-individual-memo',
  templateUrl: './individual-memo.component.html',
  styleUrls: ['./individual-memo.component.scss']
})
export class IndividualMemoComponent implements OnInit {
  faUserAlt = faUserAlt;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private caseService: CaseService,
  private taskService: TaskService, private dialog: MatDialog) {}

newMemo: MemoModel = new MemoModel(null, null, null, null, null, null, null, null, null, null, null, null);
newCase: CaseModel = new CaseModel();
newTask: TaskModel = new TaskModel();
filterModel: FilterModel = new FilterModel();

  ngOnInit() {
    console.log(this.data.Memo);
    this.createForm();
  }

  createForm() {
    this.newMemo = this.data.Memo;
    if (this.newMemo.Type === 'Case') {
      this.filterModel.CaseId = this.newMemo.ParentId;
     this.caseService.GetFilteredCases(this.filterModel).subscribe(
      foundCase => {
        this.newCase = foundCase[0];
        console.log(this.newCase);
      }
    );
    } else if (this.newMemo.Type === 'Task') {
      this.filterModel.TaskId = this.newMemo.ParentId;
      this.taskService.GetFilteredTasks(this.filterModel).subscribe(
        foundTask => {
          this.newTask = foundTask[0];
          console.log(this.newTask);
        }
      );
    }
  }

  showCasePanel(caseModel: CaseModel) {
    this.dialog.open(IndividualCaseComponent, {
      width: '95%',
      height: '97%',
      minWidth: '95%',
      data: { Case: caseModel },
      autoFocus: false
    });
  }

  showTaskPanel(taskModel: TaskModel) {
    this.dialog.open(IndividualTaskComponent, {
      width: '95%',
      height: '97%',
      minWidth: '95%',
      data: { Task: taskModel },
      autoFocus: false
    });
  }
}

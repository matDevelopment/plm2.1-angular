import { Component, OnInit, Input } from '@angular/core';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-files-upload',
  templateUrl: './files-upload.component.html',
  styleUrls: ['./files-upload.component.scss']
})
export class FilesUploadComponent implements OnInit {

  fileToUpload: File = null;
  @Input() FolderId = null;

  constructor(private fileService: FileService) { }

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileService.postFile(this.fileToUpload, this.FolderId).subscribe(data => {
      this.fileService.uploadFileSubject.next();
    }, error => {
      console.log(error);
    });
  }
}

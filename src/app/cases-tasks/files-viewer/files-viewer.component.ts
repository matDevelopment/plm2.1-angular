import { ForgeService } from './../../services/forge.service';
import { FileService } from '../../services/file.service';
import { Component, OnInit, ViewChild, Input, Inject, OnDestroy } from '@angular/core';
import { TreeNode } from 'angular-tree-component/dist/defs/api';
import { TreeComponent } from 'angular-tree-component';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-files-viewer',
  templateUrl: './files-viewer.component.html',
  styleUrls: ['./files-viewer.component.scss']
})
export class FilesViewerComponent implements OnInit, OnDestroy {

  @Input() FolderId = null;
  @Input() height = '100%';
  @Input() Title = 'Pliki Lokalne';

  pickedFile = 'Brak';
  showModal = false;
  filePath = '';

  @ViewChild(TreeComponent)
  private tree: TreeComponent;
  fileSubscription = new Subscription();

  nodes = [];

  constructor(private fileService: FileService, private forgeService: ForgeService) { }

  optionsTree = {
    getChildren: (node: TreeNode) => {
      const pathArr = [];
      let path = '';
      if (this.FolderId !== '//root') {
        path = this.FolderId;
      }
      this.createPath(node, pathArr);
      pathArr.reverse();
      pathArr.forEach(pathPart => path = path + '//' + pathPart);
      return new Promise((resolve, reject) => {
        this.fileService.GetDirectory(path).subscribe(
          res => {
            const nodes = []; res.forEach(element => {
              nodes.push({ name: element.Name, hasChildren: element.HasChildren });
            });
            resolve(nodes);
          }
        );
      });
    },
  };

  PickFile(node: TreeNode) {
    if (!node.node.hasChildren) {
      const pathArr = [];
      if (this.FolderId !== '//root') {
        this.filePath = this.FolderId;
      }
      this.createPathNode(node, pathArr);
      pathArr.reverse();
      pathArr.forEach(pathPart => this.filePath = this.filePath + '//' + pathPart);
      this.pickedFile = node.node.displayField;
      this.showModal = true;
    }
  }

  DownloadFile() {
    this.fileService.DownloadFile(this.filePath)
      .subscribe(file => {
        const name = file.headers.get('Content-Disposition').split("''");
        saveAs(new Blob([file.body], { type: file.headers.get('Content-Type') }), name[name.length - 1]);
      });
  }

  RemoveFile() {

  }

  SendFileToBim360() {
    this.forgeService.sendFileSubject.next(this.filePath);
    this.showModal = false;
  }

  createPath(node: TreeNode, pathArr: string[]) {
    if (node.parent.displayField != null) {
      pathArr.push(node.displayField);
      this.createPath(node.parent, pathArr);
    } else {
      pathArr.push(node.displayField);
      return pathArr;
    }
  }

  createPathNode(node: TreeNode, pathArr: string[]) {
    if (node.node.parent.displayField != null) {
      pathArr.push(node.node.displayField);
      this.createPath(node.node.parent, pathArr);
    } else {
      pathArr.push(node.node.displayField);
      return pathArr;
    }
  }

  ngOnInit() {
    if (this.FolderId === null) {
      this.FolderId = '//root';
    } else {
      this.FolderId = '//' + this.FolderId;
    }
    this.getData();
    this.fileSubscription = this.fileService.uploadFileSubject.subscribe(
      () => this.getData()
    );
  }

  getData() {
    this.nodes = [];
    this.fileService.GetDirectory(this.FolderId).subscribe(
      res => {
        res.forEach(element => {
          this.nodes.push(
            { name: element.Name, hasChildren: element.HasChildren }
          );
        });
        this.tree.treeModel.update();
      }
    );
  }

  ngOnDestroy(): void {
    this.fileSubscription.unsubscribe();
  }

}

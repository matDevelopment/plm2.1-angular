import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatRadioChange, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MemoService } from 'src/app/services/memo.service';
import { Subscription } from 'rxjs';
import { DateService } from './../../../services/date.service';
import { MemoModel } from 'src/app/models-database/memo.model';
import { UserService } from '../../../services/user.service';
import { GroupService } from '../../../services/group.service';
import { UserModel } from '../../../models-database/user.model';
import { FormGroup, FormControl, Validators } from '../../../../../node_modules/@angular/forms';
import { GroupModel } from '../../../models-database/group.model';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-new-memo',
  templateUrl: './new-memo.component.html',
  styleUrls: ['./new-memo.component.scss']
})
export class NewMemoComponent implements OnInit, OnDestroy {


    constructor(private memoService: MemoService, private dateService: DateService,
      private userService: UserService, private groupService: GroupService, private authService: AuthService,
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<NewMemoComponent>) {}
      FilteredUsersAsync: Promise<UserModel[]>;
      FilteredUsers: UserModel[] = [];

      userSubscritpion = new Subscription();
      groupSubscription = new Subscription();

      newMemo: MemoModel = new MemoModel(0, null, null, null, null, null, null, null, null, null, null, null);
      name = '';
      NewMemoForm: FormGroup;

    ngOnInit() {
      console.log(this.data);
      this.changeGroup();
      this.changeUser();
      this.createForm();
    }

  createForm() {
    if (this.data.Type === 'edit') {
      let user = null;
      let group = null;
      let userState = true;
      let groupState = true;
      const startDate = new Date(this.newMemo.Time);
      if (this.newMemo.Assignee === null) {
        group = this.newMemo.Group.Name;
        userState = false;
      } else {
        user = this.newMemo.Assignee.Name;
        groupState = false;
      }
      this.NewMemoForm = new FormGroup({
        'title': new FormControl({ value: this.newMemo.Title, disabled: false }, Validators.required),
        'user': new FormControl({ value: user, disabled: userState }, Validators.required),
        'group': new FormControl({ value: group, disabled: groupState }, Validators.required),
        'time': new FormControl({ value: startDate, disabled: true }, Validators.required),
        'description': new FormControl({ value: this.newMemo.Description, disabled: false }, Validators.required),
      });
    } else {
      this.NewMemoForm = new FormGroup({
        'title': new FormControl(null, Validators.required),
        'user': new FormControl({ value: null, disabled: false }, Validators.required),
        'group': new FormControl({ value: null, disabled: true }, Validators.required),
        'time': new FormControl({ value: null, disabled: true }, Validators.required),
        'description': new FormControl(null, Validators.required),
      });
    }
  }

  OnAddClick() {
    if (this.data.isEdited) {
      this.dialogRef.close();
      this.memoService.UpdateMemo(this.newMemo).subscribe(res => {
          this.memoService.AddNewMemoSubject.next(res);
      });

  } else {
      this.newMemo.ReporterId = this.authService.decodedToken.nameid;
      this.newMemo.Type = this.data.Type;
      this.newMemo.ParentId = this.data.ParentId;
      this.newMemo.Title = this.NewMemoForm.get('title').value;
      this.newMemo.Description = this.NewMemoForm.get('description').value;
      this.dialogRef.close();
      this.memoService.AddNewMemo(this.newMemo).subscribe(res => {
          this.memoService.AddNewMemoSubject.next(res);
      });
  }

  }

  OnCancelClick() {
    this.dialogRef.close();
  }

  changeUser() {
    this.userSubscritpion = this.userService.AddUserSubject.subscribe(
      user => {
        this.newMemo.AssigneeId = user.Id;
        this.newMemo.GroupId = null;
      }
    );
  }

  changeGroup() {
    this.groupSubscription = this.groupService.AddGroupSubject.subscribe(
      group => {
        this.newMemo.GroupId = group.Id;
        this.newMemo.AssigneeId = null;
      }
    );
  }

  doFilter(name) {
    this.FilteredUsersAsync = new Promise( (resolve, reject) => {
      resolve(this.FilteredUsers.filter(value => {
         return value.Name.toLowerCase().includes(name.toLowerCase()); })); });
  }

  ngOnDestroy(): void {
    this.userSubscritpion.unsubscribe();
    this.groupSubscription.unsubscribe();
  }
}

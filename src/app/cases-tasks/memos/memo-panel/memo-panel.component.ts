import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-memo-panel',
  templateUrl: './memo-panel.component.html',
  styleUrls: ['./memo-panel.component.scss']
})
export class MemoPanelComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    console.log(this.data);
  }

}

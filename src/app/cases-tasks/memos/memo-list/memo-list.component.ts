import { Component, OnInit, Input, ViewChild, Inject, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatSort, MatDialog, MatIconModule, MAT_DIALOG_DATA } from '@angular/material';
import { MemoService } from 'src/app/services/memo.service';
import { NewMemoComponent } from '../new-memo/new-memo.component';
import { MemoModel } from '../../../models-database/memo.model';
import { faCoffee, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { MemoPanelComponent } from '../memo-panel/memo-panel.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-memo-list',
  templateUrl: './memo-list.component.html',
  styleUrls: ['./memo-list.component.scss']
})
export class MemoListComponent implements OnInit, OnDestroy {

  constructor(private memosService: MemoService, private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
  faEdit = faEdit;
  faTrash = faTrash;
  @ViewChild(MatSort) sort: MatSort;
  addMemoSubscription = new Subscription();
  memos: MemoModel[] = [];
  dataSource;
  displayedColumns = ['Title', 'Description', 'Time', 'Username', 'Delete'];
  ngOnInit() {
    console.log(this.data);
    this.loadMemos(this.data.Type);
    this.addMemoSubscription = this.memosService.AddNewMemoSubject.subscribe(memo => {
      console.log(memo);
      this.memos.push(memo);
      this.dataSource = new MatTableDataSource(this.memos);
      this.dataSource.sort = this.sort;
    });
  }

  onDeleteMemo(element, index) {
    if (confirm('Czy na pewno chcesz usunąć Notatkę?')) {
      this.memosService.DeleteMemo(element).subscribe(
        res => {
          this.memos.splice(index, 1);
          this.dataSource = new MatTableDataSource(this.memos);
          this.dataSource.sort = this.sort;
        });
    }
  }

  loadMemos(Type: string) {
    this.memosService.GetMemosForParent(this.data.ParentId, Type).subscribe(
      resp => {
        this.memos = resp;
        this.dataSource = new MatTableDataSource(this.memos);
        this.dataSource.sort = this.sort;
      }
    );
  }

  openNewMemoPanel() {
    this.dialog.open(NewMemoComponent, {
      width: '60%',
      maxWidth: '100vw',
      height: '90%',
      data: { ParentId: this.data.ParentId, Type: this.data.Type, Model : this.data.Model}
    });
  }

  openMemoPanel(memo: MemoModel) {
    this.dialog.open(MemoPanelComponent, {
      width: '70%',
      maxWidth: '100vw',
      height: '90%',
      data: { Memo: memo }
    });
  }

  ngOnDestroy(): void {
    this.addMemoSubscription.unsubscribe();
  }
}

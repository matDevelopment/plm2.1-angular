import { FileService } from './../services/file.service';
import { ForgeFilesComponent } from './forge/forge-files/forge-files.component';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';
import { TaskService } from './../services/task.service';
import { AnimatedButtonComponent } from './../shared/animated-button/animated-button.component';
import { CaseService } from '../services/case.service';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { CasesComponent } from './cases/cases.component';
import { CasesTilesComponent } from './cases/cases-tiles/cases-tiles.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TasksComponent } from './tasks/tasks.component';
import { CasesTasksRoutingModule } from './cases-task-routing.module';
import { MatDialogModule, MatFormFieldModule, MatSelectModule,
    MatInputModule, MatSlideToggleModule, MatAutocompleteModule,
    MatRadioModule, MatDatepickerModule, MatNativeDateModule, MatTableModule,
    MatCardModule,
    MatCheckboxModule,
} from '@angular/material';
import { DescriptionComponent } from '../shared/description/description.component';
import { NewCaseComponent } from './cases/new-case/new-case.component';
import { FilterButtonsComponent } from '../shared/filter-buttons/filter-buttons.component';
import { CaseListComponent } from './cases/case-list/case-list.component';
import { TasksTilesComponent } from './tasks/tasks-tiles/tasks-tiles.component';
import { TasksListComponent } from './tasks/tasks-list/tasks-list.component';
import { CasePanelComponent } from './cases/case-panel/case-panel.component';
import { CaseTasksListComponent } from './cases/case-panel/case-tasks-list/case-tasks-list.component';
import { TaskPanelComponent } from './tasks/task-panel/task-panel.component';
import { TagService } from '../services/tag.service';
import { StatusPickerComponent } from '../shared/status-picker/status-picker.component';
import { ClientComponent } from '../shared/client/client.component';
import {MemosComponent} from './memos/memos.component';
import {NewMemoComponent} from './memos/new-memo/new-memo.component';
import { MemoService } from '../services/memo.service';
import { MemoListComponent } from './memos/memo-list/memo-list.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import {NotificationService} from '../services/notification.service';
import {IndividualCaseComponent} from './individual-panels/individual-case/individual-case.component';
import { IndividualMemoComponent } from './individual-panels/individual-memo/individual-memo.component';
import { IndividualTaskComponent } from './individual-panels/individual-task/individual-task.component';
import { ForgeComponent } from './forge/forge.component';
import { TreeModule } from 'angular-tree-component';
import { ForgeViewerComponent } from './forge/forge-viewer/forge-viewer.component';
import { FilesViewerComponent } from './files-viewer/files-viewer.component';
import { FilesUploadComponent } from './files-viewer/files-upload/files-upload.component';
import { MemoPanelComponent } from './memos/memo-panel/memo-panel.component';
import { ForgeDirectorySettingsComponent } from './forge/forge-directory-settings/forge-directory-settings.component';
import { CaseTaskTreeComponent } from './forge/forge-directory-settings/case-task-tree/case-task-tree.component';
import { NewTaskComponent } from './tasks/new-task/new-task.component';

@NgModule({
    declarations: [
        CasesComponent,
        CasesTilesComponent,
        TasksComponent,
        DescriptionComponent,
        NewCaseComponent,
        FilterButtonsComponent,
        CaseListComponent,
        TasksTilesComponent,
        FilterPanelComponent,
        TasksListComponent,
        CasePanelComponent,
        CaseTasksListComponent,
        TaskPanelComponent,
        NewTaskComponent,
        StatusPickerComponent,
        ClientComponent,
        MemosComponent,
        NewMemoComponent,
        MemoListComponent,
        IndividualCaseComponent,
        IndividualMemoComponent,
        IndividualTaskComponent,
        ForgeComponent,
        ForgeViewerComponent,
        ForgeFilesComponent,
        FilesViewerComponent,
        FilesUploadComponent,
        MemoPanelComponent,
        ForgeDirectorySettingsComponent,
        CaseTaskTreeComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        FormsModule,
        CasesTasksRoutingModule,
        MatDialogModule,
        SharedModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatTableModule,
        MatCardModule,
        MatCheckboxModule,
        TreeModule.forRoot(),
    ],
    providers: [ CaseService, TaskService, TagService, MemoService, NotificationService, FileService ],
    entryComponents: [
        DescriptionComponent,
        NewCaseComponent,
        FilterPanelComponent,
        CasePanelComponent,
        TaskPanelComponent,
        NewTaskComponent,
        MemoPanelComponent,
        CaseTasksListComponent,
        ClientComponent,
        NewMemoComponent,
        AnimatedButtonComponent,
        MemoListComponent,
        IndividualCaseComponent,
        IndividualMemoComponent,
        IndividualTaskComponent,
        ForgeComponent,
    ]
})
export class CasesTasksModule { }

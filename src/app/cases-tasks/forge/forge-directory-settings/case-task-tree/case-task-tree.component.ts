import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeNode } from 'angular-tree-component/dist/defs/api';
import { TreeComponent } from 'angular-tree-component';
import { CaseService } from 'src/app/services/case.service';
import { FilterModel } from 'src/app/models/filter.model';
import { TaskService } from 'src/app/services/task.service';
import { ForgeService } from 'src/app/services/forge.service';

@Component({
  selector: 'app-case-task-tree',
  templateUrl: './case-task-tree.component.html',
  styleUrls: ['./case-task-tree.component.scss']
})
export class CaseTaskTreeComponent implements OnInit {

  constructor(private caseService: CaseService, private taskService: TaskService, private forgeService: ForgeService) { }

  nodes = [];

  pickedDirectory = null;
  pickedBimDirectory = null;

  pickedDirectoryName = null;
  pickedBimDirectoryName = null;

  type = 'case';

  @ViewChild(TreeComponent)
  private tree: TreeComponent;

  optionsTree = {
    getChildren: (node: TreeNode) => {
      return new Promise((resolve, reject) => {
        const filter = new FilterModel();
        filter.CaseId = node.data.id;
        this.taskService.GetFilteredTasks(filter).subscribe(
          tasks => {
            const nodes = [];
            tasks.forEach(task => {
              nodes.push({ name: task.Name + ' ' + task.Order, id: task.Id, hasChildren: false });
            });
            resolve(nodes);
          }
        );
      });
    }
  };

  ngOnInit() {
    this.caseService.GetFilteredCases(new FilterModel()).subscribe(
      res => {
        res.forEach(
          caseModel => this.nodes.push({ name: caseModel.Name + ' ' + caseModel.Order, id: caseModel.Id, hasChildren: true })
        );
        this.tree.treeModel.update();
      }
    )

      this.forgeService.folderIdSubject.subscribe(
        res => {
          const pathArr = [];
          let finalPath = '';
          this.createPath(res.node, pathArr);
          pathArr.forEach(path => finalPath = finalPath + path + '||');
          this.pickedBimDirectory = finalPath;
          this.pickedBimDirectoryName = res.Name;
        }
      );
  }

  pickDirectory(node) {
    this.pickedDirectory = node.node.data.id;
    this.pickedDirectoryName = node.node.data.name;
    if (node.node.data.hasChildren) {
      this.type = 'case';
    } else {
      this.type = 'task';
    }
  }

  createPath(node: TreeNode, pathArr: string[]) {
    if (node.parent.displayField != null) {
      pathArr.push(node.data.id);
      this.createPath(node.parent, pathArr);
    } else {
      pathArr.push(node.data.id);
      return pathArr;
    }
  }

  setBimFolder() {
    switch (this.type) {
      case 'case':
        this.caseService.SetBimFolder(this.pickedDirectory, this.pickedBimDirectory).subscribe(
          () => alert('Folder ustawiony pomyślnie'),
          () => alert('Wystąpił błąd przy ustawianiu folderu Bim360')
        );
      break;
      case 'task':
        this.taskService.SetBimFolder(this.pickedDirectory, this.pickedBimDirectory).subscribe(
          () => alert('Folder ustawiony pomyślnie'),
          () => alert('Wystąpił błąd przy ustawianiu folderu Bim360')
        );
      break;
    }
  }

}

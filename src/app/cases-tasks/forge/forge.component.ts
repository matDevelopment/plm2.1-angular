import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ForgeService } from 'src/app/services/forge.service';
import { MAT_DIALOG_DATA } from '@angular/material';


declare var Autodesk: any;

@Component({
  selector: 'app-forge',
  templateUrl: './forge.component.html',
  styleUrls: ['./forge.component.scss']
})

export class ForgeComponent implements OnInit {

  constructor(private forgeService: ForgeService, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  token = '';
  authorized = false;

  ngOnInit() {
    console.log(this.data);
    this.forgeService.GetToken().subscribe(res => {
      this.authorized = true;
    },
    error => {
      this.authorized = false;
    }
    );
  }
}

import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ForgeService } from 'src/app/services/forge.service';
import { TreeNode } from 'angular-tree-component/dist/defs/api';
import { TreeComponent } from 'angular-tree-component';
@Component({
  selector: 'app-forge-files',
  templateUrl: './forge-files.component.html',
  styleUrls: ['./forge-files.component.scss']
})
export class ForgeFilesComponent implements OnInit {

  constructor(private forgeService: ForgeService) {
  }

  @ViewChild(TreeComponent)
  private tree: TreeComponent;

  token = '';

  nodes = [];

  @Input() forgeFolderId = 'root';

  @Input() height = '100%';

  optionsTree = {
    getChildren: (node: TreeNode) => {
      return new Promise((resolve, reject) => {
        this.forgeService.GetData(node.data.id).subscribe(
          res => {
            const nodes = []; res.forEach(resNode => {
              nodes.push({ name: resNode.text, id: resNode.id, hasChildren: resNode.children, type: resNode.type });
            });
            console.log(nodes);
            resolve(nodes);
          }
        );
      });
    }
  };

  ngOnInit() {
    this.getData();

    this.forgeService.sendFileSubject.subscribe(
      filePath => {
        if (this.forgeFolderId !== null) {
          this.forgeService.SendFileToBim360(this.forgeFolderId, filePath).subscribe(
            res => alert('plik został dodany do Bim360')
          );
        } else {
          alert('należy wybrać folder bim360')
        }
      }
    );
  }

  getData() {
    let path = this.forgeFolderId;
    const splitedString = this.forgeFolderId.split('||');
    if (this.forgeFolderId !== 'root' && splitedString.length > 1) {
      path = splitedString[0];
    }
    this.forgeService.GetData(path).subscribe(
      res => {
        console.log(res);
        res.forEach(resNode => {
          this.nodes.push({ name: resNode.text, id: resNode.id, hasChildren: resNode.children, type: resNode.type });
        });
        this.tree.treeModel.update();
      }
    );
  }

  loadFile(event) {
    if (event.node.hasChildren) {
      this.forgeService.folderIdSubject.next({ Id: event.node.id, Name: event.node.displayField, node: event.node });
    } else {
      this.forgeService.GetToken().subscribe(res => {
        let urn;
        let viewableId;
        console.log(event.node.id);
        if (event.node.id.indexOf('|') > -1) {
          urn = event.node.id.split('|')[1];
          viewableId = event.node.id.split('|')[2];
        }
        else {
          urn = event.node.id;
          viewableId = undefined;
        }
        if (!event.node.hasChildren) {
          this.forgeService.loadFileSubject
          .next({ urn: urn, accessToken: res.access_token, viewableId: viewableId, node: event.node, parents: this.forgeFolderId });
        }
      });
    }
  }

}

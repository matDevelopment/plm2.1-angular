import { Subscription } from 'rxjs';
import { ForgeService } from './../../../services/forge.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';

declare var Autodesk: any;
declare var av: any;

@Component({
  selector: 'app-forge-viewer',
  templateUrl: './forge-viewer.component.html',
  styleUrls: ['./forge-viewer.component.scss']
})
export class ForgeViewerComponent implements OnInit, OnDestroy {

  constructor(private forgeService: ForgeService) { }

  viewer = null;
  viewableId = undefined;
  loadFileSubscription = new Subscription();

  pickedNodeId = '';
  pickedNodeType = '';
  nodeParents = [];

  ngOnInit() {
    this.loadFileSubscription = this.forgeService.loadFileSubject.subscribe(
      res => {
        Viewer.nodeId = res.node.data.id;
        Viewer.nodeType = res.node.data.type;
        Viewer.forgeService = this.forgeService;
        const arr = res.parents.split('||');
        arr.push('#');
        arr.splice(arr.length - 1, 1);
        Viewer.nodeParents = arr;
        this.bim360Issues();
        this.launchViewer(res.urn, res.viewableId, res.accessToken);
      }
    );
  }

  launchViewer(urn, viewableId, accessToken) {
    if (Viewer.app != null) {
      var thisviewer = Viewer.app.getCurrentViewer();
      if (thisviewer) {
        thisviewer.tearDown();
        thisviewer.finish();
        thisviewer = null;
      }
    }

    var options = {
      env: 'AutodeskProduction',
      accessToken: accessToken,
    };
    var documentId = 'urn:' + urn;
    Autodesk.Viewing.Initializer(options, () => {
      Viewer.app = new Autodesk.Viewing.ViewingApplication('forgeViewer');
      Viewer.app.registerViewer(Viewer.app.k3D, Autodesk.Viewing.Private.GuiViewer3D,
        { extensions: ['BIM360IssueExtension'] });
        Viewer.app.loadDocument(documentId, (doc) => this.initializeViewer(doc, viewableId)
        , (viewerErrorCode) => this.onDocumentLoadFailure(viewerErrorCode));
    });
  }

  initializeViewer(doc, viewableId) {
    // We could still make use of Document.getSubItemsWithProperties()
    // However, when using a ViewingApplication, we have access to the **bubble** attribute,
    // which references the root node of a graph that wraps each object from the Manifest JSON.
    var viewables = Viewer.app.bubble.search({ 'type': 'geometry' });
    if (viewables.length === 0) {
      console.error('Document contains no viewables.');
      return;
    }

    if (viewableId !== undefined) {
      viewables.forEach((viewable) => {
        if (viewable.data.viewableID == viewableId) {
          Viewer.app.selectItem(viewable.data, (viewers, item) => this.onItemLoadSuccess(viewers, item),
            (errorCode) => this.onItemLoadFail(errorCode));
        }
      });
    } else {
      Viewer.app.selectItem(viewables[0].data, (viewers, item) => this.onItemLoadSuccess(viewers, item),
        (errorCode) => this.onItemLoadFail(errorCode));
    }
  }

  onDocumentLoadFailure(viewerErrorCode) {
    console.error('onDocumentLoadFailure() - errorCode:' + viewerErrorCode);
  }

  onItemLoadSuccess(viewer, item) {
    // item loaded, any custom action?
  }

  onItemLoadFail(errorCode) {
    console.error('onItemLoadFail() - errorCode:' + errorCode);
  }

  ngOnDestroy(): void {
    if (Viewer.app != null) {
      let thisviewer = Viewer.app.getCurrentViewer();
      if (thisviewer) {
        thisviewer.tearDown();
        thisviewer.finish();
        thisviewer = null;
      }
    }
    this.loadFileSubscription.unsubscribe();
    Viewer.app = null
  }

  bim360Issues() {

    function BIM360IssueExtension(viewer, options) {
      Autodesk.Viewing.Extension.call(this, viewer, options);
      this.viewer = viewer;
      this.panel = null; // create the panel variable
      this.containerId = null;
      this.hubId = null;
      this.issues = null;
      this.pushPinExtensionName = 'Autodesk.BIM360.Extension.PushPin';
    }

    BIM360IssueExtension.prototype = Object.create(Autodesk.Viewing.Extension.prototype);
    BIM360IssueExtension.prototype.constructor = BIM360IssueExtension;

    BIM360IssueExtension.prototype.load = function () {
      if (this.viewer.toolbar) {
        // Toolbar is already available, create the UI
        this.createUI();
      } else {
        // Toolbar hasn't been created yet, wait until we get notification of its creation
        this.onToolbarCreatedBinded = this.onToolbarCreated.bind(this);
        this.viewer.addEventListener(av.TOOLBAR_CREATED_EVENT, this.onToolbarCreatedBinded);
      }
      return true;
    };

    BIM360IssueExtension.prototype.onToolbarCreated = function () {
      this.viewer.removeEventListener(av.TOOLBAR_CREATED_EVENT, this.onToolbarCreatedBinded);
      this.onToolbarCreatedBinded = null;
      this.createUI();
    };

    BIM360IssueExtension.prototype.createUI = function () {
      var _this = this;

      // SubToolbar
      this.subToolbar = (this.viewer.toolbar.getControl("MyAppToolbar") ?
        this.viewer.toolbar.getControl("MyAppToolbar") :
        new Autodesk.Viewing.UI.ControlGroup('MyAppToolbar'));
      this.viewer.toolbar.addControl(this.subToolbar);

      // load/render issues button
      {
        var loadQualityIssues = new Autodesk.Viewing.UI.Button('loadQualityIssues');
        loadQualityIssues.onClick = function (e) {
          // check if the panel is created or not
          if (_this.panel == null) {
            _this.panel = new BIM360IssuePanel(_this.viewer, _this.viewer.container, 'bim360IssuePanel', 'BIM 360 Issues');
          }
          // show/hide docking panel
          _this.panel.setVisible(!_this.panel.isVisible());

          // if panel is NOT visible, exit the function
          if (!_this.panel.isVisible()) return;

          // ok, it's visible, let's load the issues
          _this.loadIssues();
        };
        loadQualityIssues.addClass('loadQualityIssues');
        loadQualityIssues.setToolTip('Show Issues');
        this.subToolbar.addControl(loadQualityIssues);
      }

      // create quality issue
      {
        var createQualityIssues = new Autodesk.Viewing.UI.Button('createQualityIssues');
        createQualityIssues.onClick = function (e) {
          var pushPinExtension = _this.viewer.getExtension(_this.pushPinExtensionName);
          if (pushPinExtension == null) {
            var extensionOptions = {
              hideRfisButton: true,
              hideFieldIssuesButton: true,
            };
            _this.viewer.loadExtension(_this.pushPinExtensionName, extensionOptions).then(function () { _this.createIssue(); });
          }
          else
            _this.createIssue(); // show issues
        };
        createQualityIssues.addClass('createQualityIssues');
        createQualityIssues.setToolTip('Create Issues');
        this.subToolbar.addControl(createQualityIssues);
      }
    };

    BIM360IssueExtension.prototype.createIssue = function () {
      var _this = this;
      var pushPinExtension = _this.viewer.getExtension(_this.pushPinExtensionName);

      var issueLabel = prompt("Enter issue label: ");
      if (issueLabel === null) return;

      // prepare to end creation...
      let listener = pushPinExtension.pushPinManager.addEventListener('pushpin.created', function (e) {
        pushPinExtension.pushPinManager.removeEventListener('pushpin.created', listener);
        pushPinExtension.endCreateItem();
        // now prepare the data
        var selected = getSelectedNode();
        var target_urn = selected.urn.split('?')[0];
        var starting_version = Number.parseInt(selected.version);
        // https://forge.autodesk.com/en/docs/bim360/v1/tutorials/pushpins/create-pushpin/
        // Once the user clicks the ``Create Pushpin`` button (see step 3), you need to grab the current position of the newly created pushpin and the pushpin data using its ID, which is automatically set to ``0``.
        var issue = pushPinExtension.getItemById('0');
        if (issue === null) return; // safeguard
        var data = {
          type: 'quality_issues',//issue.type,
          attributes: {
            title: issue.label, // In our example, this is the ``title`` the user sets in the form data (see step 3).
            // The extension retrieved the ``type`` and ``status`` properties in step 3, concatenated them, added a dash, and
            // assigned the new string to the ``status`` property of the newly created pushpin object. For example, ``issues-
            // open``.
            // You now need to extract the ``status`` (``open``) from the pushpin object.
            status: issue.status.split('-')[1] || issue.status,
            // The ``target_urn`` is the ID of the document (``item``) associated with an issue; see step 1.
            target_urn: target_urn,
            starting_version: starting_version, // See step 1 for the version ID.
            // The issue type ID and issue subtype ID. See GET ng-issue-types for more details.
            //ng_issue_subtype_id: "f6689e90-12ee-4cc8-af7a-afe10a37eeaa",
            ng_issue_type_id: "35f5c820-1e13-41e2-b553-0355b2b8b3dd",
            // ``sheet_metadata`` is the sheet in the document associated with the pushpin.
            sheet_metadata: { // `viewerApp.selectedItem` references the current sheet
              is3D: Viewer.app.selectedItem.is3D(),
              sheetGuid: Viewer.app.selectedItem.guid(),
              sheetName: Viewer.app.selectedItem.name()
            },
            pushpin_attributes: { // Data about the pushpin
              type: 'TwoDVectorPushpin', // This is the only type currently available
              object_id: issue.objectId, // (Only for 3D models) The object the pushpin is situated on.
              location: issue.position, // The x, y, z coordinates of the pushpin.
              viewer_state: issue.viewerState // The current viewer state. For example, angle, camera, zoom.
            },
          }
        };

        // submit data
        _this.getContainerId(selected.project, selected.urn, function () {
          const urn = btoa(target_urn.split('?')[0]);
          console.log(data);
          Viewer.forgeService.SubmitBim360data(_this.containerId, urn, data).subscribe(
            () => _this.loadIssues()
          );
        });
      });

      // start asking for the push location
      pushPinExtension.startCreateItem({ label: issueLabel, status: 'open', type: 'issues' });
    }

    BIM360IssueExtension.prototype.submitNewIssue = function () {

    };


    BIM360IssueExtension.prototype.unload = function () {
      this.viewer.toolbar.removeControl(this.subToolbar);
      return true;
    };

    Autodesk.Viewing.theExtensionManager.registerExtension('BIM360IssueExtension', BIM360IssueExtension);


    // *******************************************
    // BIM 360 Issue Panel
    // *******************************************
    function BIM360IssuePanel(viewer, container, id, title, options?) {
      this.viewer = viewer;
      Autodesk.Viewing.UI.PropertyPanel.call(this, container, id, title, options);
    }
    BIM360IssuePanel.prototype = Object.create(Autodesk.Viewing.UI.PropertyPanel.prototype);
    BIM360IssuePanel.prototype.constructor = BIM360IssuePanel;

    // *******************************************
    // Issue specific features
    // *******************************************
    BIM360IssueExtension.prototype.loadIssues = function (containerId, urn) {

      //probably it is unneccesary to get container id and urn again
      //because Pushpin initialization has done.
      //but still keep these line 
      var _this = this;
      var selected = getSelectedNode();

      _this.getContainerId(selected.project, selected.urn, function () {
        _this.getIssues(_this.hubId, _this.containerId, selected.urn, true);
      });
    }

    BIM360IssueExtension.prototype.getContainerId = function (href, urn, cb) {
      var _this = this;
      if (_this.panel) {
        _this.panel.removeAllProperties();
        _this.panel.addProperty('Loading...', '');
      }

      Viewer.forgeService.GetBimContainer(href).subscribe(
        res => {
          _this.containerId = res.ContainerId;
          _this.hubId = res.HubId;
          cb();
        }
      )
    }

    BIM360IssueExtension.prototype.getIssues = function (accountId, containerId, urn) {
      var _this = this;
      urn = urn.split('?')[0]
      urn = btoa(urn);

      Viewer.forgeService.GetBimIssues(accountId, containerId, urn).subscribe(data => {
        _this.issues = data;

        // do we have issues on this document?
        var pushPinExtension = _this.viewer.getExtension(_this.pushPinExtensionName);
        if (_this.panel) _this.panel.removeAllProperties();
        if (data.length > 0) {
          if (pushPinExtension == null) {
            var extensionOptions = {
              hideRfisButton: true,
              hideFieldIssuesButton: true,
            };
            _this.viewer.loadExtension(_this.pushPinExtensionName, extensionOptions).then(function () { _this.showIssues(); }); // show issues (after load extension)
          }
          else
            _this.showIssues(); // show issues
        }
        else {
          if (_this.panel) _this.panel.addProperty('No issues found', 'Use create issues button');
        }
      },
        () => alert('Cannot read Issues')
      )

    }

    BIM360IssueExtension.prototype.showIssues = function () {
      var _this = this;

      //remove the list of last time
      var pushPinExtension = _this.viewer.getExtension(_this.pushPinExtensionName);
      pushPinExtension.removeAllItems();
      pushPinExtension.showAll();
      var selected = getSelectedNode();

      _this.issues.forEach(function (issue) {
        var dateCreated = moment(issue.attributes.created_at);

        // show issue on panel
        if (_this.panel) {
          _this.panel.addProperty('Title', issue.attributes.title, 'Issue ' + issue.attributes.identifier);
          //_this.panel.addProperty('Location', stringOrEmpty(issue.attributes.location_description), 'Issue ' + issue.attributes.identifier);
          _this.panel.addProperty('Version', 'V' + issue.attributes.starting_version + (selected.version != issue.attributes.starting_version ? ' (Not current)' : ''), 'Issue ' + issue.attributes.identifier);
          _this.panel.addProperty('Created at', dateCreated.format('MMMM Do YYYY, h:mm a'), 'Issue ' + issue.attributes.identifier);
          _this.panel.addProperty('Assigned to', issue.attributes.assigned_to_name, 'Issue ' + issue.attributes.identifier);
        }

        // add the pushpin
        var issueAttributes = issue.attributes;
        var pushpinAttributes = issue.attributes.pushpin_attributes;
        if (pushpinAttributes) {
          issue.type = issue.type.replace('quality_', ''); // temp fix during issues > quality_issues migration
          pushPinExtension.createItem({
            id: issue.id,
            label: issueAttributes.identifier,
            status: issue.type && issueAttributes.status.indexOf(issue.type) === -1 ? `${issue.type}-${issueAttributes.status}` : issueAttributes.status,
            position: pushpinAttributes.location,
            type: issue.type,
            objectId: pushpinAttributes.object_id,
            viewerState: pushpinAttributes.viewer_state
          });
        }
      })
    }

    function getSelectedNode() {
      var node = ''
      var parent;
      for (var i = 0; i < Viewer.nodeParents.length; i++) {
        var p = Viewer.nodeParents[i];
        if (p.indexOf('hubs') > 0 && p.indexOf('projects') > 0) parent = p;
      }

      if (Viewer.nodeId.indexOf('|') > -1) { // Plans folder
        var params = Viewer.nodeId.split("|");
        return { 'project': parent, 'urn': params[0], 'version': params[3] };
      }
      else { // other folders
        for (var i = 0; i < Viewer.nodeParents.length; i++) {
          var parent = Viewer.nodeParents[i];
          if (parent.indexOf('hubs') > 0 && parent.indexOf('projects') > 0) {
            var version = atob(Viewer.nodeId.replace('_', '/')).split('=')[1]
            return { 'project': parent, 'urn': (Viewer.nodeType == 'versions' ? id(Viewer.nodeParents[0]) : ''), version: version };
          }
        }
      }
      return null;
    }

    function id(href) {
      return href.substr(href.lastIndexOf('/') + 1, href.length);
    }

    function stringOrEmpty(str) {
      if (str == null) return '';
      return str;
    }

  }

}

export class Viewer {
  public static app: any;
  public static forgeService;
  public static nodeId;
  public static nodeType;
  public static nodeParents;
}

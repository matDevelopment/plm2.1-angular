import { PriorityModel } from './../../models-database/priority.model';
import { FilterService } from './../../services/filter.service';
import { Component, OnInit, Inject } from '@angular/core';
import { StatusModel } from 'src/app/models-database/status.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FilterModel } from 'src/app/models/filter.model';

@Component({
  selector: 'app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.scss']
})
export class FilterPanelComponent implements OnInit {

  constructor(private filterService: FilterService, public filterDialogRef: MatDialogRef<FilterPanelComponent>,
    @Inject(MAT_DIALOG_DATA) public data) { }

  priorities: PriorityModel[] = [];
  statuses: StatusModel[] = [];

  newFilter: FilterModel = this.data.Filter;

  ngOnInit() {
    this.filterService.GetPriorities().subscribe(
      priorities => this.priorities = priorities
    );
    this.filterService.GetStatuses().subscribe(
      statuses => this.statuses = statuses
    );
    this.newFilter.StartDate =
    new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), 0, 0, 0).toDateString();
    this.newFilter.EndDate =
    new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDay(), 0, 0, 0).toDateString();
  }

  changeStatus(id: number) {
    this.newFilter.StatusId = id;
  }

  changePriority(id: number) {
    this.newFilter.PriorityId = id;
  }

  applyFilters() {
    this.filterService.filtersSubject.next(this.newFilter);
    this.filterDialogRef.close();
  }

}

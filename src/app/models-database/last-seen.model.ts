import { UserModel } from './user.model';
import { TaskModel } from './task.model';
import { CaseModel } from './case.model';

export class LastSeenModel {
    constructor() {}
    public Id: number;
    public UserId: number;
    public User: UserModel;
    public TaskId: number;
    public Task: TaskModel;
    public CaseId: number;
    public Case: CaseModel;
    public Type: string;
}

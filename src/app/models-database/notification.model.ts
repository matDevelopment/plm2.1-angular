import { UserModel } from './user.model';
import { CaseModel } from './case.model';
import { TaskModel } from './task.model';
import { MemoModel } from './memo.model';

export class NotificationModel {
    constructor() { }

    Id: number;
    TaskId: number;
    CaseId: number;
    MemoId: number;
    Message: string;
    SendTime: string;
    Seen: boolean;
    UserId: number;
    Task: TaskModel;
    Case: CaseModel;
    User: UserModel;
    Memo: MemoModel;
}

import { UserModel } from './user.model';

export class GroupModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public Avatar: string;
    public TeamLeaderId: number;
    public Users: UserModel[];
    public UsersAmount: number;
}

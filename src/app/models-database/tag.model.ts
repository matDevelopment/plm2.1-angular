export class TagModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public CaseTagParentId: number;
    public TaskTagMainId: number;
    public TaskTagAccessoryId: number;
}

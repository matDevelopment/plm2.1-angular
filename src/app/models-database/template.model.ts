import { PriorityModel } from './priority.model';
import { ClientModel } from './client.model';
import { TagModel } from './tag.model';
import { RepresentativeModel } from './representative.model';
import { TaskTemplateModel } from './task-template.model';

export class TemplateModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public CaseName: string;
    public Order: string;
    public PriorityId: number;
    public ClientId: number;
    public TimeEstimated: number;
    public CaseTagParentId: number;
    public CaseTagChildId: number;
    public RepresentativeId: number;
    public CasesCount: number;
    public Priority: PriorityModel;
    public Client: ClientModel;
    public CaseTagParent: TagModel;
    public CaseTagChild: TagModel;
    public Representative: RepresentativeModel;
    public TaskTemplates: TaskTemplateModel[];
}

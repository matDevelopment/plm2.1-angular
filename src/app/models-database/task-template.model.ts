import { PriorityModel } from './priority.model';
import { TagModel } from './tag.model';
import { CaseModel } from './case.model';
import { TemplateModel } from './template.model';

export class TaskTemplateModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public TaskName: string;
    public CaseId: number;
    public PriorityId: number;
    public TimeEstimated: number;
    public NameCount: number;
    public TaskTagMainId: number;
    public TaskTagAccessoryId: number;
    public TaskTagAccessoryMinorId: number;
    public TaskTagMain: TagModel;
    public TaskTagAccessory: TagModel;
    public TaskTagAccessoryMinor: TagModel;
    public Priority: PriorityModel;
    public Case: CaseModel;
    public CaseTagId: number;
    public TemplateId: number;
    public Template: TemplateModel;
}

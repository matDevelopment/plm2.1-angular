import { UserModel } from './user.model';

export class PaymentPeriodModel {
    public StartJsDate;
    public FinishJSDate;
    public Id: number;
    public UserId: number;
    public User: UserModel;
    public StartDate: string;
    public FinishDate: string;
    public Netto: number;
    public Brutto: number;
    constructor() {
        this.StartJsDate = this.converCDateToDate(this.StartDate);
        this.FinishJSDate = this.converCDateToDate(this.FinishDate);
    }

    converCDateToDate(date: string) {
        const dateSplit = date.split('T');
        const hourAndMinutes = dateSplit[1].split(':');
        const yearMonthDay = dateSplit[0].split('-');
        return new Date( parseInt(yearMonthDay[0], 10), parseInt(yearMonthDay[1], 10) - 1, parseInt(yearMonthDay[2], 10),
        parseInt(hourAndMinutes[0], 10), parseInt(hourAndMinutes[1], 10));
    }
}

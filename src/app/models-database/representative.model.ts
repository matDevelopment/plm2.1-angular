export class RepresentativeModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public Surname: string;
    public Email: string;
    public Phone: number;
    public ClientId: number;
}

import { PaymentPeriodModel } from './payment-period.model';

export class UserModel {
    constructor( ) {}
    EcpCounted: number;
    public Id: number;
    public Name: string;
    public Email: string;
    public FirstName: string;
    public Surname: string;
    public Roles: string[];
    public PaymentPeriods: PaymentPeriodModel[];
    public IsVisible: boolean;
}

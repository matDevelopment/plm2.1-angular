import { CaseModel } from 'src/app/models-database/case.model';
import { PriorityModel } from './priority.model';
import { StatusModel } from './status.model';
import { UserModel } from './user.model';
import { GroupModel } from './group.model';
import { TagModel } from './tag.model';

export class TaskModel  {
    constructor() { }
    public Id: number;
    public Name: string;
    public Order: string;
    public Description: string;
    public StatusId: number;
    public PriorityId: number;
    public StartDate: string;
    public EndDate: string;
    public TimeEstimated: number;
    public TimeTaken: number;
    public buttonExpanded: boolean;
    public Status: StatusModel;
    public Priority: PriorityModel;
    public UserId: number;
    public GroupId: number;
    public TaskTagMainId: number;
    public TaskTagAccessoryId: number;
    public TaskTagAccessoryMinorId: number;
    public CaseId: number;
    public Case: CaseModel;
    public Users: UserModel[];
    public Group: GroupModel;
    public TaskTagMain: TagModel;
    public TaskTagAccessory: TagModel;
    public TaskTagAccessoryMinor: TagModel;
    public DestinatedEmployees: number;
    public FolderId: string;
    public MemoCount: string;
    public BimFolderId: string;
}

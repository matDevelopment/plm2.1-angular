
export class ClientModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public Email: string;
    public PhoneNumber: string;
    public Nip: number;
}

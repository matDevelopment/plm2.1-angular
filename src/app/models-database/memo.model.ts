import { GroupModel } from './group.model';
import { UserModel } from './user.model';

export class MemoModel {
    constructor(public Id: number, public ParentId: number, public Title: string, public Description: string, public Type: string,
    public GroupId: number, public Group: GroupModel, public Time: Date, public ReporterId: number, public Reporter: UserModel,
    public AssigneeId: number, public Assignee: UserModel) {
        this.Time = new Date();
    }
    public FolderId: string;
}

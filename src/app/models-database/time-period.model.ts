export class TimePeriodModel {
    constructor( public Id: number,
        public StartTime: string,
        public BackgroundColor: string,
        public AllowEdit: boolean,
        public CollectionId: number,
        public FromDataBase: boolean,
        public CaseId: number,
        public TaskId: number
        ) {}
}

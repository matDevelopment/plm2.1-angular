import { ClientModel } from './client.model';
import { PriorityModel } from './priority.model';
import { StatusModel } from './status.model';
import { TagModel } from './tag.model';
import { UserModel } from './user.model';
import { GroupModel } from './group.model';
import { RepresentativeModel } from './representative.model';

export class CaseModel  {
    constructor() { }
    public Id: number;
    public Name: string;
    public Order: string;
    public TasksCount: number;
    public Description: string;
    public StatusId: number;
    public PriorityId: number;
    public StartDate: string;
    public EndDate: string;
    public TimeEstimated: number;
    public TimeTaken: number;
    public buttonExpanded: boolean;
    public ClientId: number;
    public UserId: number;
    public GroupId: number;
    public CaseTagParentId: number;
    public CaseTagChildId: number;
    public User: UserModel;
    public Group: GroupModel;
    public Status: StatusModel;
    public Priority: PriorityModel;
    public Client: ClientModel;
    public CaseTagParent: TagModel;
    public CaseTagChild: TagModel;
    public TasksTime: number;
    public RepresentativeId: number;
    public Representative: RepresentativeModel;
    public TemplateId: number;
    public FolderId: string;
    public MemoCount: number;
    public BimFolderId: string;
}

export class StatusModel {
    constructor() {}
    public Id: number;
    public Name: string;
    public BackgroundColor: string;
    public Color: string;
    public PosX: number;
    public PosY: number;
}

import { Component, OnInit, Input } from '@angular/core';
import { faObjectGroup, faClone, faChartBar, faUser, faCalendarAlt, faUserCircle, faMap } from '@fortawesome/free-solid-svg-icons';
import { faProjectDiagram, faUserFriends, faUserTie, faSignInAlt, faSignOutAlt, faTags, faTable } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-menu-mobile',
  templateUrl: './menu-mobile.component.html',
  styleUrls: ['./menu-mobile.component.scss']
})
export class MenuMobileComponent implements OnInit {

  faObjectGroup = faClone; faChartBar = faChartBar; faStickyNote = faProjectDiagram; faUser = faUser; faUserFriends = faUserFriends;
  faCalendar = faCalendarAlt; faUserTie = faUserTie; faSignInAlt = faSignInAlt; faSignOutAlt = faSignOutAlt; faTags = faTags;
  faUserCircle = faUserCircle; faTable = faTable; faMap = faMap;

  @Input() expanded;

  constructor() { }

  ngOnInit() {
  }

}

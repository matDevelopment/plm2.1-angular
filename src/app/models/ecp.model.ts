import { CaseModel } from '../models-database/case.model';
import { TaskModel } from '../models-database/task.model';
import { UserModel } from '../models-database/user.model';

export class EcpModel {

    constructor() { }

    public Id: number; public UserId: number;
    public CaseId: number; public TaskId: number; public Date: string;
    public Description: string;
    public StartTime: string;
    public FinishTime: string;
    public Case: CaseModel;
    public Task: TaskModel;
    public User: UserModel;
    public StartTimeHours: string;
    public StartTimeMinutes: string;
    public FinishTimeHours: string;
    public FinishTimeMinutes: string;
    public TimeWorkedHours: number;
    public TimeWorkedMinutes: number;
    StartTimeJs: Date;
    FinishTimeJs: Date;
    AllowEdit: boolean;

}

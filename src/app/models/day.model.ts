export class DayModel {
    constructor (public day: number, public date: Date, public IsTransparent: boolean, public IsTimeReported: boolean) {}

    public TimeWorked: string;
}

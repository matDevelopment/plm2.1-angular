export class PathModel {
    public static readonly Url = window.location.origin;
    public static readonly UrlWithOutHttp = window.location.origin.replace('http://', '');
}

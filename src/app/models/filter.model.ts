export class FilterModel {

constructor() {
        this.TeamLeaderId = null;
    }
    TeamLeaderId: number;
    public PriorityId: number;
    public StatusId: number;
    public StartDate: string;
    public EndDate: string;
    public TimeEstimated: number;
    public TimeTaken: number;
    public UserId: number;
    public GroupId: number;
    public CaseId: number;
    public TaskUserId: number;
    public Date: string;
    public UserName: string;
    public TaskId: number;
    public StartTime: string;
    public FinishTime: string;
    public EcpDate: string;
    public CaseTagParentId: number;
    public CaseTagChildId: number;
    public TaskTagMainId: number;
    public TaskTagAccessoryId: number;
    public TaskTagAccessoryMinorId: number;
    public LastSeenUser: number;
}

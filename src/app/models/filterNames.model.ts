export class FilterNamesModel {

    constructor() { }

    public Priority: string;
    public Status: string;
    public Date: string;
    public EcpDate: string;
    public StartDate: string;
    public EndDate: string;
    public StartTime: string;
    public FinishTime: string;
    public TimeEstimated: string;
    public TimeTaken: string;
    public User: string;
    public Group: string;
    public Case: string;
    public Task: string;
    public TaskUser: string;
    public UserName: string;
    public CaseTagParent: string;
    public CaseTagChild: string;
    public TaskTagMain: string;
    public TaskTagAccessory: string;
    public TaskTagAccessoryMinor: string;
    public TeamLeader: string;
    public LastSeenUser: string;

}

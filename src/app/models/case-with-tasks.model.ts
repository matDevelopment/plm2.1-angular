import { CaseModel } from '../models-database/case.model';
import { TaskModel } from '../models-database/task.model';

export class CaseWithTasksModel {
    constructor (public Case: CaseModel, public Tasks: TaskModel[], public expanded: boolean) {}
}

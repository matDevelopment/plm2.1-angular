import { EcpModel } from './ecp.model';
import { TaskModel } from '../models-database/task.model';

export class TableSubDataModel {

    constructor(subModel: any, ecps: EcpModel[]) {
        Object.keys(subModel).forEach(key => this[key] = subModel[key]);
        this.EcpCounted = 0;
        ecps.forEach(ecp => {
            ecp.StartTimeJs = this.converCDateToDate(ecp.StartTime);
            ecp.FinishTimeJs = this.converCDateToDate(ecp.FinishTime);
            this.EcpCounted = this.EcpCounted + (ecp.FinishTimeJs.getTime() - ecp.StartTimeJs.getTime());
          });
        if (subModel.Status !== null && subModel.Status !== undefined) {
            this.StatusName = subModel.Status.Name;
            this.CaseName = subModel.Case.Name;
            this.Description !== null ? this.Description = this.Description.substring(0, 20) + '...' : this.Description = '';
            this.TimeDiference = subModel.TimeEstimated - subModel.TimeTaken;
        }
        this.EcpCounted = this.EcpCounted / (1000 * 60 * 60);
    }

    SubModel: any;
    Ecps: EcpModel[];
    EcpCounted;
    StatusName: string;
    TimeDiference: number;
    StartDateJs: Date;
    EndDateJs: Date;
    CaseName: string;
    Description: string;

    converCDateToDate(date: string) {
        const dateSplit = date.split('T');
        const hourAndMinutes = dateSplit[1].split(':');
        const yearMonthDay = dateSplit[0].split('-');
        return new Date(parseInt(yearMonthDay[0], 10), parseInt(yearMonthDay[1], 10) - 1, parseInt(yearMonthDay[2], 10),
            parseInt(hourAndMinutes[0], 10), parseInt(hourAndMinutes[1], 10));
    }
}

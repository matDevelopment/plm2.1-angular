import { CaseModel } from '../models-database/case.model';

export class CasesViewModel {
    constructor (public Cases: CaseModel[], public Type: string) {}
}

export class ButtonModel {

    constructor() { }

    public Id: number;
    public Text: string;
    public BackgroundColor: string;
    public Color: string;
    public PosY: number;
    public PosX: number;

}

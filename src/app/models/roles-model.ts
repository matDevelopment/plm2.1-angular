export class RolesModel {
    public static readonly Admin = 'Administrator';
    public static readonly ProjectManager = 'Menadżer Projektu';
    public static readonly TeamLeader = 'Lider Zespołu';
    public static readonly User = 'Użytkownik';
    public static readonly Roles = [ RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader, RolesModel.User ];
}

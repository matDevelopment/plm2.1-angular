import { EcpModel } from './ecp.model';
import { TableSubDataModel } from './table-sub-data.model';

export class TableDataModel {

    constructor(mainModel: any, subModels: any[], ecps: EcpModel[], compareBy: string, isUserTable: boolean) {
        Object.keys(mainModel).forEach(key => this[key] = mainModel[key]);
        this.Ecps = ecps;
        this.SubModels = [];
        subModels.forEach(subModel => {
            const newEcps: EcpModel[] = [];
            if (isUserTable) {
                ecps.forEach(ecp => {
                    if (ecp[compareBy].Id === subModel.Id && ecp.UserId === mainModel.Id) { newEcps.push(ecp); }
                });
            }
            else {
                ecps.forEach(ecp => {
                    if (ecp[compareBy].Id === subModel.Id) { newEcps.push(ecp); }
                });
            }
            const newSubModel = new TableSubDataModel(subModel, newEcps);
            this.SubModels.push(newSubModel);
        });
        this.EcpCounted = 0;
        if (isUserTable) {
            this.Ecps.forEach(ecp => {
                ecp.StartTimeJs = this.converCDateToDate(ecp.StartTime);
                ecp.FinishTimeJs = this.converCDateToDate(ecp.FinishTime);
                if(ecp.UserId === mainModel.Id) {
                    this.EcpCounted = this.EcpCounted + (ecp.FinishTimeJs.getTime() - ecp.StartTimeJs.getTime());
                }
            });
        } else {
            this.Ecps.forEach(ecp => {
                ecp.StartTimeJs = this.converCDateToDate(ecp.StartTime);
                ecp.FinishTimeJs = this.converCDateToDate(ecp.FinishTime);
                this.EcpCounted = this.EcpCounted + (ecp.FinishTimeJs.getTime() - ecp.StartTimeJs.getTime());
            });
        }
        if (mainModel.TimeEstimated !== null && mainModel.TimeEstimated !== undefined) {
            this.StatusName = mainModel.Status.Name;
            this.TimeDiference = mainModel.TimeEstimated - mainModel.TimeTaken;
            this.Description !== null ? this.Description = this.Description.substring(0, 20) + '...' : this.Description = '';
            this.StartDateJs = this.converCDateToDate(mainModel.StartDate);
            this.EndDateJs = this.converCDateToDate(mainModel.EndDate);
        }
        this.EcpCounted = this.EcpCounted / (1000 * 60 * 60);
    }
    MainModel: any;
    SubModels: any[];
    Ecps: EcpModel[];
    EcpCounted;
    StatusName: string;
    TimeDiference: number;
    StartDateJs: Date;
    EndDateJs: Date;
    Description: string;
    pickedSubSort = '';
    pickedSubReverse = '';

    converCDateToDate(date: string) {
        const dateSplit = date.split('T');
        const hourAndMinutes = dateSplit[1].split(':');
        const yearMonthDay = dateSplit[0].split('-');
        return new Date(parseInt(yearMonthDay[0], 10), parseInt(yearMonthDay[1], 10) - 1, parseInt(yearMonthDay[2], 10),
            parseInt(hourAndMinutes[0], 10), parseInt(hourAndMinutes[1], 10));
    }
}

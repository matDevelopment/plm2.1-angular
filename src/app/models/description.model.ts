export class DescriptionModel {
    constructor (public Name: string, public CollectionId: number, public StarTime: string, public EndTime: string,
        public FromDatabase: boolean, public CaseId: number, public TaskId: number) {}
}

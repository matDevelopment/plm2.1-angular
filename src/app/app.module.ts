import { ForgeAuthComponent } from './menu/auth/forge-auth/forge-auth.component';
import { AuthComponent } from './menu/auth/auth.component';
import { AuthService } from './services/auth.service';
import { TemplatesModule } from './templates/templates.module';
import { DateService } from './services/date.service';
import { CasesTasksModule } from './cases-tasks/cases-task.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TreeModule } from 'angular-tree-component';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { MenuMobileComponent } from './menu-mobile/menu-mobile.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import 'hammerjs';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { GroupsUsersModule } from './groups-users/groups-users.module';
import { EcpModule } from './ecp/ecp.module';
import { TimePeriodService } from './services/time-period.service';
import { MatFormFieldModule, MatInputModule, MatTreeModule, MatExpansionModule, MatSlideToggleModule,
     MatCardModule,
     MAT_DATE_LOCALE,
     MatSelectModule,
     MatAutocompleteModule} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EcpService } from './services/ecp.service';
import { AuthGuard } from './guards/auth.guard';
import { SharedModule } from './shared/shared.module';
import { TablesModule } from './tables/tables.module';
import { EcpPanelNewComponent } from './ecp/ecp-panel-new/ecp-panel-new.component';
import { CreateEcpComponent } from './ecp/ecp-panel-new/create-ecp/create-ecp.component';
import { EcpCasesTasksComponent } from './ecp/ecp-panel-new/ecp-cases-tasks/ecp-cases-tasks.component';
import { DatePipe } from '@angular/common';
import { LastSeenService } from './services/last-seen.service';
import { LastSeenComponent } from './menu/auth/last-seen/last-seen.component';
import { ForgeService } from './services/forge.service';
import { JwtHttpInterceptor } from './services/JwtHttpInterceptor.service';
import { NotificationsComponent } from './menu/auth/notifications/notifications.component';
import { TagsEditPanelComponent } from './tags-edit-panel/tags-edit-panel.component';

/*export function tokenGetter() {
   return localStorage.getItem('token');
}*/

@NgModule({
   declarations: [
      AppComponent,
      MenuComponent,
      MenuMobileComponent,
      EcpPanelNewComponent,
      EcpCasesTasksComponent,
      CreateEcpComponent,
      AuthComponent,
      NotificationsComponent,
      LastSeenComponent,
      ForgeAuthComponent,
      TagsEditPanelComponent
   ],
   imports: [
      BrowserModule,
      ReactiveFormsModule,
      FontAwesomeModule,
      FormsModule,
      NgbModule,
      MatInputModule,
      EcpModule,
      TablesModule,
      SharedModule,
      FontAwesomeModule,
      CasesTasksModule,
      TemplatesModule,
      GroupsUsersModule,
      AppRoutingModule,
      HttpClientModule,
      MatFormFieldModule,
      MatSelectModule,
      MatTreeModule,
      MatExpansionModule,
      MatFormFieldModule,
      MatSelectModule,
      MatInputModule,
      MatSlideToggleModule,
      MatAutocompleteModule,
      /*JwtModule.forRoot(\r\nconfig
   ]
}),*/
      MatSlideToggleModule,
      MatCardModule
   ],
   providers: [
      DateService,
      TimePeriodService,
      AuthService,
      EcpService,
      AuthGuard,
      LastSeenService,
      DatePipe,
      ForgeService,
      [{ provide: MAT_DATE_LOCALE, useValue: 'pl-PL' }],
      { provide: HTTP_INTERCEPTORS, useClass: JwtHttpInterceptor, multi: true },

   ],
   entryComponents: [
      EcpPanelNewComponent
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }

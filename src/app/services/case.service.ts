import { FilterModel } from './../models/filter.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { CaseModel } from '../models-database/case.model';
import { RepresentativeModel } from '../models-database/representative.model';


@Injectable()
export class CaseService {

    constructor(private http: HttpClient) { }

    AddNewCaseSubject = new Subject<CaseModel>();
    CaseFiltersSubject = new Subject<{ Type: string, Id: number}>();
    EdtiCaseSubject = new Subject<CaseModel>();
    UpdateCasesSubject = new Subject<CaseModel[]>();
    RemoveCaseSubject = new Subject<CaseModel>();
    PickNewCaseSubject = new Subject<CaseModel>();

    AddNewCase(caseModel: CaseModel) {
        return this.http.post(PathModel.Url + '/api/cases/add', caseModel).pipe(
            map(res => {
                const _case = res as CaseModel;
                if (_case.RepresentativeId === null) {
                    _case.Representative = new RepresentativeModel();
                }
                return _case;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetCases() {
        return this.http.get(PathModel.Url + '/api/cases').pipe(
            map(res => {
                const cases = res as CaseModel[];
                cases.forEach(_case => { if (_case.RepresentativeId === null) { _case.Representative = new RepresentativeModel(); } });
                return cases;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }
    GetCaseById(id: Number) {
        return this.http.get(PathModel.Url + '/api/cases/get/' + id).pipe(
            map(res => {
                const _case = res as CaseModel;
                if (_case.RepresentativeId === null) {
                    _case.Representative = new RepresentativeModel();
                }
                return _case;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetFilteredCases(filterModel: FilterModel) {
        return this.http.post(PathModel.Url + '/api/cases/filter', filterModel).pipe(
            map(res => {
                const cases = res as CaseModel[];
                cases.forEach(_case => { if (_case.RepresentativeId === null) { _case.Representative = new RepresentativeModel(); } });
                return cases;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    EditCase(caseModel: CaseModel) {
        return this.http.post(PathModel.Url + '/api/cases/edit', caseModel).pipe(
            map(res => {
                const _case = res as CaseModel;
                if (_case.RepresentativeId === null) {
                    _case.Representative = new RepresentativeModel();
                }
                return _case;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveCase(caseModel: CaseModel) {
        return this.http.post(PathModel.Url + '/api/cases/remove', caseModel).pipe(
            map(res => {
                const _case = res as CaseModel;
                if (_case.RepresentativeId === null) {
                    _case.Representative = new RepresentativeModel();
                }
                return _case;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    ChangeStatus(caseModel: CaseModel) {
        return this.http.post(PathModel.Url + '/api/cases/status', caseModel).pipe(
            map(res => {
                const _case = res as CaseModel;
                if (_case.RepresentativeId === null) {
                    _case.Representative = new RepresentativeModel();
                }
                return _case;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    SetBimFolder(id: number, folderId: string) {
        return this.http.post(PathModel.Url + '/api/cases/bim360', { Id: id, FolderId: folderId }).pipe(
            map(res => {}),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }
}

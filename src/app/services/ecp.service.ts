import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EcpModel } from '../models/ecp.model';
import { PathModel } from '../models/path-model.model';
import { map, catchError} from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { FilterModel } from '../models/filter.model';
import { CaseModel } from '../models-database/case.model';
import { TaskModel } from '../models-database/task.model';

@Injectable({
  providedIn: 'root'
})
export class EcpService {

  constructor(private http: HttpClient) { }

  pickCaseTaskSubject = new Subject<{Case: CaseModel, Task: TaskModel}>();
  changeCaseTaskSubject = new Subject<{Case: CaseModel, Task: TaskModel}>();
  changeEcpDescriptionSubject = new Subject<string>();
  changeEcpForDescriptionSubject = new Subject<string>();
  allowDescriptionEdit = new Subject<boolean>();
  updateEcpSubject = new Subject<EcpModel[]>();
  getEcpsSubject = new Subject();
  loadTimelineSubject = new Subject<number>();
  changeTimelineDateSubject = new Subject<{StartTime: Date, FinishTime: Date}>();
  ChangeDateSubject = new Subject<{StartDate: Date, EndDate: Date}>();
  ChangeTaskTimeSubject = new Subject<number>();
  AddEcps(ecps: EcpModel[]) {
    return this.http.post(PathModel.Url + '/api/ecps/add',  ecps ).pipe(
      map(res => {
        return res as EcpModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetEcps(filter: FilterModel) {
    return this.http.post(PathModel.Url + '/api/ecps', filter).pipe(
      map(res => {
        console.log(res as EcpModel[]);
        return res as EcpModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetAllEcps() {
    return this.http.get(PathModel.Url + '/api/ecps/all').pipe(
      map(res => {
        return res as EcpModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveEcps(ecp: EcpModel) {
    return this.http.post(PathModel.Url + '/api/ecps/remove',  ecp ).pipe(
      map(res => {
        return res as EcpModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTodaysDate() {
    return this.http.get(PathModel.Url + '/api/ecps/date').pipe(
      map(res => {
        return res as string;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetReportedDaysInMonth(year: number, month: number) {
    return this.http.post(PathModel.Url + '/api/ecps/days-in-month', {Year: year, Month: month}).pipe(
      map(res => {
        return res as boolean[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetReportedDaysInMonthForUser(year: number, month: number, userId: number) {
    return this.http.post(PathModel.Url + '/api/ecps/days-in-month-user', {Year: year, Month: month, userId: userId}).pipe(
      map(res => {
        return res as any[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

}


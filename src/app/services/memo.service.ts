import { FilterModel } from './../models/filter.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { MemoModel } from '../models-database/memo.model';

@Injectable({
  providedIn: 'root'
})
export class MemoService {

constructor(private http: HttpClient) { }

    AddNewMemoSubject = new Subject<MemoModel>();
    UpdateMemoSubject = new Subject<MemoModel[]>();
    actionActivated = new Subject();

    AddNewMemo(memoModel: MemoModel) {
        console.log(PathModel.Url + '/api/memos/new');
        console.log(memoModel);
        return this.http.post(PathModel.Url + '/api/memos/new', memoModel).pipe(
          map(res => {
              return res as MemoModel;
          }),
          catchError(
              err => err.code === 404 ? throwError('Not Found') : throwError(err)
          )
      );
    }
    GetMemo(id: Number) {
      return this.http.get(PathModel.Url + '/api/memos/' + id).pipe(
        map(res => {
            return res as MemoModel;
        }),
        catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
    }
    GetMemosForUser(id: number) {
      return this.http.get(PathModel.Url + '/api/memos/user/' + id).pipe(
        map(res => {
            return res as MemoModel[];
        }),
        catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
  }

    GetMemosForParent(ParentId: number, Type: string) {
      return this.http.get(PathModel.Url + '/api/memos/' + Type + '/' + ParentId).pipe(
          map(res => {
              return res as MemoModel[];
          }),
          catchError(
              err => err.code === 404 ? throwError('Not Found') : throwError(err)
          )
      );
  }
  GetMemosForParentForUser(ParentId: number, Type: string, UserId: number) {
    return this.http.get(PathModel.Url + '/api/memos/' + Type + '/' + ParentId + '/' + UserId).pipe(
        map(res => {

          return res as MemoModel[];
        }),
        catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }
  UpdateMemo(memoModel: MemoModel) {
      return this.http.post(PathModel.Url + '/api/memos/' , memoModel).pipe(
        map(res => {
            return res as MemoModel;
        }),
        catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
  }
  DeleteMemo(memoModel: MemoModel) {
      return this.http.post(PathModel.Url + '/api/memos/delete/', memoModel).pipe(
        map(res => {
            return res as MemoModel;
        }),
        catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
  }

}

/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TimePeriodService } from './time-period.service';

describe('Service: TimePeriod', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimePeriodService]
    });
  });

  it('should ...', inject([TimePeriodService], (service: TimePeriodService) => {
    expect(service).toBeTruthy();
  }));
});

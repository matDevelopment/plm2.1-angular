import { Injectable } from '@angular/core';
import { LastSeenModel } from '../models-database/last-seen.model';
import { PathModel } from '../models/path-model.model';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class LastSeenService {

  constructor(private http: HttpClient) { }

  AddLastSeen(lastSeen: LastSeenModel) {
    return this.http.post(PathModel.Url + '/api/last-seen/add', lastSeen).pipe(
      map(res => {
        return res as LastSeenModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetLastSeen(userId: number) {
    return this.http.get(PathModel.Url + '/api/last-seen/' + userId).pipe(
      map(res => {
        return res as LastSeenModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

}

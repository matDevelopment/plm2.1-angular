/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ForgeService } from './forge.service';

describe('Service: Forge', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ForgeService]
    });
  });

  it('should ...', inject([ForgeService], (service: ForgeService) => {
    expect(service).toBeTruthy();
  }));
});

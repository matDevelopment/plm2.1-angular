import { Injectable } from '@angular/core';
import { FilterModel } from '../models/filter.model';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class TablesService {

    loadChartSubject = new Subject<any>();
    exportJsonToExcelSubject = new Subject<any>();
    addSubject = new Subject<any>();
    removeSubject = new Subject<any>();
    editSubject = new Subject<any>();
    subRemoveSubject = new Subject<any>();
    subEditSubject = new Subject<any>();

    constructor(private http: HttpClient) { }

    GetCasesWithUsersWithEcps(filter: FilterModel, subFilter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/tables/users', { filter: filter, subFilter: subFilter }).pipe(
            map(res => {
                return res as any[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetUsersWithTasksWithEcps(filter: FilterModel, subFilter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/tables/tasks', { filter: filter, subFilter: subFilter }).pipe(
            map(res => {
                return res as any[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetCasesWithTasks(filter: FilterModel, subFilter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/tables/case-tasks', { filter: filter, subFilter: subFilter }).pipe(
            map(res => {
                return res as any[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetGroupsWithUsers(filter: FilterModel, subFilter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/tables/groups', { filter: filter, subFilter: subFilter }).pipe(
            map(res => {
                console.log(res);
                return res as any[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    ExportToExcel(object: any) {
        return this.http.post(PathModel.Url + '/api/tables/export-json', object,
        { responseType: 'arraybuffer' } ).pipe(
            map(res => {
                return res;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

}

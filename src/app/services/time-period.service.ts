import { Injectable } from '@angular/core';
import { TimePeriodModel } from '../models-database/time-period.model';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { DescriptionModel } from '../models/description.model';
import { CaseModel } from '../models-database/case.model';
import { TaskModel } from '../models-database/task.model';

@Injectable({
  providedIn: 'root'
})
export class TimePeriodService {

  constructor(private http: HttpClient) { }

  addTimePeriodsSubject = new Subject<TimePeriodModel[]>();
  pickTimePeriodsByCaseSubject = new Subject<number>();
  changeTimePeriodsDescriptionSubject = new Subject<DescriptionModel>();
  deleteCollectionSubject = new Subject<number>();
  changeTimePeriodsCollectionSubject = new Subject<number>();
  changeDescriptionCollectionSubject = new Subject<number>();
  changeDescriptionHoursSubject = new Subject<{ StartTime: string, EndTime: string}>();
  changeCaseTaskIdSubject = new Subject<{ CaseId: number, TaskId: number }>();
  changeCaseTaskModelsSubject = new Subject<{ Case: CaseModel, Task: TaskModel }>();

  GetTimePeriods() {
    return this.http.get(PathModel.Url + '/api/ecps/time-periods').pipe(
        map(res => {
            return res as TimePeriodModel[];
        }),
        catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
}
}

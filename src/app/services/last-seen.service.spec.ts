/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LastSeenService } from './last-seen.service';

describe('Service: LastSeen', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LastSeenService]
    });
  });

  it('should ...', inject([LastSeenService], (service: LastSeenService) => {
    expect(service).toBeTruthy();
  }));
});

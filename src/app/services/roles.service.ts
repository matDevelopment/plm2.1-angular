import { Injectable } from '@angular/core';
import { RolesModel } from '../models/roles-model';
@Injectable()
export class RolesService {

constructor() { }

Check(role: string) {

    let userRoles = '';
/*
    if (this.authorizationService.GetUser() !== null) {
        userRoles = this.authorizationService.GetUser().Roles;
    }
*/
    switch (role) {
        case RolesModel.Admin:
            if (userRoles === RolesModel.Admin) {
                return true;
            }
            break;
        case RolesModel.TeamLeader:
            if (userRoles === RolesModel.TeamLeader || userRoles === RolesModel.Admin || userRoles === RolesModel.ProjectManager) {
                return true;
            }
            break;
         case RolesModel.ProjectManager:
            if (userRoles === RolesModel.ProjectManager || userRoles === RolesModel.Admin) {
                return true;
            }
            break;
        case RolesModel.User:
            return true;

        default:
            return false;
    }
    return false;
}

}

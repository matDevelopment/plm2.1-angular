/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EcpService } from './ecp.service';

describe('Service: Ecp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EcpService]
    });
  });

  it('should ...', inject([EcpService], (service: EcpService) => {
    expect(service).toBeTruthy();
  }));
});

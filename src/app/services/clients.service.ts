import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { ClientModel } from '../models-database/client.model';
import { RepresentativeModel } from '../models-database/representative.model';

@Injectable()
export class ClientsService {

    AddClientSubject = new Subject<ClientModel>();
    AddClientToDbSubject = new Subject<ClientModel>();
    EditClientSubject = new Subject<ClientModel>();

    AddRepresentativeSubject = new Subject<RepresentativeModel>();
    AddRepresentativeToDbSubject = new Subject<RepresentativeModel>();
    EdtitRepresentativeSubject = new Subject<RepresentativeModel>();

    constructor(private http: HttpClient) { }

    AddClient(client: ClientModel) {
        return this.http.post(PathModel.Url + '/api/clients/add', client).pipe(
            map(res => {
                return res as ClientModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetClients() {
        return this.http.get(PathModel.Url + '/api/clients').pipe(
            map(res => {
                return res as ClientModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetClient(id: number) {
        return this.http.get(PathModel.Url + '/api/clients/' + id).pipe(
            map(res => {
                return res as ClientModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveClient(client: ClientModel) {
        return this.http.post(PathModel.Url + '/api/clients/remove', client).pipe(
            map(res => {
                return res as ClientModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    EditClient(client: ClientModel) {
        return this.http.post(PathModel.Url + '/api/clients/edit', client).pipe(
            map(res => {
                return res as ClientModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetRepresentatives(id: number) {
        return this.http.get(PathModel.Url + '/api/clients/representatives/' + id).pipe(
            map(res => {
                return res as RepresentativeModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    AddRepresentative(representative: RepresentativeModel) {
        return this.http.post(PathModel.Url + '/api/clients/representatives/add', representative).pipe(
            map(res => {
                return res as RepresentativeModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    EditRepresentative(client: RepresentativeModel) {
        return this.http.post(PathModel.Url + '/api/clients/representatives/edit', client).pipe(
            map(res => {
                return res as RepresentativeModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveRepresentative(client: RepresentativeModel) {
        return this.http.post(PathModel.Url + '/api/clients/representatives/remove', client).pipe(
            map(res => {
                return res as RepresentativeModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }


}

import { Injectable } from '@angular/core';

@Injectable()
export class DateService {

  constructor() { }

  convertToCDate(date: Date): string {
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();

    if ((date.getMonth() + 1) < 10) {
      month = '0' + month;
    }

    if (date.getDate() < 10) {
      day = '0' + day;
    }
    return date.getFullYear() + '-' + month + '-' + day + 'T' + '00' + ':' + '00' + ':' + '00' + 'Z';
  }

  convertDateToMiutesAndHours(startDate: Date, finishTime: Date) {
    let startTimeHours = startDate.getHours().toString();
    let startTimeMinutes = startDate.getMinutes().toString();
    let finishTimeHours = finishTime.getHours().toString();
    let finishTimeMinutes = finishTime.getMinutes().toString();
    startTimeHours.length === 1 ? startTimeHours = '0' + startTimeHours : startTimeHours = startTimeHours;
    startTimeMinutes.length === 1 ? startTimeMinutes = '0' + startTimeMinutes : startTimeMinutes = startTimeMinutes;
    finishTimeHours.length === 1 ? finishTimeHours = '0' + finishTimeHours : finishTimeHours = finishTimeHours;
    finishTimeMinutes.length === 1 ? finishTimeMinutes = '0' + finishTimeMinutes : finishTimeMinutes = finishTimeMinutes;
    return {
      StartTimeHours: startTimeHours, StartTimeMinutes: startTimeMinutes,
      FinishTimeHours: finishTimeHours, FinishTimeMinutes: finishTimeMinutes
    };
  }

  convertToCdateWithHours(date: Date) {
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();
    let hours = date.getHours().toString();
    let minutes = date.getMinutes().toString();

    if ((date.getMonth() + 1) < 10) {
      month = '0' + month;
    }

    if (date.getDate() < 10) {
      day = '0' + day;
    }

    if (date.getHours() < 10) {
      hours = '0' + hours;
    }

    if (date.getMinutes() < 10) {
      minutes = '0' + minutes;
    }
    return date.getFullYear() + '-' + month + '-' + day + 'T' + hours + ':' + minutes + ':' + '00' + 'Z';
  }

  convertHoursMinutesToCDate(date: Date, hours: string, minutes: string) {
    let month = (date.getMonth() + 1).toString();
    let day = date.getDate().toString();


    if ((date.getMonth() + 1) < 10) {
      month = '0' + month;
    }

    if (date.getDate() < 10) {
      day = '0' + day;
    }

    return date.getFullYear() + '-' + month + '-' + day + 'T' + hours + ':' + minutes + ':' + '00' + 'Z';
  }

  convertHoursMinutesToDate(date: Date, hours: string, minutes: string) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), parseInt(hours, 10), parseInt(minutes, 10), 0, 0);

  }

  convertDescriptionDateToCDate(time: string, date: Date) {
    const res = time.split(':');
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(), parseInt(res[0], 10), parseInt(res[1], 10));
  }

  convertCDateToTimePeriod(date: Date) {
    const hours = date.getHours().toString();
    let minutes = date.getMinutes().toString();
    if (date.getMinutes() < 10) {
      minutes = '0' + minutes;
    }
    return hours + ':' + minutes;
  }

  converCDateToDate(date: string) {
    const dateSplit = date.split('T');
    const hourAndMinutes = dateSplit[1].split(':');
    const yearMonthDay = dateSplit[0].split('-');
    return new Date(parseInt(yearMonthDay[0], 10), parseInt(yearMonthDay[1], 10) - 1, parseInt(yearMonthDay[2], 10),
      parseInt(hourAndMinutes[0], 10), parseInt(hourAndMinutes[1], 10));
  }

  dateAdd(date, interval, units) {
    let ret = new Date(date);
    const checkRollover = () => { if (ret.getDate() !== date.getDate()) { ret.setDate(0); } };
    switch (interval.toLowerCase()) {
      case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
      case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
      case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
      case 'week': ret.setDate(ret.getDate() + 7 * units); break;
      case 'day': ret.setDate(ret.getDate() + units); break;
      case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
      case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
      case 'second': ret.setTime(ret.getTime() + units * 1000); break;
      default: ret = undefined; break;
    }
    return ret;
  }

  convertToAm4Date(date: string) {
    const am4Date = date.split('T');
    am4Date[1] = am4Date[1].slice(0, -3);
    return am4Date[0] + ' ' + am4Date[1];
  }

  GetWorkHoursBetweenDates(startDate: Date, finishDate: Date) {
    let workHours = 0;
    while (startDate.getTime() < finishDate.getTime()) {
      if (startDate.getDay() !== 0 && startDate.getDay() !== 6) {
        workHours += 8;
      }
      startDate.setDate(startDate.getDate() + 1);
    }
    return workHours;
  }

}

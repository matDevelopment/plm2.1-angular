import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, retry, catchError } from 'rxjs/operators';
import { PathModel } from '../models/path-model.model';
import { Subject, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserModel } from '../models-database/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  loginSubject = new Subject();
  jwtHelper = new JwtHelperService;
  decodedToken: any;

  login(model: any) {
    return this.http.post(PathModel.Url + '/api/auth/login', model)
      .pipe(
        map((response: any) => {
          if (response) {
            localStorage.setItem('token', response.token);
            this.decodedToken = this.jwtHelper.decodeToken(response.token);
          }
        })
      );
  }

  changePassword(username: string, oldPassword: string, newPassword: string) {
    return this.http.post(PathModel.Url + '/api/auth/change-password',
    {Username: username, oldPassword: oldPassword, NewPassword: newPassword}).pipe(
      map(res => {
          return res;
      }),
      catchError(
          err =>  err.code === 404 ? throwError('Not Found') : throwError(err)
      )
  );
  }

  resetPassword(user: UserModel) {
    return this.http.post(PathModel.Url + '/api/auth/reset-password', user, { responseType : 'text' }).pipe(
      map(res => {
          return res;
      }),
      catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
  );
  }

  deleteUser(user: UserModel) {
    return this.http.post(PathModel.Url + '/api/auth/delete-user', user, { responseType : 'text' }).pipe(
      map(res => {
          return res;
      }),
      catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
  );
  }

  loggedIn(): boolean {
    const token = localStorage.getItem('token');
    this.decodedToken = this.jwtHelper.decodeToken(token);
    return !this.jwtHelper.isTokenExpired(token);
  }

  roleMatch(allowedRoles): boolean {
    let isMatch = false;
    if (this.decodedToken.role !== undefined && this.decodedToken.role !== null) {
      const userRoles = this.decodedToken.role as Array<string>;
      allowedRoles.forEach(element => {
        if (userRoles.includes(element)) {
          isMatch = true;
          return ;
        }
      });
    }
    return isMatch;
  }

  groupsIdMatch(id: number): boolean {
    const groupsId = this.decodedToken.groupsid;
    if (this.decodedToken.groupsid === undefined || this.decodedToken.groupsid === null || id === null) {
      return false;
    }
    if (Array.isArray(groupsId)) {
      if (groupsId[0] === id.toString()) {
        return true;
      } else {
        return false;
      }
    }

      const string = groupsId.split(',');
      let state = false;
      string.forEach(groupId => {
        if (groupId === id.toString()) {
          state = true;
        }
      });
      return state;
  }

  roleAndIdMatch(allowedRoles, id: number, groupId, users?: UserModel[]) {
    if (this.groupsIdMatch(groupId) || this.roleMatch(allowedRoles) ||
    parseInt(this.decodedToken.nameid, 10) === id || this.usersMatch(users)) {
      return true;
    } else {
      return false;
    }
  }

  usersMatch(users: UserModel[]) {
    let state = false;
    if (users !== null && users !== undefined) {
      users.forEach(user => {
        if (user.Id === parseInt(this.decodedToken.nameid, 10)) {
          state = true;
        }
      });
    }
    return state;
  }

  register(model) {
    return this.http.post(PathModel.Url + '/api/auth/register', model)
      .pipe(
        map(response => response)
      );
  }
}

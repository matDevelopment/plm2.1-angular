import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private http: HttpClient) { }

  uploadFileSubject = new Subject();

  GetDirectory(id: string) {
    return this.http.post(PathModel.Url + '/api/files/get', { Id: id }).pipe(
      map(res => {
        return res as any[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  postFile(fileToUpload: File, path) {
    const formData: FormData = new FormData();
    formData.append(path, fileToUpload, fileToUpload.name);
    console.log(formData);
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    return this.http.put(PathModel.Url + '/api/files/upload', formData , { headers: headers }).pipe(
      map(() => true),
      catchError(err => err.code === 404 ? throwError('Not Found') : throwError(err))
    )
  }

  DownloadFile(file: string) {
    return this.http.post(PathModel.Url + '/api/files/download', { Id: file }, { observe: 'response', responseType: 'blob' }).pipe(
      map(res => {
        return res;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

}

import { element } from 'protractor';
import { StatusModel } from './../models-database/status.model';
import { PathModel } from './../models/path-model.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { PriorityModel } from '../models-database/priority.model';
import { ButtonModel } from '../models/button.model';
import { FilterModel } from '../models/filter.model';
import { FilterNamesModel } from '../models/filterNames.model';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  filtersSubject = new Subject<FilterModel>();
  filterWithSubfilterSubject = new Subject<{Filter: FilterModel, SubFilter: FilterModel}>();
  prioritySubject = new Subject<PriorityModel>();
  statusSubject = new Subject<StatusModel>();

  constructor(private http: HttpClient) { }

  GetPriorities() {
    return this.http.get(PathModel.Url + '/api/filters/priorities').pipe(
      map(res => {
          const priorities = res as PriorityModel[];
          const buttons: ButtonModel[] = [];
          priorities.forEach(priority => {
            const button = new ButtonModel();
            button.Id = priority.Id;
            button.Text = priority.Name;
            button.BackgroundColor = priority.BackgroundColor;
            button.Color = priority.Color;
            button.PosY = priority.PosY;
            button.PosX = priority.PosX;
            buttons.push(new ButtonModel());
          });

          return priorities;
      }),
      catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetStatuses() {
    return this.http.get(PathModel.Url + '/api/filters/statuses').pipe(
      map(res => {

        const statuses = res as StatusModel[];
        const buttons: ButtonModel[] = [];
        statuses.forEach(status => {
          const button = new ButtonModel();
          button.Id = status.Id;
          button.Text = status.Name;
          button.BackgroundColor = status.BackgroundColor;
          button.Color = status.Color;
          status.PosY = status.PosY;
          status.PosX = status.PosX;
          buttons.push(button);
        });
        return statuses;

      }),
      catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetFilterNames(filter: FilterModel) {
    return this.http.post(PathModel.Url + '/api/filters/names', filter).pipe(
      map(res => res as FilterNamesModel),
      catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }


}

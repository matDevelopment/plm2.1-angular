import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { UserModel } from '../models-database/user.model';
import { GroupModel } from '../models-database/group.model';
import { FilterModel } from '../models/filter.model';


@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    AddUserSubject = new Subject<UserModel>();
    AddUsersSubject = new Subject<UserModel[]>();
    EditUserSubject = new Subject<UserModel>();

    AddUser(user: UserModel) {
        return this.http.post(PathModel.Url + '/api/users/add', user).pipe(
            map(res => {
                return res as UserModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    AddUserToGroup(user: UserModel, group: GroupModel) {
        return this.http.post(PathModel.Url + '/api/users/group/add', { 'user' : user, 'group' : group}).pipe(
            map(res => {
                return res as UserModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetUsers() {
        return this.http.get(PathModel.Url + '/api/users').pipe(
            map((res: UserModel[]) => {
                return res as UserModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetUsersForGroup(groupId: number) {
        return this.http.post(PathModel.Url + '/api/users/group', groupId).pipe(
            map(res => {
                return res as UserModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveUserFromGroup(user: UserModel, group: GroupModel) {
        return this.http.post(PathModel.Url + '/api/users/group/remove', { 'user' : user, 'group' : group}).pipe(
            map(res => {
                return res;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    getRoles() {
        return this.http.get(PathModel.Url + '/api/users/get/roles').pipe(
            map(res => {
                return res as any;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    changeRoles(name: string, roles: string[]) {
        return this.http.post(PathModel.Url + '/api/users/roles', { Name: name, Roles: roles }).pipe(
            map(res => {
                return res as string;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    getUsersForUser(name: string) {
        return this.http.get(PathModel.Url + '/api/users/user/' + name).pipe(
            map(res => {
                return res as UserModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    getUsersForEcpDay(filter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/users/ecp', filter).pipe(
            map(res => {
                return res as UserModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    getFilteredUsers(filter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/users/filtered', filter).pipe(
            map(res => {
                return res as UserModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    getTeamLeaders() {
        return this.http.get(PathModel.Url + '/api/users/team-leader').pipe(
            map((res: UserModel[]) => {
                return res as UserModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    getUser(userId: number) {
        return this.http.get(PathModel.Url + '/api/users/' + userId).pipe(
            map((res) => {
                return res as UserModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    EditUser(user: UserModel) {
        return this.http.post(PathModel.Url + '/api/users/edit', user).pipe(
            map((res) => {
                return res as UserModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

}

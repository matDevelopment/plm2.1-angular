import { TaskModel } from '../models-database/task.model';
import { FilterModel } from '../models/filter.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { number } from '@amcharts/amcharts4/core';

@Injectable({
    providedIn: 'root'
})
export class TaskService {

    constructor(private http: HttpClient) { }

    AddNewTaskSubject = new Subject<TaskModel>();
    EdtiTaskSubject = new Subject<TaskModel>();
    TaskFiltersSubject = new Subject<{ Type: string, Id: number }>();
    UpdateTasksSubject = new Subject<TaskModel[]>();
    RemoveTaskSubject = new Subject<TaskModel>();
    ChangeTaskTableSubjec = new Subject<{Tasks: TaskModel[], Type: string}>();
    FilterSubject = new Subject<number>();

    AddNewTask(task: TaskModel) {
        return this.http.post(PathModel.Url + '/api/tasks/add', task).pipe(
            map(res => {
                return res as TaskModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetTasks() {
        return this.http.get(PathModel.Url + '/api/tasks').pipe(
            map(res => {
                return res as TaskModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetTaskById(id: Number) {
        return this.http.get(PathModel.Url + '/api/tasks/task/task/' + id).pipe(
            map(res => {
                return res as TaskModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetFilteredTasks(filterModel: FilterModel) {
        return this.http.post(PathModel.Url + '/api/tasks/filter', filterModel).pipe(
            map(res => {
                console.log(res);
                return res as TaskModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetTasksForCase(id) {
        return this.http.get(PathModel.Url + '/api/tasks/case/' + id).pipe(
            map(res => {
                return res as TaskModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    EditCase(caseModel: TaskModel) {
        return this.http.post(PathModel.Url + '/api/tasks/edit', caseModel).pipe(
            map(res => {
                return res as TaskModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveTask(caseModel: TaskModel) {
        return this.http.post(PathModel.Url + '/api/tasks/remove', caseModel).pipe(
            map(res => {
                return res as TaskModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    ChangeStatus(task: TaskModel) {
        return this.http.post(PathModel.Url + '/api/tasks/status', task).pipe(
            map(res => {
                return res as TaskModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    SetBimFolder(id: number, folderId: string) {
        return this.http.post(PathModel.Url + '/api/tasks/bim360', { Id: id, FolderId: folderId }).pipe(
            map(res => {}),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }
}

/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { JwtHttpInterceptorService } from './JwtHttpInterceptor.service';

describe('Service: JwtHttpInterceptor', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JwtHttpInterceptorService]
    });
  });

  it('should ...', inject([JwtHttpInterceptorService], (service: JwtHttpInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});

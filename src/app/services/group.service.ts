import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { GroupModel } from '../models-database/group.model';
import { FilterModel } from '../models/filter.model';


@Injectable()
export class GroupService {

    constructor(private http: HttpClient) { }

    CaseFiltersSubject = new Subject<{ Type: string, Id: number}>();
    NewGroupSubject = new Subject<GroupModel>();
    AddGroupSubject = new Subject<GroupModel>();
    RemoveGroupSubject = new Subject<GroupModel>();

    AddGroup(group: GroupModel) {
        console.log(group);
        return this.http.post(PathModel.Url + '/api/groups/add',  group).pipe(
            map(res => {
                return res as GroupModel;
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetGroupsWithUsers() {
        return this.http.get(PathModel.Url + '/api/groups/users').pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetGroups() {
        return this.http.get(PathModel.Url + '/api/groups').pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    SetGroupTeamLeader(group: GroupModel) {
        return this.http.post(PathModel.Url + '/api/groups/set/team-leader', group).pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveTeamLeaderFromGroup(group: GroupModel) {
        return this.http.post(PathModel.Url + '/api/groups/remove/team-leader', group).pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveGroup(group: GroupModel) {
        return this.http.post(PathModel.Url + '/api/groups/remove', group).pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    GetFilteredGroups(filter: FilterModel) {
        return this.http.post(PathModel.Url + '/api/groups/filtered', filter).pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

    RemoveUserFromGroup(userId: number, groupId: number) {
        return this.http.post(PathModel.Url + '/api/groups/remove/user', { UserId: userId, GroupId: groupId }).pipe(
            map(res => {
                return res as GroupModel[];
            }),
            catchError(
                err => err.code === 404 ? throwError('Not Found') : throwError(err)
            )
        );
    }

}

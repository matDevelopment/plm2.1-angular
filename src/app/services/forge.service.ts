import { PathModel } from "../models/path-model.model";
import { HttpClient } from "@angular/common/http";
import { map, catchError } from "rxjs/operators";
import { throwError, Subject } from "rxjs";
import { Injectable } from "@angular/core";

@Injectable()
export class ForgeService {

  constructor(private http: HttpClient) { }

  loadFileSubject = new Subject<any>();
  sendFileSubject = new Subject<any>();
  folderIdSubject = new Subject<any>();

  SignIn() {
    return this.http.get(PathModel.Url + '/api/forgeAuth/url').pipe(
      map(res => {
        location.href = res as string;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetToken() {
    return this.http.get(PathModel.Url + '/api/forgeAuth/token').pipe(
      map(res =>
        res as any
      ),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  SendFileToBim360(folderId, filePath) {
    return this.http.post(PathModel.Url + '/api/forgeData/upload', { FolderId: folderId, FilePath: filePath }).pipe(
      map(res =>
        res
      ),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetData(id) {
    return this.http.post(PathModel.Url + '/api/forgeData/datamanagement', { Id: id }).pipe(
      map(res =>
        res as any[]
      ),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  SubmitBim360data(containerId, urn, data) {
    return this.http.post(PathModel.Url + '/api/forge/bim360/container/' + containerId + '/issues/' + urn, { data: data }).pipe(
      map(res =>
        res as any[]
      ),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetBimContainer(href) {
    return this.http.get(PathModel.Url + '/api/forge/bim360/container?href=' + href).pipe(
      map(res =>
        res as any
      ),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetBimIssues(accountId, containerId, urn) {
    return this.http.get(PathModel.Url + '/api/forge/bim360/account/' + accountId + '/container/' + containerId + '/issues/' + urn).pipe(
      map(res =>
        res as any
      ),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

}

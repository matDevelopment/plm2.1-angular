import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { TagModel } from '../models-database/tag.model';
import { number } from '@amcharts/amcharts4/core';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient) { }

  addTagsToCaseSubject = new Subject<{ ParentTagId: number, ChildTagId: number}>();
  addTagsToTaskSubject = new Subject<{ MainTag: TagModel, AccessoryTag: TagModel, AccessoryMinorTag: TagModel}>();
  pickTaskTagsSubject = new Subject<{ CaseTagId: number, MainTag: TagModel, AccessoryTag: TagModel, AccessoryMinorTag: TagModel}>();
  pickedTaskTagsSubject = new Subject<{ CaseTagId: number, MainTagId: number, AccessoryTagId: number, AccessoryMinorTagId: number }>();
  changeCaseParentIdSubject = new Subject<number>();
  removeCaseTagParentSubject = new Subject();


  GetCaseTagsChild(id) {
    return this.http.get(PathModel.Url + '/api/tags/case/child/' + id).pipe(
      map(res => {
        return res as TagModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetCaseTagsParent() {
    return this.http.get(PathModel.Url + '/api/tags/case/parent').pipe(
      map(res => {
        return res as TagModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTaskTagsMain(id: number) {
    return this.http.get(PathModel.Url + '/api/tags/task/main/' + id).pipe(
      map(res => {
        return res as TagModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTaskTagsAccessory(id: number) {
    return this.http.get(PathModel.Url + '/api/tags/task/accessory/' + id).pipe(
      map(res => {
        return res as TagModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTaskTagsAccessoryMinor(id: number) {
    return this.http.get(PathModel.Url + '/api/tags/task/accessory/minor/' + id).pipe(
      map(res => {
        return res as TagModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  AddCaseTagParent(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/case/parent/add', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  AddCaseTagChild(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/case/child/add', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  AddTaskTagMain(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/main/add', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  AddTaskTagsAccessory(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/add', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  AddTaskTagsAccessoryMinor(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/minor/add', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveCaseTagParent(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/case/parent/remove', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveCaseTagChild(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/case/child/remove', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveTaskTagMain(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/main/remove', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveTaskTagsAccessory(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/remove', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveTaskTagsAccessoryMinor(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/minor/remove', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditCaseTagParent(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/case/parent/edit', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditCaseTagChild(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/case/child/edit', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditTaskTagMain(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/main/edit', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditTaskTagsAccessory(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/edit', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditTaskTagsAccessoryMinor(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/minor/edit', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  CopyTaskTagMainStructure(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/main/copy', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  CopyTaskTagAccessoryStructure(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/copy', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  CopyTaskTagAccessoryMinorStructure(tag: TagModel) {
    return this.http.post(PathModel.Url + '/api/tags/task/accessory/minor/copy', tag).pipe(
      map(res => {
        return res as TagModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }
}

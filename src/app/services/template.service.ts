import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { TemplateModel } from '../models-database/template.model';
import { catchError, map } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { TaskTemplateModel } from '../models-database/task-template.model';
import { CaseModel } from '../models-database/case.model';
import { TaskModel } from '../models-database/task.model';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor(private http: HttpClient) { }

  PickTemplateSubject = new Subject<TemplateModel>();
  AddTemplateSubject = new Subject<TemplateModel>();
  EditTeamplateSubject = new Subject<TemplateModel>();

  PickTaskTemplateSubject = new Subject<TaskTemplateModel>();
  AddTaskTemplateSubject = new Subject<TaskTemplateModel>();
  EditTaskTeamplateSubject = new Subject<TaskTemplateModel>();

  AddTemplate(template: TemplateModel) {
    return this.http.post(PathModel.Url + '/api/templates/add', template).pipe(
      map(res => {
        return res as TemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveTemplate(template: TemplateModel) {
    return this.http.post(PathModel.Url + '/api/templates/remove', template).pipe(
      map(res => {
        return res as TemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditTemplate(template: TemplateModel) {
    return this.http.post(PathModel.Url + '/api/templates/edit', template).pipe(
      map(res => {
        return res as TemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTemplate(id: number) {
    return this.http.get(PathModel.Url + '/api/templates/' + id).pipe(
      map(res => {
        return res as TemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTemplates() {
    return this.http.get(PathModel.Url + '/api/templates').pipe(
      map(res => {
        return res as TemplateModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  AddTaskTemplate(template: TaskTemplateModel) {
    return this.http.post(PathModel.Url + '/api/templates/task/add', template).pipe(
      map(res => {
        return res as TaskTemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  RemoveTaskTemplate(template: TaskTemplateModel) {
    return this.http.post(PathModel.Url + '/api/templates/task/remove', template).pipe(
      map(res => {
        return res as TaskTemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  EditTaskTemplate(template: TaskTemplateModel) {
    return this.http.post(PathModel.Url + '/api/templates/task/edit', template).pipe(
      map(res => {
        return res as TaskTemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTaskTemplate(id: number) {
    return this.http.get(PathModel.Url + '/api/templates/task' + id).pipe(
      map(res => {
        return res as TaskTemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  GetTaskTemplates() {
    return this.http.get(PathModel.Url + '/api/templates/task').pipe(
      map(res => {
        return res as TaskTemplateModel[];
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  CopyCaseToTemplate(caseModel: CaseModel) {
    console.log(caseModel);
    return this.http.post(PathModel.Url + '/api/templates/case/copy', caseModel).pipe(
      map(res => {
        return res as TemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }

  CopyTaskToTaskTemplate(taskModel: TaskModel) {
    return this.http.post(PathModel.Url + '/api/templates/task/copy', taskModel).pipe(
      map(res => {
        return res as TaskTemplateModel;
      }),
      catchError(
        err => err.code === 404 ? throwError('Not Found') : throwError(err)
      )
    );
  }
}

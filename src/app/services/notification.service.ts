import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PathModel } from '../models/path-model.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { NotificationModel } from './../models-database/notification.model';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

constructor(private http: HttpClient) { }

  AddNewNotification(newNotification: NotificationModel) {

    return this.http.post(PathModel.Url + '/api/notifications/add', newNotification).pipe(
        map(res => {
          return res as NotificationModel;
        }),
        catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
  }
  GetAllNotificationForUser(UserId: number) {
        return this.http.get(PathModel.Url + '/api/notifications/all/' +  UserId).pipe(
          map(res => {
            return res as NotificationModel[];
          }),
          catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
          )
      );
     }

     GetUnseenNotificationForUser(UserId: number) {
        return this.http.get(PathModel.Url + '/api/notifications/unseen/' +  UserId).pipe(
          map(res => {
            return res as NotificationModel[];
          }),
          catchError(
            err => err.code === 404 ? throwError('Not Found') : throwError(err)
          )
      );
     }

     GetSeenNotificationForUser(UserId: number) {
      return this.http.get(PathModel.Url + '/api/notifications/seen/' +  UserId).pipe(
        map(res => {
          return res as NotificationModel[];
        }),
        catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
   }

     SetNotificationToSeen(id) {
      return this.http.get(PathModel.Url + '/api/notifications/see' + id).pipe(
        map(res => res),
        catchError(
          err => err.code === 404 ? throwError('Not Found') : throwError(err)
        )
    );
     }

}

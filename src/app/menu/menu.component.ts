import { AuthService } from './../services/auth.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { faUserFriends, faStickyNote, faUser, faClone, faChartBar,
  faCalendarAlt, faProjectDiagram, faUserTie, faSignInAlt, faSignOutAlt, faTags, faUserCircle,
  faTable,
  faMap} from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { RolesModel } from '../models/roles-model';
import { ForgeAuth } from '../models/ForgeAuth';
import { ForgeService } from '../services/forge.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit, OnDestroy {

  faObjectGroup = faClone; faChartBar = faChartBar; faStickyNote = faProjectDiagram; faUser = faUser; faUserFriends = faUserFriends;
  faCalendar = faCalendarAlt; faUserTie = faUserTie; faSignInAlt = faSignInAlt; faSignOutAlt = faSignOutAlt; faTags = faTags;
  faUserCircle = faUserCircle; faTable = faTable; faMap = faMap;

  forgeAuth = ForgeAuth;

  rolesModel = RolesModel;

  loginSubscription = new Subscription();

  constructor(public authService: AuthService, private forgeService: ForgeService) { }
  @Input() Type: boolean;

  ngOnInit() {
    this.authService.loginSubject.subscribe(
      () => this.loggedIn()
    );

    ForgeAuth.IsAuthenticated = false;
    this.forgeService.GetToken().subscribe(
      res => ForgeAuth.IsAuthenticated = true
    );
  }

  loggedIn(): boolean {
    return this.authService.loggedIn();
  }

  logout() {
    localStorage.removeItem('token');
    return this.authService.loggedIn();
  }

  ngOnDestroy(): void {
    this.loginSubscription.unsubscribe();
  }

}

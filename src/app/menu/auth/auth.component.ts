import { AuthService } from './../../services/auth.service';
import { Component, OnInit, HostListener } from '@angular/core';
import { faUser, faUserCircle, faChevronCircleDown, faChevronCircleUp } from '@fortawesome/free-solid-svg-icons';


export enum KEY_CODE {
  ENTER = 13,
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  model: any = {};

  faUser = faUser;
  faChevronCircleDown = faChevronCircleDown;
  faChevronCircleUp = faChevronCircleUp;
  faUserCircle = faUserCircle;
  UserId = null;
  password = new PasswordChange();
  showChangePasswordPanel = false;



  constructor(public authService: AuthService) { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.keyCode === KEY_CODE.ENTER) {
      this.login();
    }
  }

  ngOnInit() {}

  setUserId(): number {
    return this.UserId = this.authService.decodedToken.nameid;
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      this.authService.loginSubject.next();
    },
    error => alert('Złe hasło lub login')
    );
  }
  logOut() {
    localStorage.removeItem('token');
  }

  changePassword() {
    this.authService.changePassword(this.authService.decodedToken.unique_name,
       this.password.OldPassword, this.password.NewPassword).subscribe(
         res => {this.showChangePasswordPanel = false; alert('Hasło Zmienione'); },
         error => { alert('Złe hasło'); }
       );
  }
}

export class PasswordChange {
  OldPassword: string;
  NewPassword: string;
  PasswordCheck: string;
}

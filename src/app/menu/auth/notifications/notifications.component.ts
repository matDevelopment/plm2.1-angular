import { Component, OnInit, Input } from '@angular/core';
import { NotificationModel } from 'src/app/models-database/notification.model';
import { faArrowRight, faChevronCircleDown, faChevronCircleUp } from '@fortawesome/free-solid-svg-icons';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  faChevronCircleDown = faChevronCircleDown;
  faChevronCircleUp = faChevronCircleUp;
  showNotifications = false;

  constructor(private notificationService: NotificationService) { }

  faArrow = faArrowRight;
  @Input() UserId;
  Notifications: NotificationModel[] = [];

  ngOnInit() {
    this.notificationService.GetAllNotificationForUser(this.UserId).subscribe(
      notifications => {console.log(notifications); this.Notifications = notifications}
    );
  }
}

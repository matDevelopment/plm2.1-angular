import { ForgeService } from 'src/app/services/forge.service';
import { Component, OnInit } from '@angular/core';
import { ForgeAuth } from 'src/app/models/ForgeAuth';

@Component({
  selector: 'app-forge-auth',
  templateUrl: './forge-auth.component.html',
  styleUrls: ['./forge-auth.component.scss']
})
export class ForgeAuthComponent implements OnInit {

  constructor(private forgeService: ForgeService) { }

  ngOnInit() {
    
  }

  forgeLogin() {
    this.forgeService.SignIn().subscribe();
  }

}

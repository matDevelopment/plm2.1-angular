import { Component, OnInit } from '@angular/core';
import { LastSeenService } from 'src/app/services/last-seen.service';
import { AuthService } from 'src/app/services/auth.service';
import { LastSeenModel } from 'src/app/models-database/last-seen.model';
import { faArrowRight, faChevronCircleDown, faChevronCircleUp } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-last-seen',
  templateUrl: './last-seen.component.html',
  styleUrls: ['./last-seen.component.scss']
})
export class LastSeenComponent implements OnInit {

  constructor(private lastSeenService: LastSeenService, private authService: AuthService) { }

  faChevronCircleDown = faChevronCircleDown;
  faChevronCircleUp = faChevronCircleUp;
  showLastSeen = false;

  lastSeenArr: LastSeenModel[] = [];
  faArrow = faArrowRight;

  ngOnInit() {
    this.lastSeenService.GetLastSeen(this.authService.decodedToken.nameid).subscribe(
      res => {console.log(res); this.lastSeenArr = res}
    );
  }

}

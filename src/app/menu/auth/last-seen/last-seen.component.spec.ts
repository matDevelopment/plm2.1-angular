/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LastSeenComponent } from './last-seen.component';

describe('LastSeenComponent', () => {
  let component: LastSeenComponent;
  let fixture: ComponentFixture<LastSeenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastSeenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastSeenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

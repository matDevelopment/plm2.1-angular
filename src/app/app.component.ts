import { Component, OnInit } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from './services/auth.service';
import { PathModel } from './models/path-model.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {

  constructor(private authServive: AuthService) {}

  // #icons
  faAngleRight = faBars;
  faAngleLeft = faBars;
  // #iconssend

  expanded = false;
  title = 'FrontEnd';

  ngOnInit(): void {
    console.log(PathModel.UrlWithOutHttp, PathModel.UrlWithOutHttp === '192.168.0.12:2334');
    this.authServive.loggedIn();
    this.authServive.loginSubject.subscribe(
      () =>  this.expanded = !this.expanded
    );
  }

}

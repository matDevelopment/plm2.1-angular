
import { TimeBarColorComponent } from './time-bar-color/time-bar-color.component';
import { NgModule } from '@angular/core';
import { HasRoleDirective } from '../directives/hasRole.directive';
import { GroupUserPickerComponent } from './group-user-picker/group-user-picker.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
   MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatSelectModule, MatInputModule,
   MatSlideToggleModule, MatAutocompleteModule, MatRadioModule, MatTableModule, MatCardModule, MatTabsModule
} from '@angular/material';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CasePickerComponent } from './case-picker/case-picker.component';
import { TagsPickerComponent } from './tags-picker/tags-picker.component';
import { CaseTagsPickerComponent } from './tags-picker/case-tags-picker/case-tags-picker.component';
import { TaskTagsPickerComponent } from './tags-picker/task-tags-picker/task-tags-picker.component';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';
import { LoadDirective } from '../directives/load.directive';
import { ClientPickerComponent } from './client-picker/client-picker.component';
import { TaskTemplatePickerComponent } from './task-template-picker/task-template-picker.component';
import { PriorityPickerComponent } from './priority-picker/priority-picker.component';
import { TemplatePickerComponent } from './template-picker/template-picker.component';
import { YearPickerComponent } from './year-picker/year-picker.component';
import { MonthPickerComponent } from './month-picker/month-picker.component';
import { QuarterPickerComponent } from './quarter-picker/quarter-picker.component';
import { TileComponent } from './tile/tile.component';
import { IconPopoverComponent } from './icon-popover/icon-popover.component';
import { AnimatedButtonComponent } from './animated-button/animated-button.component';
import { TimeBarComponent } from './time-bar/time-bar.component';
import { RouterModule } from '@angular/router';
import { TableComponent } from './table/table.component';
import { SubTableComponent } from './table/sub-table/sub-table.component';

@NgModule({
   imports: [ReactiveFormsModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatFormFieldModule,
      MatSelectModule,
      MatInputModule,
      MatSlideToggleModule,
      MatAutocompleteModule,
      MatRadioModule,
      MatDatepickerModule,
      MatNativeDateModule,
      MatSelectModule,
      MatTableModule,
      MatCardModule,
      CommonModule,
      FormsModule,
      RouterModule,
      FontAwesomeModule,
      MatTabsModule,
   ],
   declarations: [
      HasRoleDirective,
      LoadDirective,
      GroupUserPickerComponent,
      CasePickerComponent,
      TagsPickerComponent,
      CaseTagsPickerComponent,
      TaskTagsPickerComponent,
      FilterPanelComponent,
      ClientPickerComponent,
      PriorityPickerComponent,
      TemplatePickerComponent,
      YearPickerComponent,
      MonthPickerComponent,
      QuarterPickerComponent,
      TaskTemplatePickerComponent,
      TileComponent,
      IconPopoverComponent,
      AnimatedButtonComponent,
      TimeBarComponent,
      TimeBarColorComponent,
      TableComponent,
      SubTableComponent,
   ],
   entryComponents: [FilterPanelComponent, YearPickerComponent, MonthPickerComponent, QuarterPickerComponent],
   exports: [
      HasRoleDirective,
      LoadDirective,
      GroupUserPickerComponent,
      FilterPanelComponent,
      CasePickerComponent,
      TagsPickerComponent,
      CaseTagsPickerComponent,
      CaseTagsPickerComponent,
      TaskTagsPickerComponent,
      MatTabsModule,
      MatDatepickerModule,
      ClientPickerComponent,
      PriorityPickerComponent,
      TemplatePickerComponent,
      YearPickerComponent,
      MonthPickerComponent,
      QuarterPickerComponent,
      TableComponent,
      TaskTemplatePickerComponent,
      TileComponent,
      IconPopoverComponent,
      AnimatedButtonComponent,
      TimeBarComponent,
      TimeBarColorComponent,
      TableComponent,
      SubTableComponent
   ],
})
export class SharedModule { }

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { GroupService } from 'src/app/services/group.service';
import { UserModel } from 'src/app/models-database/user.model';
import { GroupModel } from 'src/app/models-database/group.model';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';
import { FilterModel } from 'src/app/models/filter.model';
import { faMinus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-group-user-picker',
  templateUrl: './group-user-picker.component.html',
  styleUrls: ['./group-user-picker.component.scss']
})
export class GroupUserPickerComponent implements OnInit, OnDestroy {

  @Input() PickedUser = new UserModel();
  @Input() PickedGroup = new GroupModel();
  @Input() ParentControl: FormGroup = new FormGroup({
    'user' : new FormControl({value: null, disabled: false}, Validators.required),
    'group' : new FormControl({value: null, disabled: true}, Validators.required),
  });
  @Input() IsProject = false;
  @Input() ShowOnlyUser = false;
  @Input() ShowOnlyGroup = false;
  @Input() ShowUsers = false;
  options: string[] = ['One', 'Two', 'Three'];
  checked = false;

  @Input() PickedUsers: UserModel[] = [];

  Users: UserModel[];
  FilteredUsers: Promise<UserModel[]>;

  faMinus = faMinus;

  Groups: GroupModel[];
  FilteredGroups: Promise<GroupModel[]>;

  parentFormSubscription = new Subscription();
  groupFilter = new FilterModel();

  constructor(private userService: UserService, private groupService: GroupService, private authService: AuthService) { }

  userGroupControl() {
    this.checked = !this.checked;
    if (this.checked) {
      this.ParentControl.get('group').enable();
      this.ParentControl.get('user').disable();
      this.ParentControl.get('user').setValue(null);
    } else {
      this.ParentControl.get('group').disable();
      this.ParentControl.get('user').enable();
      this.ParentControl.get('group').setValue(null);
    }

  }

  ngOnInit() {
   if (this.authService.roleMatch([RolesModel.User, RolesModel.TeamLeader])) {
      this.getUsersForUser();
    } else {
      this.getUsers();
    }

    if (this.authService.roleMatch([RolesModel.User])) {
      this.groupFilter.UserId = this.authService.decodedToken.nameid;
    } else if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.groupFilter.TeamLeaderId = this.authService.decodedToken.nameid;
    }

    this.groupService.GetFilteredGroups(this.groupFilter).subscribe(
      groups => {
        this.Groups = groups; this.FilteredGroups = new Promise((resolve, reject) => {
          resolve(this.Groups);
        });
      });

    this.parentFormSubscription = this.ParentControl.statusChanges.subscribe(
      status => {
        if (status === 'PENDING') {
          this.ParentControl.get('user').markAsTouched();
          this.ParentControl.get('group').markAsTouched();
        }
      }
    );
  }

  getUsers() {
    this.userService.GetUsers().subscribe(
      users => {
        this.Users = users; this.FilteredUsers = new Promise((resolve, reject) => {
          resolve(this.Users);
        });
      });
  }

  getUsersForUser() {
    const filter = new FilterModel();
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      filter.TeamLeaderId = this.authService.decodedToken.nameid;
    } else {
      filter.UserId = this.authService.decodedToken.nameid;
    }
    this.userService.getFilteredUsers(filter).subscribe(
      users => {
        this.Users = users; console.log(users), this.FilteredUsers = new Promise((resolve, reject) => {
          resolve(this.Users);
        });
      });
  }

  onFilterUsers(name: string) {
    this.FilteredUsers = new Promise((resolve, reject) => {
      resolve(this.Users.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  onFilterGroups(name: string) {
    this.FilteredGroups = new Promise((resolve, reject) => {
      resolve(this.Groups.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  addUser(_event: any, user: UserModel) {
    if (_event.isUserInput) {
      this.userService.AddUserSubject.next(user);
      if (this.PickedUsers.find(u => u.Id === user.Id) === undefined) {
        this.PickedUsers.push(user);
      }
      this.userService.AddUsersSubject.next(this.PickedUsers);
    }
  }

  addGroup(_event, group: GroupModel) {
    if (_event.isUserInput) {
      this.groupService.AddGroupSubject.next(group);
      group.Users.forEach(user => {
        if (this.PickedUsers.find(u => u.Id === user.Id) === undefined) {
          this.PickedUsers.push(user);
        }
      });
      this.userService.AddUsersSubject.next(this.PickedUsers);
    }
  }

  removeUser(user) {
    this.PickedUsers.splice(this.PickedUsers.indexOf(user), 1);
    this.userService.AddUsersSubject.next(this.PickedUsers);
  }

  ngOnDestroy(): void {
    this.parentFormSubscription.unsubscribe();
  }

}

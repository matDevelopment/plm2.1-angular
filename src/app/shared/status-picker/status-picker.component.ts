import { Component, OnInit, Input } from '@angular/core';
import { StatusModel } from 'src/app/models-database/status.model';
import { FilterService } from 'src/app/services/filter.service';

@Component({
  selector: 'app-status-picker',
  templateUrl: './status-picker.component.html',
  styleUrls: ['./status-picker.component.scss']
})
export class StatusPickerComponent implements OnInit {

  constructor(private filterService: FilterService) { }

  statuses: StatusModel[] = [];
  @Input() pickedStatus: StatusModel = new StatusModel();

  ngOnInit() {
    this.filterService.GetStatuses().subscribe(
      statuses => this.statuses = statuses
    );
  }

  changeStatus(status: StatusModel) {
    this.pickedStatus = status;
    this.filterService.statusSubject.next(status);
  }

}

import { Component, OnInit } from '@angular/core';
import { EcpService } from 'src/app/services/ecp.service';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-quarter-picker',
  templateUrl: './quarter-picker.component.html',
  styleUrls: ['./quarter-picker.component.scss']
})
export class QuarterPickerComponent implements OnInit {

  constructor(private ecpService: EcpService, private QuarterDialogRef: MatDialogRef<QuarterPickerComponent>) { }

  formGroup = new FormControl('');

  years: string[] = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
    '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027',
    '2028', '2029', '2030', '2031'
  ];

  quarters: string[] = ['Pierwszy', 'Drugi', 'Trzeci', 'Czwarty'];

  startDate = new Date();
  endDate = new Date();

  ngOnInit() {
  }

  pickYear(year: string) {
    this.startDate.setFullYear(parseInt(year, 10));
    this.endDate.setFullYear(parseInt(year, 10));
   }

   pickQuarter(quarter: number) {
     switch (quarter) {
       case 0:
       this.startDate = new Date(this.startDate.getFullYear(), 0, 1, 0, 0);
       this.endDate = new Date(this.endDate.getFullYear(), 3, 0, 23, 59);
       break;
       case 1:
       this.startDate = new Date(this.startDate.getFullYear(), 3, 1, 0, 0);
       this.endDate = new Date(this.endDate.getFullYear(), 6, 0, 23, 59);
       break;
       case 2:
       this.startDate = new Date(this.startDate.getFullYear(), 6, 1, 0, 0);
       this.endDate = new Date(this.endDate.getFullYear(), 9, 0, 23, 59);
       break;
       case 3:
       this.startDate = new Date(this.startDate.getFullYear(), 9, 1, 0, 0);
       this.endDate = new Date(this.endDate.getFullYear(), 0, 0, 23, 59);
       break;
     }
   }

   submit() {
     this.ecpService.ChangeDateSubject.next({ StartDate: this.startDate, EndDate: this.endDate });
     this.QuarterDialogRef.close();
   }


}

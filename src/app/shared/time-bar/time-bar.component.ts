import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-time-bar',
  templateUrl: './time-bar.component.html',
  styleUrls: ['./time-bar.component.scss']
})
export class TimeBarComponent implements OnInit {

  constructor() { }

  @Input() TimeTaken: number;
  @Input() TimeEstimated: number;
  @Input() Width = 100;
  percentageValue = 0;

  ngOnInit() {
    if (this.TimeEstimated > this.TimeTaken) {
      this.percentageValue = ( (this.TimeTaken * 100) / this.TimeEstimated);
    } else {
      this.percentageValue = 100;
    }
  }

}

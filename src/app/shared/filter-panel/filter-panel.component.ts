import { FilterService } from './../../services/filter.service';
import { CaseService } from 'src/app/services/case.service';
import { FilterModel } from 'src/app/models/filter.model';
import { TagService } from './../../services/tag.service';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TagModel } from 'src/app/models-database/tag.model';
import { UserService } from 'src/app/services/user.service';
import { GroupService } from 'src/app/services/group.service';
import { StatusModel } from 'src/app/models-database/status.model';

@Component({
  selector: 'app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.scss']
})
export class FilterPanelComponent implements OnInit, OnDestroy {

  FilterForm;
  constructor(private tagService: TagService, private caseService: CaseService, private filterService: FilterService,
    private filtersDialogRef: MatDialogRef<FilterPanelComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService, private groupService: GroupService) { }

  allowFilterUser = true;
  allowFilterCase = true;
  allowFilterTags = true;

  changeCaseParentIdSubscription = new Subscription();
  changeCaseTagsSubscription = new Subscription();
  changeTaskTagsSubscription = new Subscription();
  changeCaseIdSubscription = new Subscription();
  changeUserIdSubscription = new Subscription();
  changeGroupIdSubscription = new Subscription();

  filter = new FilterModel();
  subFilter = new FilterModel();
  statuses: StatusModel[];

  CaseTagParentId: number;
  CaseTagChildId: number;
  TaskTagMainId: number;
  TaskTagAccessoryId: number;
  TaskTagAccessoryMinorId: number;
  CaseId: number;
  UserId: number;
  GroupId: number;

  ngOnInit() {
    this.FilterForm = new FormGroup({
      'user' : new FormControl({value: null, disabled: false}, Validators.required),
      'group' : new FormControl({value: null, disabled: true}, Validators.required),
      'case' : new FormControl({value: null}, Validators.required)
    });
    this.onChangeCaseParentTagId();
    this.onChangeCaseTagsId();
    this.onChangeTaskTagsId();
    this.onChangeUserId();
    this.onChangeGroupId();
    this.onChangeCaseId();
    this.filter.StatusId = null;
    this.filterService.GetStatuses().subscribe(
      statuses => this.statuses = statuses
    );
  }

  onChangeCaseParentTagId() {
    this.changeCaseParentIdSubscription = this.tagService.changeCaseParentIdSubject.subscribe(
      tagId => {
        this.CaseTagParentId = tagId;
        this.CaseTagChildId = null;
        this.TaskTagMainId = null;
        this.TaskTagAccessoryId = null;
        this.TaskTagAccessoryMinorId = null;
      }
    );
  }

  onChangeCaseTagsId() {
    this.changeCaseTagsSubscription = this.tagService.addTagsToCaseSubject.subscribe(
      tags => {
        this.CaseTagParentId = tags.ParentTagId;
        this.CaseTagChildId = tags.ChildTagId;
      }
    );
  }

  onChangeTaskTagsId() {
    this.changeTaskTagsSubscription = this.tagService.addTagsToTaskSubject.subscribe(
        tags => {
          tags.MainTag !== null ? this.TaskTagMainId = tags.MainTag.Id : this.TaskTagMainId = null;
          tags.AccessoryTag !== null ? this.TaskTagAccessoryId = tags.AccessoryTag.Id : this.TaskTagAccessoryId = null;
          tags.AccessoryMinorTag !== null ? this.TaskTagAccessoryMinorId = tags.AccessoryMinorTag.Id
          : this.TaskTagAccessoryMinorId = null;
        }
      );
  }

  onChangeCaseId() {
    this.changeCaseIdSubscription = this.caseService.PickNewCaseSubject.subscribe(
       caseModel => this.CaseId = caseModel.Id
    );
  }

  onChangeUserId() {
    this.changeUserIdSubscription = this.userService.AddUserSubject.subscribe(
      user => {
        this.GroupId = null;
        this.UserId = user.Id;
      }
    );
  }

  onChangeGroupId() {
    this.changeGroupIdSubscription = this.groupService.AddGroupSubject.subscribe(
      group => {
        this.UserId = null;
        this.GroupId = group.Id;
      }
    );
  }

  submitFilters() {
    switch (this.data.Type) {
      case 'Projekty -> Zadania':
      this.filter.CaseTagParentId = this.CaseTagParentId;
      this.filter.CaseTagChildId = this.CaseTagChildId;
      this.filter.CaseId = this.CaseId;
      this.filter.UserId = this.UserId;
      this.filter.GroupId = this.GroupId;
      this.subFilter.TaskTagMainId = this.TaskTagMainId;
      this.subFilter.TaskTagAccessoryId = this.TaskTagAccessoryId;
      this.subFilter.TaskTagAccessoryMinorId = this.TaskTagAccessoryMinorId;
      break;
      case 'Użytkownicy -> Zadania':
      this.filter.UserId = this.UserId;
      this.filter.GroupId = this.GroupId;
      this.subFilter.TaskTagMainId = this.TaskTagMainId;
      this.subFilter.TaskTagAccessoryId = this.TaskTagAccessoryId;
      this.subFilter.TaskTagAccessoryMinorId = this.TaskTagAccessoryMinorId;
      break;
      case 'Projekty -> Użytkownicy':
      this.filter.CaseTagParentId = this.CaseTagParentId;
      this.filter.CaseTagChildId = this.CaseTagChildId;
      this.filter.CaseId = this.CaseId;
      this.subFilter.UserId = this.UserId;
      this.subFilter.GroupId = this.GroupId;
      break;
      case 'Grupy -> Użytkownicy':
      this.filter.GroupId = this.GroupId;
      this.subFilter.UserId = this.UserId;
      break;
      case 'tile':
      this.filter.CaseTagParentId = this.CaseTagParentId;
      this.filter.CaseTagChildId = this.CaseTagChildId;
      this.filter.CaseId = this.CaseId;
      this.filter.UserId = this.UserId;
      this.filter.GroupId = this.GroupId;
      this.filter.TaskTagMainId = this.TaskTagMainId;
      this.filter.TaskTagAccessoryId = this.TaskTagAccessoryId;
      this.filter.TaskTagAccessoryMinorId = this.TaskTagAccessoryMinorId;
    }
    this.filterService.filterWithSubfilterSubject.next({Filter: this.filter, SubFilter: this.subFilter});
    this.filtersDialogRef.close();
  }

  changeStatus(status: StatusModel) {
    status === null ? this.filter.StatusId = null : this.filter.StatusId = status.Id;
  }

  ngOnDestroy(): void {
    this.changeCaseParentIdSubscription.unsubscribe();
    this.changeCaseTagsSubscription.unsubscribe();
    this.changeTaskTagsSubscription.unsubscribe();
    this.changeCaseIdSubscription.unsubscribe();
    this.changeUserIdSubscription.unsubscribe();
    this.changeGroupIdSubscription.unsubscribe();
  }

  nullTags() {
    this.allowFilterTags = !this.allowFilterTags;
    const emptyTag = new TagModel();
    this.tagService.addTagsToCaseSubject.next({ParentTagId: null, ChildTagId: null});
    this.tagService.addTagsToTaskSubject.next({MainTag: emptyTag, AccessoryTag: emptyTag, AccessoryMinorTag: emptyTag});
  }

  nullCase() {
    this.allowFilterCase = !this.allowFilterCase;
  }
}

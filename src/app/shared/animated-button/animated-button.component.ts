import { UserModel } from 'src/app/models-database/user.model';
import { Component, OnInit, Input } from '@angular/core';
import { faPlus, faClock } from '@fortawesome/free-solid-svg-icons';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animated-button',
  templateUrl: './animated-button.component.html',
  styleUrls: ['./animated-button.component.scss']
})
export class AnimatedButtonComponent implements OnInit {

  faPlus = faPlus;

  @Input() TopIcon;
  @Input() BottomIcon;
  @Input() MiddleIcon;
  @Input() MiddleBottomIcon;

  @Input() TopText;
  @Input() MiddleText;
  @Input() MiddleBottomText;
  @Input() BottomText;

  @Input() TopComponent;
  @Input() MiddleComponent;
  @Input() MiddleBottomComponent;
  @Input() BottomComponent;

  @Input() TopActive = true;
  @Input() MiddleActive = true;
  @Input() MiddleBottomActive = true;
  @Input() BottomActive = true;

  @Input() TopData;
  @Input() MiddleData;
  @Input() MiddleBottomData;
  @Input() BottomData;

  @Input() GroupId: number = null;
  @Input() UserId: number = null;
  @Input() StatusId: number = null;
  @Input() Users: UserModel[] = [];


  caseButton = false;

  constructor(private dialog: MatDialog, private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  openTopDialog() {
    if (this.TopActive) {
      this.dialog.open(this.TopComponent, {
        width: '100vw',
        maxHeight: '100vh',
        maxWidth: '100vw',
        height: '100vh',
        data: this.TopData
      });
    }
  }

  openMiddleDialog() {
    if (this.MiddleActive) {
      this.dialog.open(this.MiddleComponent, {
        width: '85%',
        maxWidth: '100vw',
        height: '90%',
        data: this.MiddleData
      });
    }
  }

  openMiddleBottomDialog() {
    if (this.MiddleBottomActive) {
      if (this.MiddleBottomData.Calendar) {
        this.router.navigate(['/ecp-calendar']);
      } else {
        this.dialog.open(this.MiddleBottomComponent, {
          width: '85%',
          maxWidth: '100vw',
          height: '90%',
          data: this.MiddleBottomData
        });
      }
    }
  }

  openBottomDialog() {
    if (this.BottomActive) {
      if (this.authService.roleAndIdMatch([RolesModel.Admin, RolesModel.ProjectManager], this.UserId, this.GroupId, this.Users)) {
        if (this.StatusId === null || this.StatusId === 2) {
          this.dialog.open(this.BottomComponent, {
            width: '90%',
            maxWidth: '100vw',
            height: '85%',
            panelClass: 'full-screen-modal',
            data: this.BottomData
          });
        }
      }
    }
  }

}

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {
  faUserAlt, faPlus, faClone, faFile, faPlusSquare,
  faEllipsisH, faExclamation, faStar, faLayerGroup, faCalendarAlt, faUserFriends
} from '@fortawesome/free-solid-svg-icons';
import { DescriptionComponent } from '../description/description.component';
import { MatDialog } from '@angular/material';
import { CasePanelComponent } from 'src/app/cases-tasks/cases/case-panel/case-panel.component';
import { TaskPanelComponent } from 'src/app/cases-tasks/tasks/task-panel/task-panel.component';
import { faClock } from '@fortawesome/free-regular-svg-icons';
import { ClientComponent } from '../client/client.component';
import { TaskService } from 'src/app/services/task.service';
import { Subscription } from 'rxjs';
import { CaseService } from 'src/app/services/case.service';
import { NewMemoComponent } from '../../cases-tasks/memos/new-memo/new-memo.component';
import { EcpPanelNewComponent } from 'src/app/ecp/ecp-panel-new/ecp-panel-new.component';
import { MemoListComponent } from '../../cases-tasks/memos/memo-list/memo-list.component';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';
import { ForgeComponent } from 'src/app/cases-tasks/forge/forge.component';
import { NewTaskComponent } from 'src/app/cases-tasks/tasks/new-task/new-task.component';


@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit, OnDestroy {

  faUserAlt = faUserAlt;
  faPlus = faPlus;
  faCopy = faClone;
  faFile = faFile;
  faEllipsisH = faEllipsisH;
  faPlusSquare = faPlusSquare;
  faExclamation = faExclamation;
  faCertificate = faStar;
  faClock = faClock;
  faUserFriends = faUserFriends;
  faCalendarAlt = faCalendarAlt;
  faLayerGroup = faLayerGroup;
  casePanelComponent = CasePanelComponent;
  ecpPanelComponent = EcpPanelNewComponent;
  newTaskComponent = NewTaskComponent;
  newMemoComponent = NewMemoComponent;
  memoListComponent = MemoListComponent;
  forgeViewerComponent = ForgeComponent;
  roles = RolesModel;

  todaysDate = new Date();

  @Input() Model;
  @Input() Type = 'case';

  constructor(private dialog: MatDialog, private taskService: TaskService,
    private caseService: CaseService, public authService: AuthService) { }

  EditTaskSubscription = new Subscription();
  EditCaseSubscription = new Subscription();

  ngOnInit() {
    if (this.Type === 'task') {
      this.editTask();
    } else {
      this.editCase();
    }
  }

  showDescriptionDialog() {
    this.dialog.open(DescriptionComponent, {
      width: '40%',
      data: { Description: this.Model.Description, Name: this.Model.Name }
    });
  }

  showCasePanel() {
    let caseModel = this.Model;
    if (this.Type === 'task') {
      caseModel = this.Model.Case;
    }
    this.dialog.open(CasePanelComponent, {
      width: '95%',
      height: '97%',
      minWidth: '95%',
      data: { Case: caseModel, expanded: true },
      autoFocus: false
    });
  }

  showClientPanel() {
    this.dialog.open(ClientComponent, {
      width: '50%',
      data: { Client: this.Model.Client, Representative: this.Model.Representative }
    });
  }

  showTaskPanel() {
    this.dialog.open(TaskPanelComponent, {
      width: '95%',
      height: '90%',
      minWidth: '95%',
      data: { Task: this.Model }
    });
  }

  editTask() {
    this.EditTaskSubscription = this.taskService.EdtiTaskSubject.subscribe(
      editedTask => {
        if (this.Model.Id === editedTask.Id) {
          this.Model = editedTask;
        }
      }
    );
  }

  editCase() {
    this.EditCaseSubscription = this.caseService.EdtiCaseSubject.subscribe(
      editedCase => {
        if (this.Model.Id === editedCase.Id) {
          this.Model = editedCase;
        }
      }
    );
  }

  changeStatus(id: number, message: string) {
    switch (this.Type) {
      case ('case'):
        if (confirm(message)) {
          this.Model.StatusId = id;
          this.caseService.ChangeStatus(this.Model).subscribe(
            caseModel => {
              this.Model.Status = caseModel.Status;
            }
          );
        }
        break;
      case ('task'):
        if (confirm(message)) {
          this.Model.StatusId = id;
          this.taskService.ChangeStatus(this.Model).subscribe(
            caseModel => {
              this.Model.Status = caseModel.Status;
              this.taskService.EdtiTaskSubject.next(this.Model);
            }
          );
        }
        break;
    }
  }

  ngOnDestroy(): void {
    this.EditTaskSubscription.unsubscribe();
    this.EditCaseSubscription.unsubscribe();
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { TemplateService } from 'src/app/services/template.service';
import { TemplateModel } from 'src/app/models-database/template.model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-template-picker',
  templateUrl: './template-picker.component.html',
  styleUrls: ['./template-picker.component.scss']
})
export class TemplatePickerComponent implements OnInit {

  constructor(private templateService: TemplateService) { }

  templates: TemplateModel[];
  templatesAsync: Promise<TemplateModel[]>;
  @Input() ParentForm: FormGroup;

  ngOnInit() {
    this.getTemplates();
  }

  getTemplates() {
    this.templateService.GetTemplates().subscribe(
      templates => {
        this.templates = templates; this.templatesAsync = new Promise((resolve, reject) => resolve(this.templates));
      }
    );
  }

  onFilterTemplates(name: string) {
    this.templatesAsync = new Promise((resolve, reject) => {
      resolve(this.templates.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  pickTemplate(_event, template: TemplateModel) {
    if (_event.isUserInput) {
      this.templateService.PickTemplateSubject.next(template);
    }
  }
}

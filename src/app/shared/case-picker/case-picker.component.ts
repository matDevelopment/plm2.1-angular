import { Component, OnInit, Input } from '@angular/core';
import { CaseService } from 'src/app/services/case.service';
import { CaseModel } from 'src/app/models-database/case.model';
import { FilterModel } from 'src/app/models/filter.model';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';

@Component({
  selector: 'app-case-picker',
  templateUrl: './case-picker.component.html',
  styleUrls: ['./case-picker.component.scss']
})
export class CasePickerComponent implements OnInit {

  cases: CaseModel[] = [];
  filteredCases: Promise<CaseModel[]>;
  filter: FilterModel;
  @Input() ParentControl;

  constructor(private caseService: CaseService, private authService: AuthService) { }

  ngOnInit() {
    this.filter = new FilterModel();
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.filter.TeamLeaderId = this.authService.decodedToken.nameid;
    } else if (this.authService.roleMatch([RolesModel.User])) {
      this.filter.UserId = this.authService.decodedToken.nameid;
    }
    this.getCases();
  }

  getCases() {
    this.caseService.GetFilteredCases(this.filter).subscribe(
      cases => {
      this.cases = cases; this.filteredCases = new Promise((resolve, reject) => {
        resolve(this.cases);
      });
      }
    );
  }

  onFilterCases(name) {
    this.filteredCases = new Promise((resolve, reject) => {
      resolve(this.cases.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  onPickCase(_event, caseModel: CaseModel) {
    if (_event.isUserInput) {
      this.caseService.PickNewCaseSubject.next(caseModel);
     }
  }

}

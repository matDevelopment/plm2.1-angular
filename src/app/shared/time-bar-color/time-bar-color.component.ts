import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-time-bar-color',
  templateUrl: './time-bar-color.component.html',
  styleUrls: ['./time-bar-color.component.scss']
})
export class TimeBarColorComponent implements OnInit {

  constructor() { }

  @Input() TimeEstimated;
  @Input() TimeTaken;

  width = 0;

  ngOnInit() {
    if (parseInt(this.TimeEstimated, 10) !== 0 && parseInt(this.TimeTaken, 10) !== 0) {
      this.width = (100 * parseInt(this.TimeTaken, 10)) / parseInt(this.TimeEstimated, 10);
      this.width > 100 ? this.width = 100 : this.width = this.width;
    } 
  }

}

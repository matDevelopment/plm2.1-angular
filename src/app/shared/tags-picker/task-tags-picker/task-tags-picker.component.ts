import { Component, OnInit, Input, OnDestroy, OnChanges, AfterViewInit } from '@angular/core';
import { TagService } from 'src/app/services/tag.service';
import { TagModel } from 'src/app/models-database/tag.model';
import { Subscription } from 'rxjs';
import { faCopy } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-task-tags-picker',
  templateUrl: './task-tags-picker.component.html',
  styleUrls: ['./task-tags-picker.component.scss']
})
export class TaskTagsPickerComponent implements OnInit, OnDestroy, OnChanges {

  constructor(private tagService: TagService) { }

  MainTags: TagModel[] = [];
  AccessoryTags: TagModel[] = [];
  AccessoryMinorTags: TagModel[] = [];
  @Input() PickedMainTag: TagModel = new TagModel();
  @Input() PickedAccessoryTag: TagModel = new TagModel();
  @Input() PickedAccessoryMinorTag: TagModel = new TagModel();
  @Input() PickedMainTagId;
  @Input() PickedAccessoryTagId;
  @Input() PickedAccessoryMinorTagId;
  @Input() CaseTagId = null;
  EditTaskTagMainName = '';
  EditTaskTagAccessoryName = '';
  EditTaskTagAccessoryMinorName = '';
  NewMainTag: TagModel = new TagModel();
  NewAccessoryTag: TagModel = new TagModel();
  NewAccessoryMinorTag: TagModel = new TagModel();
  RemoveCaseTagSubscription = new Subscription();
  PickCaseTagsSubscription = new Subscription();
  PickTagsSubscritpion = new Subscription();
  emptyTag = new TagModel();
  @Input() AllowEdit = false;
  faCopy = faCopy;

  ngOnInit() {
    this.PickCaseTagsSubscription = this.tagService.addTagsToCaseSubject.subscribe(tags => { this.CaseTagId = tags.ParentTagId;
      this.PickedMainTagId = null; this.GetTaskTagsMain(); });
    this.PickTagsSubscritpion = this.tagService.pickedTaskTagsSubject.subscribe(ids => {
      this.CaseTagId = ids.CaseTagId;
      this.PickedMainTagId = ids.MainTagId;
      this.PickedAccessoryTagId = ids.AccessoryTagId;
      this.PickedAccessoryMinorTagId = ids.AccessoryMinorTagId;
      this.GetTaskTagsMain();
    });
    console.log(this.CaseTagId, this.PickedMainTagId, this.PickedAccessoryTagId, this.PickedAccessoryMinorTagId);
    if (this.CaseTagId != null) {
      //this.GetTaskTagsMain();
    }
    this.onRemoveCaseTagParent();
  }

  GetTaskTagsMain() {
    this.tagService.GetTaskTagsMain(this.CaseTagId).subscribe(
      tags => {
        this.MainTags = tags;
        if (this.PickedMainTagId == null) {
          this.PickedMainTag = new TagModel();
          this.PickedAccessoryTagId = null;
        } else {
          this.PickedMainTag = this.MainTags.find(t => t.Id === this.PickedMainTagId);
        }
        this.GetTaskTagsAccessory(this.PickedMainTagId);
      }
    );
  }

  GetTaskTagsAccessory(id: number) {
    this.tagService.GetTaskTagsAccessory(id).subscribe(
      tags => {
        this.AccessoryTags = tags;
        if (this.PickedAccessoryTagId == null) {
          this.PickedAccessoryTag = new TagModel();
        } else {
          this.PickedAccessoryTag = this.AccessoryTags.find(t => t.Id === this.PickedAccessoryTagId);
        }
        this.GetTaskTagsAccessoryMinor(this.PickedAccessoryTagId);
      }
    );
  }

  GetTaskTagsAccessoryMinor(id: number) {
    this.tagService.GetTaskTagsAccessoryMinor(id).subscribe(
      tags => {
        this.AccessoryMinorTags = tags;
        if (this.PickedAccessoryMinorTagId == null) {
          this.PickedAccessoryMinorTag = null;
        } else {
          this.PickedAccessoryMinorTag = this.AccessoryMinorTags.find(t => t.Id === this.PickedAccessoryMinorTagId);
        }
      }
    );
  }

  pickMainTag(tag: TagModel) {
    if (tag === null) {
      this.PickedMainTagId = null;
      this.PickedMainTag = new TagModel();
    } else {
      this.PickedMainTagId = tag.Id;
      this.PickedMainTag = tag;
    }
    this.PickedAccessoryTag = this.emptyTag;
    this.PickedAccessoryTagId = null;
    this.PickedAccessoryMinorTag = this.emptyTag;
    this.PickedAccessoryMinorTag = null;
    this.EditTaskTagMainName = this.PickedMainTag.Name;
    this.GetTaskTagsAccessory(this.PickedMainTag.Id);
    this.tagService.addTagsToTaskSubject.next({
      MainTag: this.PickedMainTag, AccessoryTag: this.PickedAccessoryTag,
      AccessoryMinorTag: this.PickedAccessoryMinorTag
    });
  }

  pickAccessoryTag(tag: TagModel) {
    if (tag === null) {
      this.PickedAccessoryTagId = null;
      this.PickedAccessoryTag = new TagModel();
    } else {
      this.PickedAccessoryTagId = tag.Id;
      this.PickedAccessoryTag = tag;
    }
    this.PickedAccessoryMinorTag = this.emptyTag;
    this.PickedAccessoryMinorTagId = this.emptyTag.Id;
    this.EditTaskTagAccessoryName = this.PickedAccessoryTag.Name;
    this.GetTaskTagsAccessoryMinor(this.PickedAccessoryTag.Id);
    this.tagService.addTagsToTaskSubject.next({
      MainTag: this.PickedMainTag, AccessoryTag: this.PickedAccessoryTag,
      AccessoryMinorTag: this.PickedAccessoryMinorTag
    });
  }

  pickAccessoryMinorTag(tag: TagModel) {
    if (tag === null) {
      this.PickedAccessoryMinorTagId = null;
      this.PickedAccessoryMinorTag = new TagModel();
    } else {
      this.PickedAccessoryMinorTagId = tag.Id;
      this.PickedAccessoryMinorTag = tag;
    }
    this.EditTaskTagAccessoryMinorName = this.PickedAccessoryMinorTag.Name;
    this.tagService.addTagsToTaskSubject.next({
      MainTag: this.PickedMainTag, AccessoryTag: this.PickedAccessoryTag,
      AccessoryMinorTag: this.PickedAccessoryMinorTag
    });
  }

  addMainTag() {
    this.NewMainTag.CaseTagParentId = this.CaseTagId;
    this.NewMainTag.Id = 0;
    this.tagService.AddTaskTagMain(this.NewMainTag).subscribe(
      tag => { this.MainTags.push(tag); this.NewMainTag = new TagModel(); },
      error => alert('Proszę wybrać typ pracowni')
    );
  }

  copyMainTag(tag) {
    this.tagService.CopyTaskTagMainStructure(tag).subscribe(
      tag => { this.MainTags.push(tag); this.NewMainTag = new TagModel(); alert('Tag Skopiowany'); },
      error => alert('bład przy kopiowaniu tagu')
    );
  }

  copyAccessoryTag(tag) {
    this.tagService.CopyTaskTagAccessoryStructure(tag).subscribe(
      tagModel => { this.AccessoryTags.push(tagModel); this.NewAccessoryTag = new TagModel(); alert('Tag Skopiowany'); },
      error => alert('bład przy kopiowaniu tagu')
    );
  }

  copyAccessoryMinorTag(tag) {
    this.tagService.CopyTaskTagAccessoryMinorStructure(tag).subscribe(
      tagModel => { this.AccessoryMinorTags.push(tagModel); this.NewAccessoryMinorTag = new TagModel(); alert('Tag Skopiowany'); },
      error => alert('bład przy kopiowaniu tagu')
    );
  }

  addAccessoryTag() {
    this.NewAccessoryTag.CaseTagParentId = this.PickedMainTag.Id;
    this.NewAccessoryTag.Id = 0;
    this.tagService.AddTaskTagsAccessory(this.NewAccessoryTag).subscribe(
      tagModel => { this.AccessoryTags.push(tagModel); this.NewAccessoryTag = new TagModel(); },
      error => alert('Proszę wybrać etap zadania')
    );
  }

  addAccessoryMinorTag() {
    this.NewAccessoryMinorTag.CaseTagParentId = this.PickedAccessoryTag.Id;
    this.NewAccessoryMinorTag.Id = 0;
    this.tagService.AddTaskTagsAccessoryMinor(this.NewAccessoryMinorTag).subscribe(
      tagModel => { this.AccessoryMinorTags.push(tagModel); this.NewAccessoryMinorTag = new TagModel(); },
      error => alert('Proszę wybrać typ zadania')
    );
  }

  onRemoveCaseTagParent() {
    this.RemoveCaseTagSubscription = this.tagService.removeCaseTagParentSubject.subscribe(
      () => {
        this.AccessoryMinorTags = [];
        this.AccessoryTags = [];
      }
    );
  }

  removeMainTag() {
    this.tagService.RemoveTaskTagMain(this.PickedMainTag).subscribe(
      tag => {
        this.MainTags.splice(this.MainTags.findIndex(mainTag => tag.Id === mainTag.Id), 1);
        this.PickedMainTag = new TagModel();
        this.pickMainTag(new TagModel());
      },
      error => alert('Błąd upewnij się że etap zadania nie jest przypisany do żadnego zadania')
    );
  }

  removeAccessoryTag() {
    this.tagService.RemoveTaskTagsAccessory(this.PickedAccessoryTag).subscribe(
      tag => {
        this.AccessoryTags.splice(this.AccessoryTags.findIndex(mainTag => tag.Id === mainTag.Id), 1);
        this.PickedAccessoryTag = new TagModel();
        this.pickAccessoryTag(new TagModel());
      },
      error => alert('Błąd upewnij się że typ zadania nie jest przypisany do żadnego zadania')
    );
  }

  removeAccessoryMinorTag() {
    this.tagService.RemoveTaskTagsAccessoryMinor(this.PickedAccessoryMinorTag).subscribe(
      tag => {
        this.AccessoryMinorTags.splice(this.AccessoryMinorTags.findIndex(mainTag => tag.Id === mainTag.Id), 1);
        this.PickedAccessoryMinorTag = new TagModel();
      },
      error => alert('Błąd upewnij się że etap podzadania nie jest przypisany do żadnego zadania')
    );
  }

  editMainTag() {
    this.PickedMainTag.Name = this.EditTaskTagMainName;
    this.tagService.EditTaskTagMain(this.PickedMainTag).subscribe();
  }

  editAccessoryTag() {
    this.PickedAccessoryTag.Name = this.EditTaskTagAccessoryName;
    this.tagService.EditTaskTagsAccessory(this.PickedAccessoryTag).subscribe();
  }

  editAccessoryMinorTag() {
    this.PickedAccessoryMinorTag.Name = this.EditTaskTagAccessoryMinorName;
    this.tagService.EditTaskTagsAccessoryMinor(this.PickedAccessoryMinorTag).subscribe();
  }

  ngOnDestroy(): void {
    this.PickCaseTagsSubscription.unsubscribe();
    this.RemoveCaseTagSubscription.unsubscribe();
    this.PickTagsSubscritpion.unsubscribe();
  }

  ngOnChanges() {
    this.GetTaskTagsMain();
  }
}

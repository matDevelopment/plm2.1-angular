import { Component, OnInit, Input } from '@angular/core';
import { TagModel } from 'src/app/models-database/tag.model';

@Component({
  selector: 'app-tags-picker',
  templateUrl: './tags-picker.component.html',
  styleUrls: ['./tags-picker.component.scss']
})
export class TagsPickerComponent implements OnInit {

  @Input() ShowTaskTags = true;
  @Input() ShowCaseTags = true;
  @Input() AllowEdit = false;
  @Input() CasePaddingTop = 10;
  @Input() TaskPaddingTop = 50;

  @Input() PickedParentTagId: TagModel = null;
  @Input() PickedChildTagId: TagModel = null;
  @Input() TaskTagMain: TagModel = null;
  @Input() TaskTagAccessory: TagModel = null;
  @Input() TaskTagAccessoryMinor: TagModel = null;
  @Input() PickedMainTagId;
  @Input() PickedAccessoryTagId;
  @Input() PickedAccessoryMinorTagId;
  @Input() CaseTagId = null;
  constructor() { }

  ngOnInit() {
  }

}

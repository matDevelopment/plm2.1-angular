import { Component, OnInit, OnDestroy, Input, AfterViewInit } from '@angular/core';
import { TagModel } from 'src/app/models-database/tag.model';
import { TagService } from 'src/app/services/tag.service';
import { faMinus } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-case-tags-picker',
  templateUrl: './case-tags-picker.component.html',
  styleUrls: ['./case-tags-picker.component.scss']
})
export class CaseTagsPickerComponent implements OnInit, OnDestroy {

  constructor(private tagService: TagService) { }

  faMinus = faMinus;
  ParentTags: TagModel[] = [];
  ChildTags: TagModel[] = [];
  PickedParentTag: TagModel = new TagModel();
  PickedChildTag: TagModel = new TagModel();
  EditParentTagName = '';
  EditChildTagName = '';
  NewCaseParentTag: TagModel = new TagModel();
  NewCaseChildTag: TagModel = new TagModel();
  emptyTag = new TagModel();
  @Input() AllowEdit = false;
  changeTagsSubscription = new Subscription();
  @Input() CaseTagParent: TagModel;
  @Input() CaseTagChild: TagModel;
  @Input() PickedParentTagId: number = null;
  @Input() PickedChildTagId: number = null;

  ngOnInit() {
    this.tagService.GetCaseTagsParent().subscribe(
      tags => {
      this.ParentTags = tags;
        if (this.PickedParentTagId != null) {
          this.getChildrenTags();
        }
      }
    );
    this.changeTags();
  }

  changeTags() {
    this.changeTagsSubscription = this.tagService.addTagsToCaseSubject.subscribe(
      model => {
        this.PickedParentTagId = model.ParentTagId;
        this.getChildrenTags();
        this.PickedChildTagId = model.ChildTagId;
      }
    );
  }

  PickParentTag(tag: TagModel) {
    this.PickedParentTag = tag;
    this.PickedParentTagId = tag.Id;
    this.EditParentTagName = this.PickedParentTag.Name;
    this.PickedChildTag = new TagModel();
    this.getChildrenTags();
    this.tagService.addTagsToCaseSubject.next({ ParentTagId: this.PickedParentTagId, ChildTagId: this.PickedChildTagId });
  }

  getChildrenTags() {
    this.tagService.GetCaseTagsChild(this.PickedParentTagId).subscribe(
      tags => this.ChildTags = tags
    );
  }

  addCaseParentTag() {
    this.NewCaseParentTag.Id = 0;
    this.tagService.AddCaseTagParent(this.NewCaseParentTag).subscribe(
      tagModel => { this.ParentTags.push(tagModel); this.NewCaseParentTag = new TagModel(); },
      error => alert('Proszę wybrać typ pracowni')
    );
  }

  addCaseChildTag() {
    this.NewCaseChildTag.CaseTagParentId = this.PickedParentTag.Id;
    this.NewCaseChildTag.Id = 0;
    this.tagService.AddCaseTagChild(this.NewCaseChildTag).subscribe(
      tagModel => { this.ChildTags.push(tagModel); this.NewCaseChildTag = new TagModel(); },
      error => alert('Proszę wybrać typ projektu')
    );
  }

  pickChildTag(tag: TagModel) {
    this.PickedChildTag = tag;
    this.PickedChildTagId = tag.Id;
    this.EditChildTagName = this.PickedChildTag.Name;
    this.tagService.addTagsToCaseSubject.next({ ParentTagId: this.PickedParentTagId, ChildTagId: this.PickedChildTagId });
  }

  removeCaseParentTag() {
    this.tagService.RemoveCaseTagParent(this.PickedParentTag).subscribe(
      tag => {
        this.PickedParentTag = new TagModel();
        this.ParentTags.splice(this.ParentTags.indexOf(this.PickedParentTag), 1);
        this.ChildTags = [];
        this.tagService.removeCaseTagParentSubject.next();
      },
      error => alert('Błąd! Upewnij się że Typ Pracowni nie jest przypisany do żadnego projektu')
    );
  }

  removeCaseChildTag() {
    this.tagService.RemoveCaseTagChild(this.PickedChildTag).subscribe(
      tag => {
        this.PickedChildTag = new TagModel();
        this.ChildTags.splice(this.ChildTags.indexOf(this.PickedChildTag), 1);
      },
      error => alert('Błąd! Upewnij się że Typ Projektu nie jest przypisany do żadnego projektu')
    );
  }

  editPickedParentTag() {
    this.PickedParentTag.Name = this.EditParentTagName;
    this.tagService.EditCaseTagParent(this.PickedParentTag).subscribe();
  }

  editPickedChildTag() {
    this.PickedChildTag.Name = this.EditChildTagName;
    this.ChildTags.find(child => child.Id === this.PickedChildTag.Id).Name = this.EditChildTagName;
    this.tagService.EditCaseTagChild(this.PickedChildTag).subscribe();
  }

  nullParentTags() {
    this.tagService.changeCaseParentIdSubject.next(null);
    this.tagService.addTagsToCaseSubject.next({ ParentTagId: null , ChildTagId: null });
    this.tagService.addTagsToTaskSubject.next({ MainTag: this.emptyTag, AccessoryTag: this.emptyTag, AccessoryMinorTag: this.emptyTag });
  }

  nullChildTags() {
    this.tagService.addTagsToCaseSubject.next({ ParentTagId: null , ChildTagId: null });
  }

  ngOnDestroy(): void {
    this.changeTagsSubscription.unsubscribe();
  }

}

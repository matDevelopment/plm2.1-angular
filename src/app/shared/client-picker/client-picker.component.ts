import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ClientsService } from 'src/app/services/clients.service';
import { ClientModel } from 'src/app/models-database/client.model';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { RepresentativeModel } from 'src/app/models-database/representative.model';

@Component({
  selector: 'app-client-picker',
  templateUrl: './client-picker.component.html',
  styleUrls: ['./client-picker.component.scss']
})

export class ClientPickerComponent implements OnInit, OnDestroy {

  faPlus = faPlus;

  constructor(private clientService: ClientsService) { }

  myControl = new FormControl();

  parentFormSubscription = new Subscription();

  FilteredClientsAsync: Promise<ClientModel[]>;
  Clients: ClientModel[];
  FilteredRepresentativesAsync: Promise<RepresentativeModel[]>;
  Representatives: RepresentativeModel[] = [];
  @Input() ParentForm: FormGroup;
  @Input() ClientId: number = null;

  ngOnInit() {
    this.clientService.GetClients().subscribe(
      clients => {
        this.Clients = clients; this.FilteredClientsAsync = new Promise((resolve, reject) => {
          resolve(this.Clients);
        }
        );
    });

    if (this.ClientId != null) {
      this.getRepresentativesForClient(this.ClientId);
    }

    this.parentFormSubscription = this.ParentForm.statusChanges.subscribe(
      status => {
        if (status === 'PENDING') {
          this.ParentForm.get('client').markAsTouched();
          this.ParentForm.get('representative').markAsTouched();
        }
      }
    );
  }

  onFilterUsers(name: string) {
    this.FilteredClientsAsync = new Promise((resolve, reject) => {
      resolve(this.Clients.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  onFilterRepresentatives(name: string) {
    this.FilteredRepresentativesAsync = new Promise((resolve, reject) => {
      resolve(this.Representatives.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  addClient(_event, client: ClientModel) {
    if (_event.isUserInput) {
      this.getRepresentativesForClient(client.Id);
      this.clientService.AddClientSubject.next(client);
    }
  }

  addRepresentative(_event, representative: RepresentativeModel) {
    if (_event.isUserInput) {
      this.clientService.AddRepresentativeSubject.next(representative);
    }
  }

  getRepresentativesForClient(id) {
    this.clientService.GetRepresentatives(id).subscribe(
      representatives => {
        this.Representatives = representatives;
        const rep = new RepresentativeModel(); rep.Name = 'Brak'; this.Representatives.push(rep);
        this.FilteredRepresentativesAsync = new Promise((resolve, reject) => {
          resolve(representatives);
        }
        );
      }
    );
  }

  ngOnDestroy(): void {
    this.parentFormSubscription.unsubscribe();
  }
}

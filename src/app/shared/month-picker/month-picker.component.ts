import { Component, OnInit } from '@angular/core';
import { EcpService } from 'src/app/services/ecp.service';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-month-picker',
  templateUrl: './month-picker.component.html',
  styleUrls: ['./month-picker.component.scss']
})
export class MonthPickerComponent implements OnInit {

  constructor(private MonthDialogRef: MatDialogRef<MonthPickerComponent>, private ecpService: EcpService) { }

  formGroup = new FormControl('');

  years: string[] = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
    '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027',
    '2028', '2029', '2030', '2031'
  ];

  months: string[] = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj',
    'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];

  startDate: Date;
  endDate: Date;

  ngOnInit() {
    this.startDate = new Date();
    this.endDate = new Date();
  }

  pickYear(year: string) {
   this.startDate.setFullYear(parseInt(year, 10));
   this.endDate.setFullYear(parseInt(year, 10));
  }

  pickMonth(month: number) {
    this.startDate = new Date(this.startDate.getFullYear(), month, 1, 0, 0);
    this.endDate = new Date(this.endDate.getFullYear(), month + 1, 0, 23, 59);
  }

  submit() {
    this.ecpService.ChangeDateSubject.next({ StartDate: this.startDate, EndDate: this.endDate });
    this.MonthDialogRef.close();
  }
}

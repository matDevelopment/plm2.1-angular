import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserModel } from 'src/app/models-database/user.model';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { EcpPanelNewComponent } from 'src/app/ecp/ecp-panel-new/ecp-panel-new.component';

@Component({
  selector: 'app-user-ecp-picker',
  templateUrl: './user-ecp-picker.component.html',
  styleUrls: ['./user-ecp-picker.component.scss']
})
export class UserEcpPickerComponent implements OnInit {

  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any,
  private caseDialogRef: MatDialogRef<UserEcpPickerComponent>) { }
  faUserAlt = faUserAlt;
  ecpPanelComponent = EcpPanelNewComponent;

  ngOnInit() {
    console.log(this.data.Users);
  }

  pickUser(user: UserModel) {
    this.dialog.open(this.ecpPanelComponent, {
      width: '90%',
      maxWidth: '100vw',
      height: '85%',
      panelClass: 'full-screen-modal',
      data: { Date: this.data.Date, UserId: user.Id }
    });
  }
}

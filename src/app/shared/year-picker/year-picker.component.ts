import { Component, OnInit } from '@angular/core';
import { EcpService } from 'src/app/services/ecp.service';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-year-picker',
  templateUrl: './year-picker.component.html',
  styleUrls: ['./year-picker.component.scss']
})
export class YearPickerComponent implements OnInit {

  constructor(private ecpService: EcpService, private yearDialogRef: MatDialogRef<YearPickerComponent>) { }

  formGroup = new FormControl('');

  years: string[] = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017',
    '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2027',
    '2028', '2029', '2030', '2031'
  ];

  startDate = new Date();
  endDate = new Date();

  ngOnInit() {
  }

  pickYear(year: string) {
    this.startDate = new Date(parseInt(year, 10), 0, 1, 0, 0);
    this.endDate = new Date(parseInt(year, 10), 11, 31, 23, 59);
  }

  submit() {
    this.ecpService.ChangeDateSubject.next({ StartDate: this.startDate, EndDate: this.endDate });
    this.yearDialogRef.close();
  }

}

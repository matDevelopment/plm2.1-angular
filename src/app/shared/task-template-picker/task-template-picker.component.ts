import { Component, OnInit, Input } from '@angular/core';
import { TemplateService } from 'src/app/services/template.service';
import { TaskTemplateModel } from 'src/app/models-database/task-template.model';
import { TemplateModel } from 'src/app/models-database/template.model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-task-template-picker',
  templateUrl: './task-template-picker.component.html',
  styleUrls: ['./task-template-picker.component.scss']
})
export class TaskTemplatePickerComponent implements OnInit {

  constructor(private templateService: TemplateService) { }

  templates: TaskTemplateModel[] = [];
  templatesAsync: Promise<TaskTemplateModel[]>;
  @Input() ParentForm: FormGroup;

  ngOnInit() {
    this.getTemplates();
  }

  getTemplates() {
    this.templateService.GetTaskTemplates().subscribe(
      templates => {
        this.templates = templates.filter(t => t.CaseId != null);
        this.templatesAsync = new Promise((resolve, reject) => resolve(this.templates));
      }
    );
  }

  onFilterTemplates(name: string) {
    this.templatesAsync = new Promise((resolve, reject) => {
      resolve(this.templates.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  pickTemplate(_event, template: TaskTemplateModel) {
    if (_event.isUserInput) {
      this.templateService.PickTaskTemplateSubject.next(template);
    }
  }
}

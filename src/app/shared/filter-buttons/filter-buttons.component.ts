import { FilterService } from './../../services/filter.service';
import { CaseService } from 'src/app/services/case.service';
import { Component, OnInit, Input } from '@angular/core';
import { StatusModel } from 'src/app/models-database/status.model';
import { TaskService } from 'src/app/services/task.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-filter-buttons',
  templateUrl: './filter-buttons.component.html',
  styleUrls: ['./filter-buttons.component.scss']
})
export class FilterButtonsComponent implements OnInit {

  expanded = false;
  @Input() defaultText = 'Wszystkie';
  @Input() defaultBackground = '#d4d4d4';
  @Input() defaultColor = '#222222';

  @Input() Type: string;
  @Input() FilterType: string;
  @Input() width: number;
  @Input() statuses: StatusModel[];

  statusId: number;
  priorityId: number;
  changeFilterSubscription = new Subscription();
  @Input() pickedFilter = null;

  constructor(private caseService: CaseService, private taskService: TaskService, private filterService: FilterService) { }

  ngOnInit() {
        this.changeFilterSubscription = this.filterService.filterWithSubfilterSubject.subscribe(
          filter => {
            this.pickedFilter = filter.Filter.StatusId;
          }
        );
  }

  changeDefault(button: StatusModel) {
    this.expanded = false;
    if (button === null) {
      this.defaultText = 'Wszystkie';
      this.defaultBackground = '#222222';
      this.defaultColor = 'white';
      this.ChangeId(null);
    } else {
      this.defaultText = button.Name;
      this.defaultBackground = button.BackgroundColor;
      this.defaultColor = button.Color;
      this.ChangeId(button.Id);
    }
  }

  ChangeId(id) {
    this.pickedFilter = id;
    switch (this.Type) {
      case 'case':
          this.changeForCase(id);
          break;
      case 'task':
          this.changeForTask(id);
          break;
    }
  }

  changeForTask(id) {
    this.taskService.FilterSubject.next(id);
  }

  changeForCase(id) {
    this.caseService.CaseFiltersSubject.next({ Type: this.FilterType, Id: id});
  }

}

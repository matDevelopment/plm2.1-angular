import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import { faAngleDown, faAngleUp, faArrowDown, faArrowUp, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { TablesService } from 'src/app/services/tables.service';
import { Subscription } from 'rxjs';
import { faEdit } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-sub-table',
  templateUrl: './sub-table.component.html',
  styleUrls: ['./sub-table.component.scss']
})
export class SubTableComponent implements OnInit, OnDestroy {

  @Input() tableSubDataObject = 'SubModels';
  @Input() tableSubNames: string[] = ['Nazwa Zadania', 'Użytkownik', 'Czas Rzeczywisty'];
  @Input() tableSubObjects = ['Name', 'Description'];
  @Input() SortTypeSub: string[] = [];
  @Input() SubObjectsDate: number[] = [];
  @Input() ShowSubTable = true;
  @Input() data: any = [];

  pickedSort = '';
  pickedReverse = '';

  faAngleDown = faArrowDown;
  faAngleUp = faArrowUp;
  faTrash = faTrash;
  faPlus = faPlus;
  faEdit = faEdit;

  tablesSubscription = new Subscription();

  constructor(private tablesService: TablesService, private dateService: DateService) { }

  ngOnInit() {
  }

  subEdit(data) {
    this.tablesService.subEditSubject.next(data);
  }

  subRemove(data) {
    this.tablesService.subRemoveSubject.next(data);
  }

  sortString(data, name) {
    return data.sort( (tableObject, nextTableObject) => {
      let nameA = '';
      if (tableObject[name] != null) {
        nameA = tableObject[name].toUpperCase();
      }
      let nameB = '';
      if (nextTableObject[name] != null) {
        nameB = nextTableObject[name].toUpperCase();
      }
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    }
    );
  }

  sortNumber(data, name) {
    return data.sort( (tableObject, nextTableObject) => {
      let a = 0;
      if (tableObject[name] != null) {
        a = parseInt(tableObject[name], 10);
      }
      let b = 0;
      if (nextTableObject[name] != null) {
        b = parseInt(nextTableObject[name], 10);
      }
      return a - b;
    }
    );
  }

  sortDate(data, name) {
    return data.sort( (tableObject, nextTableObject) => {
      let a = new Date();
      if (tableObject[name] != null) {
        a = this.dateService.converCDateToDate(tableObject[name]);
      }
      let b = new Date();
      if (nextTableObject[name] != null) {
        b = this.dateService.converCDateToDate(nextTableObject[name]);
      }
      return a.getTime() - b.getTime();
    }
    );
  }

  sortSubTablesData(subTableId: number, nameid: number, sortName: string) {
    const name = this.tableSubObjects[nameid];
    this.pickedSort = sortName;
    this.pickedReverse = '';
    const sortType = this.SortTypeSub[nameid];
    switch (sortType) {
      case 'string':
      this.sortString(this.data[this.tableSubDataObject], name);
      break;
      case 'number':
      this.sortNumber(this.data[this.tableSubDataObject], name);
      break;
      case 'date':
      this.sortDate(this.data[this.tableSubDataObject], name);
      break;
    }
  }

  sortReverseSubTableData(sortName: string) {
    this.pickedSort = '';
    this.pickedReverse = sortName;
    this.data[this.tableSubDataObject].reverse();
  }

  ngOnDestroy(): void {
    this.tablesSubscription.unsubscribe();
  }
}

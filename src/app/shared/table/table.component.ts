import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { DateService } from 'src/app/services/date.service';
import { faAngleDown, faAngleUp, faArrowDown, faArrowUp, faTrash, faPlus } from '@fortawesome/free-solid-svg-icons';
import { TableDataModel } from 'src/app/models/table-data.model';
import { TablesService } from 'src/app/services/tables.service';
import { Subscription } from 'rxjs';
import { saveAs } from 'file-saver';
import { faEdit } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {

  @Input() tableData: any[];
  @Input() tableNames: string[] = ['Nazwa Projekt', 'Użytkownik', 'Czas Poświęcony'];
  @Input() tableObjects = ['Name'];
  @Input() MainObjectsDate: number[] = [];
  @Input() SortTypeMain: string[] = [];

  @Input() tableSubDataObject = 'SubModels';
  @Input() tableSubNames: string[] = ['Nazwa Zadania', 'Użytkownik', 'Czas Rzeczywisty'];
  @Input() tableSubObjects = ['Name', 'Description'];
  @Input() SortTypeSub: string[] = [];
  @Input() SubObjectsDate: number[] = [];
  @Input() ShowSubTable = true;

  pickedSort = '';
  pickedReverse = '';

  faAngleDown = faArrowDown;
  faAngleUp = faArrowUp;
  faTrash = faTrash;
  faPlus = faPlus;
  faEdit = faEdit;

  tablesSubscription = new Subscription();

  constructor(private tablesService: TablesService, private dateService: DateService) { }

  ngOnInit() {
    if (this.SortTypeMain.length === 0) {
      this.tableNames.forEach(() => {
        this.SortTypeMain.push('string');
      });
    }

    if (this.SortTypeSub.length === 0) {
      this.tableNames.forEach(() => {
        this.SortTypeSub.push('string');
      });
    }
    this.exportJsonToExcel();
  }

  add(data) {
    this.tablesService.addSubject.next(data);
  }

  edit(data) {
    this.tablesService.editSubject.next(data);
  }

  remove(data) {
    this.tablesService.removeSubject.next(data);
  }

  sortTablesData(nameid: number, sortName: string) {
    const name = this.tableObjects[nameid];
    const sortType = this.SortTypeMain[nameid];
    this.pickedSort = sortName;
    this.pickedReverse = '';
    switch (sortType) {
      case 'string':
      this.sortString(this.tableData, name);
      break;
      case 'number':
      this.sortNumber(this.tableData, name);
      break;
      case 'date':
      this.sortDate(this.tableData, name)
      break;
    }
  }

  sortString(data, name) {
    return data.sort( (tableObject, nextTableObject) => {
      let nameA = '';
      if (tableObject[name] != null) {
        nameA = tableObject[name].toUpperCase();
      }
      let nameB = '';
      if (nextTableObject[name] != null) {
        nameB = nextTableObject[name].toUpperCase();
      }
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    }
    );
  }

  sortNumber(data, name) {
    return data.sort( (tableObject, nextTableObject) => {
      let a = 0;
      if (tableObject[name] != null) {
        a = parseInt(tableObject[name], 10);
      }
      let b = 0;
      if (nextTableObject[name] != null) {
        b = parseInt(nextTableObject[name], 10);
      }
      return a - b;
    }
    );
  }

  sortDate(data, name) {
    return data.sort( (tableObject, nextTableObject) => {
      let a = new Date();
      if (tableObject[name] != null) {
        a = this.dateService.converCDateToDate(tableObject[name]);
      }
      let b = new Date();
      if (nextTableObject[name] != null) {
        b = this.dateService.converCDateToDate(nextTableObject[name]);
      }
      return a.getTime() - b.getTime();
    }
    );
  }

  sortReverseTableData(sortName: string) {
    this.pickedSort = '';
    this.pickedReverse = sortName;
    this.tableData.reverse();
  }

  exportJsonToExcel() {
    this.tablesSubscription = this.tablesService.exportJsonToExcelSubject.subscribe(
      () => {
        const json = {
          Names: this.tableNames,
          SubNames: this.tableSubNames,
          ObjectNames: this.tableObjects,
          ObjectSubNames: this.tableSubObjects,
          Data: this.tableData
        };
        this.tablesService.ExportToExcel(json).subscribe(
         res => saveAs(new Blob([res], { type: 'application/vnd.ms-excel' }), 'excel.xlsx')
        );
      }
    );
  }

  ngOnDestroy(): void {
    this.tablesSubscription.unsubscribe();
  }

}
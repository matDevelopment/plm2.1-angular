import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-icon-popover',
  templateUrl: './icon-popover.component.html',
  styleUrls: ['./icon-popover.component.scss']
})
export class IconPopoverComponent implements OnInit {


  @Input() Icon;
  @Input() Text;
  @Input() Size = 25;
  @Input() Color = 'white';
  @Input() Width = 130;
  @Input() Left = 130;
  @Input() FontSize = 13;
  leftMinus;
  constructor() { }

  ngOnInit() {
    this.leftMinus = (this.Left * (-1));
  }

}

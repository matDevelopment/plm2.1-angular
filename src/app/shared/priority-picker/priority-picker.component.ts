import { Component, OnInit, Input } from '@angular/core';
import { FilterService } from 'src/app/services/filter.service';
import { PriorityModel } from 'src/app/models-database/priority.model';

@Component({
  selector: 'app-priority-picker',
  templateUrl: './priority-picker.component.html',
  styleUrls: ['./priority-picker.component.scss']
})
export class PriorityPickerComponent implements OnInit {

  constructor(private filterService: FilterService) { }

  priorities: PriorityModel[] = [];
  @Input() pickedPriorityId = 0;

  ngOnInit() {
    this.filterService.GetPriorities().subscribe(
      priorities => this.priorities = priorities
    );
  }

  changePriority(priority: PriorityModel) {
    this.pickedPriorityId = priority.Id;
    this.filterService.prioritySubject.next(priority);
  }

}

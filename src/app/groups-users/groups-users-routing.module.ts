import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { RolesModel } from '../models/roles-model';
import { AuthGuard } from '../guards/auth.guard';

const appRoutes: Routes = [
    { path: 'users', component: UsersComponent, data: { roles: [RolesModel.Admin, RolesModel.ProjectManager]}, canActivate: [AuthGuard]},
    { path: 'groups', component: GroupsComponent,
    data: { roles: [RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader, RolesModel.User]}, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class GroupsUsersRoutingModule {  }

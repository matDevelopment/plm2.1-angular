import { Component, OnInit, Inject } from '@angular/core';
import { PaymentPeriodModel } from 'src/app/models-database/payment-period.model';
import { UserModel } from 'src/app/models-database/user.model';
import { DateService } from 'src/app/services/date.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-payment-panel',
  templateUrl: './payment-panel.component.html',
  styleUrls: ['./payment-panel.component.scss']
})
export class PaymentPanelComponent implements OnInit {

  constructor(private dateService: DateService, @Inject(MAT_DIALOG_DATA) public data: any) { }

  user = new UserModel();
  newPaymentPeriod = new PaymentPeriodModel();

  ngOnInit() {
  }

  changeStartDate() {
    this.newPaymentPeriod.StartDate = this.dateService.convertToCDate(this.newPaymentPeriod.StartJsDate);
  }

  changeFinishDate() {
    this.newPaymentPeriod.FinishDate = this.dateService.convertToCDate(this.newPaymentPeriod.FinishJSDate);
  }

  addPaymentPeriod() {
    this.newPaymentPeriod.StartDate = this.dateService.convertToCDate(this.newPaymentPeriod.StartJsDate);
    this.newPaymentPeriod.FinishDate = this.dateService.convertToCDate(this.newPaymentPeriod.FinishJSDate);
    this.user.PaymentPeriods.push(this.newPaymentPeriod);
  }

}

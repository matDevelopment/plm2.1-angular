import { Component, OnInit, Inject } from '@angular/core';
import { UserModel } from 'src/app/models-database/user.model';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private userService: UserService
    , private authService: AuthService, private userDialogRef: MatDialogRef<UserEditComponent>) { }

  newUser: UserForRegister;
  type = 'new';

  editUser() {
    if (this.type === 'new') {
      this.authService.register(this.newUser).subscribe(
        () => {
          const user = new UserModel();
          user.Name = this.newUser.Name;
          user.Email = this.newUser.Email;
          user.FirstName = this.newUser.FirstName;
          user.Surname = this.newUser.Surname;
          user.Roles = [RolesModel.User];
          this.userService.AddUserSubject.next(user);
          this.userDialogRef.close();
        });
    } else {
      const user = new UserModel();
      user.Id = this.data.user.Id;
      user.Name = this.newUser.Name;
      user.Email = this.newUser.Email;
      user.FirstName = this.newUser.FirstName;
      user.Surname = this.newUser.Surname;
      user.Roles = [RolesModel.User];
      this.userService.EditUser(user).subscribe(
        () => { this.userService.EditUserSubject.next(user); this.userDialogRef.close() }
      );
    }
  }

  ngOnInit() {
    if (this.data.user != null) {
      this.newUser = this.data.user;
      this.type = 'edit';
    } else {
      this.newUser = new UserForRegister(null, null, null, null);
      this.type = 'new';
    }
  }

}


export class UserForRegister {
  constructor(public Name: string, public Email: string, public FirstName: string, public Surname: string) { }
}
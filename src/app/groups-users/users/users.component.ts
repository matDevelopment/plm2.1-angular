import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { UserModel } from 'src/app/models-database/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { RolesModel } from 'src/app/models/roles-model';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { PaymentPanelComponent } from './payment-panel/payment-panel.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {

  constructor(private authService: AuthService, private userService: UserService, private PaymentDialog: MatDialog,
    private dialog: MatDialog) { }

  users: UserModel[] = [];
  roles: string[] = [];
  paymentPanelComponent = PaymentPanelComponent;
  faUser = faUserAlt;
  addUserSubscription = new Subscription();
  editUserSubscription = new Subscription();

  ngOnInit() {
    this.getUsers();
    this.getRoles();
    this.addUserSubscription = this.userService.AddUserSubject.subscribe(user => { this.users.push(user); alert('Użytkownik dodany'); }
    );
    this.editUserSubscription = this.userService.EditUserSubject.subscribe(userEdited =>
      this.users[this.users.findIndex(u => u.Id === userEdited.Id)] = userEdited
    );
  }

  getUsers() {
    this.userService.GetUsers().subscribe(
      users => {
        this.users = users;
      }
    );
  }

  getRoles() {
    this.userService.getRoles().subscribe(
      roles => {
        roles.forEach(role => this.roles.push(role.Name));
      }
    );
  }

  changeRole(role: string, user: UserModel) {
    const roles: string[] = [role];
    user.Roles = roles;
    this.userService.changeRoles(user.Name, roles).subscribe();
  }

  openPaymentDialog(user) {
    this.PaymentDialog.open(this.paymentPanelComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '80%',
      panelClass: 'full-screen-modal',
      data: { User: user, Users: this.users }
    });
  }

  deleteUser(user: UserModel) {
    this.authService.deleteUser(user).subscribe(
      res => this.users.splice(this.users.findIndex(u => u.Id === user.Id), 1),
      error => alert('Upewnij się że użytkownik nie jest przypisany do żadnego projektu lub grupy')
    );
  }

  resetPassword(user: UserModel) {
    this.authService.resetPassword(user).subscribe(
      res => alert('nowe hasło: ' + res)
    );
  }

  showEditDialog(user) {
    this.dialog.open(UserEditComponent, {
      width: '50vw',
      maxWidth: '100vw',
      panelClass: 'full-screen-modal',
      data: { user: user }
    });
  }

  ngOnDestroy(): void {
    this.addUserSubscription.unsubscribe();
    this.editUserSubscription.unsubscribe();
  }

}


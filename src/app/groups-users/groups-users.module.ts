import { GroupComponent } from './groups/group/group.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule, MatDialogModule, MatSelectModule, MatInputModule,
    MatAutocompleteModule,
    MatIconModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule} from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { GroupService } from '../services/group.service';
import { UserService } from '../services/user.service';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupsUsersRoutingModule } from './groups-users-routing.module';
import { NewGroupComponent } from './groups/new-group/new-group.component';
import { PaymentPanelComponent } from './users/payment-panel/payment-panel.component';
import { SharedModule } from '../shared/shared.module';
import { UserEditComponent } from './users/user-edit/user-edit.component';

@NgModule({
   declarations: [
    UsersComponent,
    GroupsComponent,
    GroupComponent,
    NewGroupComponent,
    PaymentPanelComponent,
    UserEditComponent,
   ],
   imports: [
    CommonModule,
    GroupsUsersRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    FontAwesomeModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatAutocompleteModule,
    FormsModule,
    MatIconModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule
   ],
   entryComponents: [ NewGroupComponent, PaymentPanelComponent, UserEditComponent ],
   providers: [GroupService, UserService],
   bootstrap: []
})
export class GroupsUsersModule { }

import { GroupService } from 'src/app/services/group.service';
import { OnInit, Component, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { GroupModel } from 'src/app/models-database/group.model';
import { UserModel } from 'src/app/models-database/user.model';
import { faUserAlt, faUserFriends, faPlus} from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-regular-svg-icons';
import { MatDialog } from '@angular/material';
import { NewGroupComponent } from './new-group/new-group.component';
import { Subscription } from 'rxjs';
import { FilterModel } from 'src/app/models/filter.model';
import { RolesModel } from 'src/app/models/roles-model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})

export class GroupsComponent implements OnInit, OnDestroy {

  constructor(private groupService: GroupService, private userService: UserService, private dialog: MatDialog,
    private authService: AuthService) { }

  faStar = faStar;
  faUserFriends = faUserFriends;
  faUserAlt = faUserAlt;
  faPlus = faPlus;
  rolesModel = RolesModel;
  expandAll = false;
  Groups: GroupModel[] = [];
  Users: UserModel[] = [];
  NewGroup: GroupModel = new GroupModel();
  groupFilter = new FilterModel();

  newGroupSubscription = new Subscription();
  removeGroupSubscription = new Subscription();

  ngOnInit() {
    if (this.authService.roleMatch([RolesModel.User, RolesModel.TeamLeader])) {
      this.groupFilter.UserId = this.authService.decodedToken.nameid;
    }
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.groupFilter.TeamLeaderId = this.authService.decodedToken.nameid;
    }

    this.groupService.GetFilteredGroups(this.groupFilter).subscribe(
      resp => {
        this.Groups = resp;
      }
    );
    this.userService.GetUsers().subscribe(
      resp => {
        this.Users = resp;
      }
    );

    this.groupService.NewGroupSubject.subscribe(
      group => this.Groups.push(group)
    );

    this.groupService.RemoveGroupSubject.subscribe(
      group => this.Groups.splice(this.Groups.indexOf(group), 1)
    );
  }

  addGroup() {
    this.dialog.open(NewGroupComponent, {
      width: '40%',
      data: { Type: 'new', Users: this.Users}
    });
  }

  ngOnDestroy(): void {
    this.newGroupSubscription.unsubscribe();
    this.removeGroupSubscription.unsubscribe();
  }

}

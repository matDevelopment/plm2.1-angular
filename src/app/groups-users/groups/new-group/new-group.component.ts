import { Component, OnInit, Input, Inject } from '@angular/core';
import { UserModel } from 'src/app/models-database/user.model';
import { GroupModel } from 'src/app/models-database/group.model';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { faUserAlt, faUserFriends, faPlus, faMinus, faStar} from '@fortawesome/free-solid-svg-icons';
import { GroupService } from 'src/app/services/group.service';

@Component({
  selector: 'app-new-group',
  templateUrl: './new-group.component.html',
  styleUrls: ['./new-group.component.scss']
})
export class NewGroupComponent implements OnInit {

  faPlus = faPlus;
  faMinus =  faMinus;
  faStar = faStar;

  @Input() Users: UserModel[];

  FilteredUsersAsync: Promise<UserModel[]>;
  NewGroup = new GroupModel();
  myControl = new FormControl();

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private groupDialogRef: MatDialogRef<NewGroupComponent>,
  private groupService: GroupService) { }

  ngOnInit() {
    this.Users = this.data.Users;
    this.NewGroup.Users = [];
  }

  onFilterUsers (name: string) {
    this.sortUsers();
    this.FilteredUsersAsync = new Promise( (resolve, reject) => {
      resolve(this.Users.filter(value =>  value.Name.toLowerCase().includes(name.toLowerCase()) ));
    });
  }

  sortUsers() {
    this.NewGroup.Users.forEach(
      userInGroup => {
          this.Users.forEach(user => {
          if (user.Id === userInGroup.Id) {
            this.Users.splice(this.Users.indexOf(user), 1);
          }
        });
      }
    );
    this.FilteredUsersAsync = new Promise( (resolve, reject) => {
      resolve(this.Users);
    });
  }

  addUserToGroup(user: UserModel) {
    this.NewGroup.Users.push(user);
  }

  removeUserFromGroup(index: number, user) {
    this.NewGroup.Users.splice(index, 1);
    this.Users.push(user);
  }

  closeDialog() {
    if (confirm('Czy na pewno chcesz anulować tworzenie Zlecenia?')) {
      this.groupDialogRef.close();
    }
  }

  changeTeamLeader(id: number) {
    this.NewGroup.TeamLeaderId = id;
  }

  removeTeamLeader() {
    this.NewGroup.TeamLeaderId = null;
  }

  addGroup() {
    if (this.checkProperties(this.NewGroup)) {
      this.groupService.AddGroup(this.NewGroup).subscribe(
        group => {this.groupService.NewGroupSubject.next(group); this.groupDialogRef.close(); }
      );
    } else {
      alert('Wszystkie pola muszą być wypełnione');
    }
  }

  checkProperties(obj) {
    for (const key in obj) {
        if (obj[key] === null || obj[key] === '') {
          if (key.toString() === 'TeamLeaderId') {
          } else {
              return false;
          }
        }
    }
    return true;
}

}

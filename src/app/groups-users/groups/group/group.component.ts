import { UserModel } from './../../../models-database/user.model';
import { GroupModel } from './../../../models-database/group.model';
import { Component, Input, OnInit } from '@angular/core';
import { faUserAlt, faUserFriends, faAngleDown, faAngleDoubleUp, faStar, faMinus, faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { GroupService } from 'src/app/services/group.service';
import { RolesModel } from 'src/app/models/roles-model';
import { FilterModel } from 'src/app/models/filter.model';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  faUserAlt = faUserAlt;
  faUserFriends = faUserFriends;
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  rolesModel = RolesModel;
  faAngleDoubleUp = faAngleDoubleUp;
  faStar = faStar;
  faMinus = faMinus;
  @Input() expanded = false;
  @Input() faStarRegular;
  @Input() Group: GroupModel;
  @Input() Users: UserModel[];

  pickedTeamLeader: UserModel = new UserModel();
  TeamLeaders: UserModel[];
  FilteredTeamLeadersAsync: Promise<UserModel[]>;

  FilteredUsersAsync: Promise<UserModel[]>;

  constructor(private userService: UserService, private groupService: GroupService) { }

  myControl = new FormControl();

  ngOnInit(): void {
    this.userService.getTeamLeaders().subscribe(
      users => {
        this.TeamLeaders = users; const emptyUser = new UserModel();
        emptyUser.Name = 'Brak';
        this.TeamLeaders.push(emptyUser); this.FilteredTeamLeadersAsync = new Promise((resolve, reject) => {
          resolve(this.TeamLeaders);
        });
      }
    );
    this.getTeamLeader();
  }

  getTeamLeader() {
    if (this.Group.TeamLeaderId != null) {
      this.userService.getUser(this.Group.TeamLeaderId).subscribe(
        user => this.pickedTeamLeader = user
      );
    } else {
      const user = new UserModel();
      user.Name = 'Brak';
      this.pickedTeamLeader = user;
    }
  }

  onFilterUsers(name: string) {
    this.sortUsers();
    this.FilteredUsersAsync = new Promise((resolve, reject) => {
      resolve(this.Users.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  onFilterTeamLeaders(name: string) {
    this.FilteredTeamLeadersAsync = new Promise((resolve, reject) => {
      resolve(this.TeamLeaders.filter(value => value.Name.toLowerCase().includes(name.toLowerCase())));
    });
  }

  sortUsers() {
    this.Group.Users.forEach(
      userInGroup => {
        this.Users.forEach(user => {
          if (user.Id === userInGroup.Id) {
            this.Users.splice(this.Users.indexOf(user), 1);
          }
        });
      }
    );
    this.FilteredUsersAsync = new Promise((resolve, reject) => {
      resolve(this.Users);
    });
  }

  addUserToGroup(user: UserModel) {
    this.userService.AddUserToGroup(user, this.Group).subscribe(
      resp => { this.Group.Users.push(resp); this.sortUsers(); }
    );
  }

  removeUserFromGroup(user: UserModel, index: number) {
    this.userService.RemoveUserFromGroup(user, this.Group).subscribe(
      resp => { this.Group.Users.splice(index, 1); }
    );
  }

  setGroupTeamLeader(_event: any, userId: number) {
    if (_event.isUserInput) {
      this.Group.TeamLeaderId = userId;
      if (userId === null) {
        this.groupService.RemoveTeamLeaderFromGroup(this.Group).subscribe();
      } else {
        this.groupService.SetGroupTeamLeader(this.Group).subscribe();
      }
      this.getTeamLeader();
    }
  }


  removeGroup(group: GroupModel) {
    if (confirm('Czy na pewno chcesz usunąć grupę?')) {
      this.groupService.RemoveGroup(group).subscribe(
        () => this.groupService.RemoveGroupSubject.next(group),
        error => alert('Błąd! Grupa nie została usunięta')
      );
    }
  }

}

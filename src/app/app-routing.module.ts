import { AuthComponent } from './menu/auth/auth.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TagsEditPanelComponent } from './tags-edit-panel/tags-edit-panel.component';

const appRoutes: Routes = [
    { path: 'tags', component: TagsEditPanelComponent },
    { path: 'auth', component: AuthComponent},
    { path: '', redirectTo: '/auth', pathMatch: 'full'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {  }

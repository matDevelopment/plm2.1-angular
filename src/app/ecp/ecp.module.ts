
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UserService } from '../services/user.service';
import { EcpRoutingModule } from './ecp-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UserEcpPickerComponent } from '../shared/user-ecp-picker/user-ecp-picker.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EcpCalendarComponent } from './ecp-calendar/ecp-calendar.component';
import { EcpTimelineComponent } from './ecp-calendar/ecp-timeline/ecp-timeline.component';
import { EcpTilesComponent } from './ecp-tiles/ecp-tiles.component';
import { UserTableComponent } from './ecp-calendar/user-table/user-table.component';


@NgModule({
   declarations: [
      UserEcpPickerComponent,
      EcpCalendarComponent,
      EcpTimelineComponent,
      EcpTilesComponent,
      UserTableComponent,
   ],
   imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    EcpRoutingModule,
    FontAwesomeModule,
   ],
   entryComponents: [ UserEcpPickerComponent, EcpTilesComponent ],
   providers: [UserService],
   bootstrap: []
})
export class EcpModule { }

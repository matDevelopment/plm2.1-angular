import { Component, OnInit } from '@angular/core';
import { DayModel } from 'src/app/models/day.model';
import { MatDialog } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { FilterModel } from 'src/app/models/filter.model';
import { DateService } from 'src/app/services/date.service';
import { AuthService } from 'src/app/services/auth.service';
import { UserEcpPickerComponent } from 'src/app/shared/user-ecp-picker/user-ecp-picker.component';
import { RolesModel } from 'src/app/models/roles-model';
import { EcpPanelNewComponent } from '../ecp-panel-new/ecp-panel-new.component';
import { EcpService } from 'src/app/services/ecp.service';
import { EcpTilesComponent } from '../ecp-tiles/ecp-tiles.component';
import { faUserTag } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-ecp-calendar',
  templateUrl: './ecp-calendar.component.html',
  styleUrls: ['./ecp-calendar.component.scss']
})
export class EcpCalendarComponent implements OnInit {

  constructor(private dialog: MatDialog, private userService: UserService,
    private dateService: DateService, public authService: AuthService, public ecpService: EcpService) { }

  faUserTag = faUserTag;
  dayNames: string[] = [];
  days: DayModel[] = [];
  Date: Date = new Date();
  ecpPanelComponent = EcpPanelNewComponent;
  userEcpPickerComponent = UserEcpPickerComponent;
  ecpTilesComponent = EcpTilesComponent;
  picked = 'Moich Zadaniach';
  expanded = false;
  filterType = 1;
  rolesModel = RolesModel;
  startDate = new Date();
  endDate = new Date();
  UserId = this.authService.decodedToken.nameid;

  ngOnInit() {
    this.initDayNames();
    this.initDays();
    this.startDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth(), 1, 0, 0);
    this.endDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth() + 1, 0, 23, 59);
  }

  initDayNames() {
    this.dayNames = [
      'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'
    ];
  }

  initDays() {
    this.days = [];
        let startingDay = new Date(this.Date.getFullYear(), this.Date.getMonth(), 0).getDay();
        const startDay = startingDay;
        const monthLength = new Date(this.Date.getFullYear(), this.Date.getMonth() + 1, 0).getDate();

        let day = 1;
        for (let i = 0; i < 42; i++) {
          if (i === startingDay && i < monthLength + startDay) {
            const date = new Date(this.Date.getFullYear(), this.Date.getMonth(), day, 0, 0, 0, 0);
            this.days.push(new DayModel(day, date, false, false));
            day += 1;
            startingDay += 1;
          } else {
            this.days.push(new DayModel(null, this.Date, true, false));
          }
        }
    !this.authService.roleMatch([RolesModel.User]) ?
    this.ecpService.GetReportedDaysInMonth(this.Date.getFullYear(), this.Date.getMonth() + 1).subscribe(
      res => {
        this.days.forEach(dayModel => {
          if (res[dayModel.day - 1]) {
            dayModel.IsTimeReported = true;
          }
        });
      }
    ) :
    this.ecpService.GetReportedDaysInMonthForUser(this.Date.getFullYear(), this.Date.getMonth() + 1, this.UserId).subscribe(
      res => {
        this.days.forEach(dayModel => {
          if (dayModel.day > 0 && dayModel.day != null) {
            if (res[dayModel.day - 1].Worked) {
              dayModel.IsTimeReported = true;
              dayModel.TimeWorked = res[dayModel.day - 1].Time;
            }
          }
        });
      }
    );
  }

  onPreviousMonth() {
    this.Date = new Date(this.Date.getFullYear(), this.Date.getMonth(), 0);
    this.initDays();
  }

  onNextMonth() {
    this.Date = new Date(this.Date.getFullYear(), this.Date.getMonth() + 2, 0);
    this.initDays();
  }

  openEcpDialog(date: Date) {
    switch (this.filterType) {
      case 1:
        this.dialog.open(this.ecpPanelComponent, {
          width: '90%',
          maxWidth: '100vw',
          height: '85%',
          panelClass: 'full-screen-modal',
          data: { Date: date, UserId: this.authService.decodedToken.nameid }
        });
        break;
      case 2:
        const filter = new FilterModel();
        if (!this.authService.roleMatch([RolesModel.Admin, RolesModel.ProjectManager])) {
          filter.TeamLeaderId = this.authService.decodedToken.nameid;
        }
        this.userService.getFilteredUsers(filter)
          .subscribe(
            users => this.dialog.open(this.userEcpPickerComponent, {
              width: '45%',
              maxWidth: '100vw',
              height: '60%',
              panelClass: 'full-screen-modal',
              data: { Date: date, Users: users }
            })
          );
        break;
      case 3:
        this.dialog.open(this.ecpTilesComponent, {
          width: '90%',
          maxWidth: '100vw',
          height: '85%',
          panelClass: 'full-screen-modal',
          data: { Date: date, UserId: this.authService.decodedToken.nameid }
        });
        break;
    }
  }

  loadChart(event) {
    this.ecpService.loadTimelineSubject.next(event.index);
  }

  changeDate() {
    this.ecpService.changeTimelineDateSubject.next({ StartTime: this.startDate, FinishTime: this.endDate });
  }

  changeFiltering(type: number, text: string) {
    this.picked = text;
    this.expanded = false;
    this.filterType = type;
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { TableDataModel } from 'src/app/models/table-data.model';
import { TablesService } from 'src/app/services/tables.service';
import { FilterModel } from 'src/app/models/filter.model';
import { AuthService } from 'src/app/services/auth.service';
import { EcpService } from 'src/app/services/ecp.service';
import { DateService } from 'src/app/services/date.service';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {

  constructor(private tablesService: TablesService, private authService: AuthService,
    private ecpService: EcpService, private dateService: DateService) { }

  userWithTasks: TableDataModel[] = [];
  filter = new FilterModel();
  subFilter = new FilterModel();

  ngOnInit() {
    this.getUsersWithTasksWithEcps();
    this.ecpService.changeTimelineDateSubject.subscribe(time => {
      this.filter.StartTime = this.dateService.convertToCDate(time.StartTime);
      this.filter.FinishTime = this.dateService.convertToCDate(time.FinishTime);
      this.subFilter.StartTime = this.dateService.convertToCDate(time.StartTime);
      this.subFilter.FinishTime = this.dateService.convertToCDate(time.FinishTime);
      this.getUsersWithTasksWithEcps();
    });
  }

  getUsersWithTasksWithEcps() {
    this.userWithTasks = [];
    this.filter.UserId = this.authService.decodedToken.nameid;
    this.tablesService.GetUsersWithTasksWithEcps(this.filter, this.subFilter).subscribe(
      res => {
        res.forEach(
          response => {
            const userWithTasks = new TableDataModel(response.User, response.Tasks, response.Ecps, 'Task', true);
            this.userWithTasks.push(userWithTasks);
          }
        );
      });
  }

}

import { Subject, Subscription } from 'rxjs';
import { Component, OnInit, NgZone, AfterViewInit, OnDestroy } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { EcpService } from 'src/app/services/ecp.service';
import { EcpModel } from 'src/app/models/ecp.model';
import { DateService } from 'src/app/services/date.service';
import { FilterModel } from 'src/app/models/filter.model';
import { MatDialog } from '@angular/material';
import { EcpPanelNewComponent } from 'src/app/ecp/ecp-panel-new/ecp-panel-new.component';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-ecp-timeline',
  templateUrl: './ecp-timeline.component.html',
  styleUrls: ['./ecp-timeline.component.scss']
})
export class EcpTimelineComponent implements OnInit, OnDestroy {

  private chart: am4charts.XYChart;
  startDate: Date = new Date();
  endDate: Date = new Date();
  ecps: EcpModel[] = [];
  ecpPanelComponent = EcpPanelNewComponent;
  dialogSubject = new Subject<any>();
  dialogSubscription = new Subscription();
  rolesModel = RolesModel;
  filter = new FilterModel();

  constructor(private zone: NgZone, private ecpService: EcpService, private dateService: DateService, private dialog: MatDialog
    , private authService: AuthService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.startDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth(), 1 , 0, 0);
    this.endDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth() + 1, 0, 23, 59);
    this.filter.StartTime = this.dateService.convertToCDate(this.startDate);
    this.filter.FinishTime = this.dateService.convertToCDate(this.endDate);
    this.ecpService.loadTimelineSubject.subscribe(
      index => {
        if ((this.chart === undefined || this.chart === null) && index === 1 ) {
          setTimeout(() => {this.init(); }, 600);
        }
      }
    );
    this.ecpService.changeTimelineDateSubject.subscribe(
      dateModel => { this.startDate = dateModel.StartTime;
      this.endDate = dateModel.FinishTime;
      if (this.chart !== undefined && this.chart !== null) {
        this.removeChart();
        this.getEcps();
      }
      }
    );
  }

  init() {
    if (!this.authService.roleMatch([RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader])) {
      this.filter.UserId = this.authService.decodedToken.nameid;
    }
    this.getEcps();
    this.onDialog();
  }

  getEcps() {
    this.filter.StartTime = this.dateService.convertToCDate(this.startDate);
    this.filter.FinishTime = this.dateService.convertToCDate(this.endDate);
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.filter.TeamLeaderId = this.authService.decodedToken.nameid;
    }
    this.ecpService.GetEcps(this.filter).subscribe(
      res => this.createChart(res)
    );
  }

  createChart(ecps: EcpModel[]) {
    this.ecps = ecps;
    this.zone.runOutsideAngular(() => {
      const chart = am4core.create('chartdiv', am4charts.XYChart);

      chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

      chart.paddingRight = 30;
      chart.language.locale = new am4core.Language().locale;
      chart.dateFormatter.inputDateFormat = 'yyyy-MM-dd HH:mm';

      const colorSet = new am4core.ColorSet();
      colorSet.saturation = 0.4;
      let brighten = 0;
      document.getElementById('chartdiv').style.height = '200px';
      this.ecps.forEach(ecp => {
        brighten += 0.2;
        const taskName = ecp.Task ? ecp.Task.Name : 'Poza Zadaniem';
        const caseName = ecp.Case ? ecp.Case.Name : 'Poza Projektem';
        const startTime = this.dateService.converCDateToDate(ecp.StartTime);
        const finishTime = this.dateService.converCDateToDate(ecp.FinishTime);
        const date = this.dateService.converCDateToDate(ecp.Date);
        chart.data.push(
          {
            name: ecp.User.Name,
            task: taskName,
            case: caseName,
            Task: ecp.Task,
            UserId: ecp.UserId,
            Date: date,
            date: new DatePipe('en-US').transform(date, 'yyyy-MM-dd'),
            startTime: new DatePipe('en-US').transform(startTime, 'HH:mm'),
            finishTime: new DatePipe('en-US').transform(finishTime, 'HH:mm'),
            fromDate: this.dateService.convertToAm4Date(ecp.StartTime),
            toDate: this.dateService.convertToAm4Date(ecp.FinishTime),
            color: colorSet.getIndex(ecp.UserId).brighten(brighten)
          }
        );
        document.getElementById('chartdiv').style.height =
          (parseInt(document.getElementById('chartdiv').style.height, 10) + 25).toString() + 'px';
      });

      const categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = 'name';
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.inversed = true;

      const dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.dateFormatter.dateFormat = 'yyyy-MM-dd HH:mm';
      dateAxis.renderer.minGridDistance = 50;
      dateAxis.baseInterval = { count: 15, timeUnit: 'minute' };
      dateAxis.max = this.endDate.getTime();
      dateAxis.strictMinMax = true;
      dateAxis.renderer.tooltipLocation = 0;
      dateAxis.clickable = true;

      const series1 = chart.series.push(new am4charts.ColumnSeries());
      series1.columns.template.width = am4core.percent(100);
      series1.columns.template.tooltipText = '{case} | {task}: {date} {startTime} - {finishTime}';
      series1.columns.template.events.on('hit', (ev) => this.dialogSubject.next(ev), this);

      series1.dataFields.openDateX = 'fromDate';
      series1.dataFields.dateX = 'toDate';
      series1.dataFields.categoryY = 'name';
      series1.columns.template.propertyFields.fill = 'color'; // get color from data
      series1.columns.template.propertyFields.stroke = 'color';
      series1.columns.template.strokeOpacity = 1;
      chart.scrollbarX = new am4core.Scrollbar();

      this.chart = chart;
    });
  }

  onDialog() {
    this.dialogSubscription = this.dialogSubject.subscribe(
      ev => {
        this.zone.run(() =>
          this.dialog.open(this.ecpPanelComponent, {
            width: '90%',
            maxWidth: '100vw',
            height: '85%',
            panelClass: 'full-screen-modal',
            data: {
              Date: ev.target.dataItem.dataContext['Date'],
              UserId: ev.target.dataItem.dataContext['UserId'], Task: ev.target.dataItem.dataContext['Task']
            }
          }));
      }
    );
  }

  removeChart() {
    this.zone.runOutsideAngular(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  ngOnDestroy() {
    this.removeChart();
    this.dialogSubscription.unsubscribe();
  }

}

import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { NewGroupComponent } from 'src/app/groups-users/groups/new-group/new-group.component';
import { DateService } from 'src/app/services/date.service';
import { EcpService } from 'src/app/services/ecp.service';
import { AuthService } from 'src/app/services/auth.service';
import { TaskModel } from 'src/app/models-database/task.model';
import { CaseModel } from 'src/app/models-database/case.model';
import { TimePeriodModel } from 'src/app/models-database/time-period.model';
import { DescriptionModel } from 'src/app/models/description.model';
import { Subscription } from 'rxjs';
import { EcpModel } from 'src/app/models/ecp.model';
import { FilterModel } from 'src/app/models/filter.model';
import { RolesModel } from 'src/app/models/roles-model';

@Component({
  selector: 'app-ecp-panel-new',
  templateUrl: './ecp-panel-new.component.html',
  styleUrls: ['./ecp-panel-new.component.scss']
})
export class EcpPanelNewComponent implements OnInit, OnDestroy {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private newEcpDialogRef: MatDialogRef<NewGroupComponent>,
   private dateService: DateService, private ecpService: EcpService,
    private authService: AuthService) { }

  Task: TaskModel = new TaskModel();
  TimePeriods: TimePeriodModel[] = [];
  Descriptions: DescriptionModel[] = [];
  CaseTaskSubscription = new Subscription();
  Ecps: EcpModel[] = [];
  Date: Date = new Date();
  UserId: number;
  TodaysDate: Date;
  allowEdit = false;
  ngOnInit() {
    this.data.UserId === null ? this.UserId = this.authService.decodedToken.nameid : this.UserId = this.data.UserId;
    this.Task.Case = new CaseModel();
    this.ecpService.GetTodaysDate().subscribe(
      date => {this.TodaysDate = this.dateService.converCDateToDate(date); this.checkTimes(3); }
    );
    this.initData();
    this.onTaskCase();
    this.getEcps();
  }

  checkTimes(allowedDays: number) {
    const date = new Date(this.Date);
    const todaysDate = new Date(this.TodaysDate);
    const daysLater = new Date(todaysDate.getFullYear(), todaysDate.getMonth(),
    todaysDate.getDate() + allowedDays, todaysDate.getHours(), todaysDate.getMinutes(), todaysDate.getSeconds());
    const daysEarlier = new Date(todaysDate.getFullYear(), todaysDate.getMonth(),
    todaysDate.getDate() - allowedDays, todaysDate.getHours(), todaysDate.getMinutes(), todaysDate.getSeconds());
    if ((date.getTime() < daysLater.getTime()  && date.getTime()  > daysEarlier.getTime())) {
      this.allowEdit = true;
    } else {
      this.allowEdit = false;
    }
    if (this.authService.roleMatch([RolesModel.Admin, RolesModel.ProjectManager])) {
      this.allowEdit = true;
    }
    // <--- Temporary always allow
    this.allowEdit = true;
    // <--- Temporary always allow
  }

  initData() {
    if (this.data.Date !== null && this.data.Date !== undefined) {
      this.Date = this.data.Date;
    }
    if (this.data.Task !== null && this.data.Task !== undefined) {
      this.Task = this.data.Task;
    }
  }

  NullTimePeriod(timePeriod: TimePeriodModel) {
    timePeriod.AllowEdit = true;
    timePeriod.CollectionId = null;
    timePeriod.FromDataBase = false;
    timePeriod.TaskId = null;
    timePeriod.CaseId = null;
  }

  reportTime() {
    if (this.Ecps.length === 0) {
      const ecp = new EcpModel;
      ecp.UserId = this.UserId;
      ecp.Date =  this.dateService.convertToCDate(this.Date);
      ecp.StartTime = this.dateService.convertToCDate(this.Date);
      ecp.FinishTime = this.dateService.convertToCDate(this.Date);
      this.ecpService.RemoveEcps(ecp).subscribe(
            () => this.newEcpDialogRef.close()
          );
    } else {
     const ecpsForAdd = this.Ecps.slice(0);
     ecpsForAdd.forEach(ecpForAdd => {
       ecpForAdd.Task = null; });
      console.log(this.Ecps);
      this.ecpService.AddEcps(this.Ecps).subscribe(
        ecps => this.newEcpDialogRef.close()
      );
    }
  }

  getEcps() {
    const filter = new FilterModel();
    filter.EcpDate = this.dateService.convertToCDate(this.Date);
    filter.UserId = this.UserId;
    this.ecpService.GetEcps(filter).subscribe(
      ecps => {
        ecps.forEach(ecp => {
          ecp.StartTimeJs = this.dateService.converCDateToDate(ecp.StartTime);
          ecp.FinishTimeJs = this.dateService.converCDateToDate(ecp.FinishTime);
          const time = this.dateService.convertDateToMiutesAndHours(ecp.StartTimeJs, ecp.FinishTimeJs);
          ecp.StartTimeHours = time.StartTimeHours; ecp.StartTimeMinutes = time.StartTimeMinutes;
          ecp.FinishTimeHours = time.FinishTimeHours; ecp.FinishTimeMinutes = time.FinishTimeMinutes;
        });
        this.Ecps = ecps;
      }
    );
  }

  onTaskCase() {
    this.CaseTaskSubscription = this.ecpService.changeCaseTaskSubject.subscribe(
      model => {
        if (model.Case !== null && model.Task !== null) {
          this.Task = model.Task, this.Task.Case = model.Case;
        } else {
          this.Task = new TaskModel();
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.CaseTaskSubscription.unsubscribe();
  }

}

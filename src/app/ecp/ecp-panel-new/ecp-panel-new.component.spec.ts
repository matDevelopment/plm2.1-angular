/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EcpPanelNewComponent } from './ecp-panel-new.component';

describe('EcpPanelNewComponent', () => {
  let component: EcpPanelNewComponent;
  let fixture: ComponentFixture<EcpPanelNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcpPanelNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcpPanelNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

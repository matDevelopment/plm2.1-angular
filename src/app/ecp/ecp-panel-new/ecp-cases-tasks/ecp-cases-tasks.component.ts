import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { TaskService } from 'src/app/services/task.service';
import { CaseService } from 'src/app/services/case.service';
import { TimePeriodService } from 'src/app/services/time-period.service';
import { TaskModel } from 'src/app/models-database/task.model';
import { CaseWithTasksModel } from 'src/app/models/case-with-tasks.model';
import { Subscription } from 'rxjs';
import { FilterModel } from 'src/app/models/filter.model';
import { CaseModel } from 'src/app/models-database/case.model';
import { EcpService } from 'src/app/services/ecp.service';

@Component({
  selector: 'app-ecp-cases-tasks',
  templateUrl: './ecp-cases-tasks.component.html',
  styleUrls: ['./ecp-cases-tasks.component.scss']
})
export class EcpCasesTasksComponent implements OnInit, OnDestroy {

  constructor(private taskService: TaskService, private casesService: CaseService, private timePeriodsService: TimePeriodService,
    private ecpService: EcpService) { }

  @Input() UserId;
  @Input() Task: TaskModel;
  casesWithTasks: CaseWithTasksModel[] = [];
  changeCaseTasksSubscription = new Subscription();
  changeTaskTimeSubscription  = new Subscription();

  ngOnInit() {
    const filter = new FilterModel();
    filter.StatusId = 2;
    filter.TaskUserId = this.UserId;
    this.casesService.GetFilteredCases(filter).subscribe(
      cases => {
        cases.forEach(caseModel => {
          this.casesWithTasks.push(new CaseWithTasksModel(caseModel, [], false));
        });
        this.pickTask();
        this.changeTask(this.Task.Case, this.Task);
      }
    );
    this.onChangeTask();
    this.onTimeEdit();
  }

  getTasks(caseWithTasks) {
    const filter = new FilterModel();
    filter.StatusId = 2;
    filter.UserId = this.UserId;
    filter.CaseId = caseWithTasks.Case.Id;
    this.taskService.GetFilteredTasks(filter).subscribe(
        tasks => {
          caseWithTasks.Tasks = tasks;
          caseWithTasks.expanded = true;
        }
      );
  }

  changeTask(caseModel: CaseModel, task: TaskModel) {
    if (task !== null) {
      this.Task = task;
      this.Task.Case = caseModel;
      this.ecpService.changeCaseTaskSubject.next({ Case: caseModel, Task: task });
    } else {
      this.Task = new TaskModel();
      this.Task.Case = new CaseModel();
      this.ecpService.changeCaseTaskSubject.next({ Case: this.Task.Case, Task: this.Task });
    }
    this.pickTask();
  }

  onChangeTask() {
    this.changeCaseTasksSubscription = this.ecpService.changeCaseTaskSubject.subscribe(
      model => { this.Task = model.Task, this.Task.Case = model.Case; this.pickTask(); }
    );
  }

  pickTask() {
    this.casesWithTasks.forEach(
      caseWithTasks => {
        if (caseWithTasks.Case.Id === this.Task.Case.Id) {
          this.getTasks(caseWithTasks);
        }
      }
    );
  }

  onTimeEdit() {
    this.changeTaskTimeSubscription = this.ecpService.ChangeTaskTimeSubject.subscribe(time => {this.Task.TimeTaken += time;
      this.casesWithTasks.find(c => c.Case.Id === this.Task.CaseId).Tasks.find(t => t.Id === this.Task.Id).TimeTaken += time;
    });
  }

  ngOnDestroy(): void {
    this.changeCaseTasksSubscription.unsubscribe();
    this.changeTaskTimeSubscription.unsubscribe();
  }
}

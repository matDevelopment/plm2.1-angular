import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { EcpModel } from 'src/app/models/ecp.model';
import { faPlus, faEdit, faMinus, faCheck } from '@fortawesome/free-solid-svg-icons';
import { DateService } from 'src/app/services/date.service';
import { TaskModel } from 'src/app/models-database/task.model';
import { CaseModel } from 'src/app/models-database/case.model';
import { EcpService } from 'src/app/services/ecp.service';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';

@Component({
  selector: 'app-create-ecp',
  templateUrl: './create-ecp.component.html',
  styleUrls: ['./create-ecp.component.scss']
})
export class CreateEcpComponent implements OnInit {

  constructor(private dateService: DateService, private ecpService: EcpService, private authService: AuthService) { }

  @Input() Ecps: EcpModel[] = [];
  @Input() Date: Date;
  @Input() Task: TaskModel = new TaskModel();
  @Input() UserId: number;
  @Input() TodaysDate: Date = new Date();
  @Input() allowEdit = false;
  faPlus = faPlus;
  faEdit = faEdit;
  faMinus = faMinus;
  faCheck = faCheck;

  description = '';

  ecpPreview: EcpModel = null;

  pickedEcp: EcpModel = new EcpModel();

  newStartTimeHours = '08';
  newStartTimeMinutes = '00';
  newFinishTimeMinutes = '00';
  newFinishTimeHours = '16';

  hours: string[] = [];
  minutes: string[] = [];
  filteredHours: Promise<string[]>;
  roles = RolesModel;

  ngOnInit() {
    for (let i = 0; i < 24; i++) {
      let hour = i.toString();
      i.toString().length === 1 ? hour = '0' + hour : hour = hour;
      this.hours.push(hour);
    }
    let fifteenMinutes = 0;
    for (let i = 0; i < 4; i++) {
      let minute = fifteenMinutes.toString();
      minute.length === 1 ? minute = '0' + minute : minute = minute;
      this.minutes.push(minute);
      fifteenMinutes += 15;
    }
    this.onTaskCaseIdsChange();
    this.onChangeDescription();
  }

  onChangeDescription() {
    this.ecpService.changeEcpDescriptionSubject.subscribe(
      desc => {
        this.description = desc;
      }
    );
  }

  createEcp(date, newStartTimeHours, newStartTimeMinutes, newFinishTimeHours, newFinishTimeMinutes, caseId, taskId, userId) {
    const startTimeJs = this.dateService.convertHoursMinutesToDate(date, newStartTimeHours, newStartTimeMinutes);
    const finishTimeJs = this.dateService.convertHoursMinutesToDate(date, newFinishTimeHours, newFinishTimeMinutes);
    if (this.allowAdd(startTimeJs, finishTimeJs, this.Ecps) && taskId != null && caseId != null) {
      const startTime = this.dateService.convertHoursMinutesToCDate(date, newStartTimeHours, newStartTimeMinutes);
      const finishTime = this.dateService.convertHoursMinutesToCDate(date, newFinishTimeHours, newFinishTimeMinutes);
      const newEcp = new EcpModel();
      newEcp.UserId = userId;
      newEcp.Date = this.dateService.convertToCDate(this.Date);
      newEcp.Description = this.description;
      newEcp.StartTime = startTime;
      newEcp.FinishTime = finishTime;
      taskId === null ? newEcp.Task = new TaskModel() : newEcp.Task = this.Task;
      newEcp.TaskId = taskId;
      newEcp.CaseId = caseId;
      newEcp.StartTimeHours = newStartTimeHours;
      newEcp.StartTimeMinutes = newStartTimeMinutes;
      newEcp.FinishTimeHours = newFinishTimeHours;
      newEcp.FinishTimeMinutes = newFinishTimeMinutes;
      newEcp.StartTimeJs = startTimeJs;
      newEcp.FinishTimeJs = finishTimeJs;
      this.newStartTimeHours = this.newFinishTimeHours;
      this.newStartTimeMinutes = this.newFinishTimeMinutes;
      this.Ecps.push(newEcp);
      this.description = '';
      this.Ecps.sort(function (a, b) { return a.StartTimeJs.getTime() - b.StartTimeJs.getTime(); });
      this.ecpService.ChangeTaskTimeSubject.next((((newEcp.FinishTimeJs.getTime() - newEcp.StartTimeJs.getTime()) / 1000 ) / 60 / 60));
    }
  }

  newEcp() {
    this.createEcp(this.Date, this.newStartTimeHours,
      this.newStartTimeMinutes, this.newFinishTimeHours, this.newFinishTimeMinutes, this.Task.CaseId, this.Task.Id, this.UserId);
  }

  allowAdd(startTimeJs, finishTimeJs, ecps) {
    if (startTimeJs.getTime() <= finishTimeJs.getTime()) {
      let allowAdd = true;
      ecps.forEach(ecp => {
        if ((ecp.StartTimeJs.getTime() < startTimeJs.getTime() && ecp.FinishTimeJs.getTime() > startTimeJs.getTime()) ||
          (ecp.StartTimeJs.getTime() < finishTimeJs.getTime() && ecp.FinishTimeJs.getTime() > finishTimeJs.getTime()) ||
          (ecp.StartTimeJs.getTime() > startTimeJs.getTime() && ecp.FinishTimeJs.getTime() < finishTimeJs.getTime()) ||
          (startTimeJs.getTime() === finishTimeJs.getTime() ||
          (ecp.StartTimeJs.getTime() === startTimeJs.getTime() && ecp.FinishTimeJs.getTime() === finishTimeJs.getTime()))
          || (ecp.StartTimeJs.getTime() === startTimeJs.getTime() && ecp.FinishTimeJs.getTime() < finishTimeJs.getTime())
          || (ecp.StartTimeJs.getTime() > startTimeJs.getTime() && ecp.FinishTimeJs.getTime() === finishTimeJs.getTime())
        ) {
          allowAdd = false;
        }
      });
      return allowAdd;
    }
  }

  onTaskCaseIdsChange() {
    this.ecpService.changeCaseTaskSubject.subscribe(
      model => {
        this.pickedEcp.TaskId = model.Task.Id;
        this.pickedEcp.CaseId = model.Case.Id;
        this.pickedEcp.Case = model.Case;
        this.pickedEcp.Task = model.Task;
        this.Task = model.Task;
        this.Task.Case = model.Case;
      }
    );
  }

  sendEmptyTaskCase() {
    const task = new TaskModel();
    const caseModel = new CaseModel();
    this.ecpService.changeCaseTaskSubject.next({ Case: caseModel, Task: task });
  }

  editEcp(ecpForEdit: EcpModel) {
    if (this.allowEdit) {
      this.Ecps.forEach(ecp => {
        ecp.AllowEdit = false;
      });
      this.pickedEcp = JSON.parse(JSON.stringify(ecpForEdit));
      if (this.pickedEcp.TaskId === null) {
        this.sendEmptyTaskCase();
      } else {
        this.ecpService.changeCaseTaskSubject.next({ Case: this.pickedEcp.Task.Case, Task: this.pickedEcp.Task });
      }
      this.description = ecpForEdit.Description;
      ecpForEdit.AllowEdit = true;
    }
  }

  submitEdit(ecpForEdit) {
    const EcpsCopy = this.Ecps.slice();
    const indexOfPickedEcp = this.Ecps.indexOf(ecpForEdit);
    EcpsCopy.splice(indexOfPickedEcp, 1);
    const startTimeJs =
      this.dateService.convertHoursMinutesToDate(this.Date, ecpForEdit.StartTimeHours, ecpForEdit.StartTimeMinutes);
    const finishTimeJs =
      this.dateService.convertHoursMinutesToDate(this.Date, ecpForEdit.FinishTimeHours, ecpForEdit.FinishTimeMinutes);
    if (this.allowAdd(startTimeJs, finishTimeJs, EcpsCopy)) {
      this.Ecps.splice(indexOfPickedEcp, 1);
      this.createEcp(this.Date, ecpForEdit.StartTimeHours, ecpForEdit.StartTimeMinutes,
        ecpForEdit.FinishTimeHours, ecpForEdit.FinishTimeMinutes, this.Task.Case.Id, this.Task.Id, this.UserId);
    }
  }

  removeEcp(ecp) {
    this.Ecps.splice(this.Ecps.indexOf(ecp), 1);
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EcpCalendarComponent } from './ecp-calendar/ecp-calendar.component';
import { AuthGuard } from '../guards/auth.guard';
import { RolesModel } from '../models/roles-model';


const appRoutes: Routes = [
    { path: 'ecp-calendar', component: EcpCalendarComponent ,
    data: { roles: [RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader, RolesModel.User]}, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class EcpRoutingModule {  }

import { Component, OnInit, Input, Inject } from '@angular/core';
import { EcpModel } from 'src/app/models/ecp.model';
import { EcpService } from 'src/app/services/ecp.service';
import { FilterModel } from 'src/app/models/filter.model';
import { DateService } from 'src/app/services/date.service';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-ecp-tiles',
  templateUrl: './ecp-tiles.component.html',
  styleUrls: ['./ecp-tiles.component.scss']
})
export class EcpTilesComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private ecpService: EcpService, private dateService: DateService) { }

  Ecps: EcpModel[] = [];
  filter = new FilterModel();
  @Input() Date: Date = new Date();

  ngOnInit() {
    this.Date = this.data.Date;
    this.getEcps();
  }

  getEcps() {
    this.filter.EcpDate = this.dateService.convertToCDate(this.Date);
    this.ecpService.GetEcps(this.filter).subscribe(
      ecps => this.Ecps = ecps
    );
  }

}

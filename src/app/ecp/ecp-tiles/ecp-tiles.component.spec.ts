/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EcpTilesComponent } from './ecp-tiles.component';

describe('EcpTilesComponent', () => {
  let component: EcpTilesComponent;
  let fixture: ComponentFixture<EcpTilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcpTilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcpTilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

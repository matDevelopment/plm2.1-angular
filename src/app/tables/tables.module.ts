import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TablesRoutingModule } from './tables-routing.module';
import { SharedModule } from '../shared/shared.module';
import {
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule, MatSlideToggleModule, MatAutocompleteModule, MatRadioModule, MatTableModule, MatCardModule,
    MatTabsModule
} from '@angular/material';
import { TablesPanelComponent } from './tables-panel/tables-panel.component';
import { TablesService } from '../services/tables.service';
import { TablesFilterPanelComponent } from './tables-filter-panel/tables-filter-panel.component';
import { TablesChartsComponent } from './tables-charts/tables-charts.component';
import { ColumnChartComponent } from './tables-charts/column-chart/column-chart.component';
import { PieChartComponent } from './tables-charts/pie-chart/pie-chart.component';



@NgModule({
    declarations: [
        TablesPanelComponent,
        TablesFilterPanelComponent,
        TablesChartsComponent,
        ColumnChartComponent,
        PieChartComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        FontAwesomeModule,
        TablesRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatSlideToggleModule,
        MatAutocompleteModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCardModule,
    ],
    entryComponents: [TablesFilterPanelComponent],
    providers: [TablesService],
    bootstrap: []
})

export class TablesModule { }

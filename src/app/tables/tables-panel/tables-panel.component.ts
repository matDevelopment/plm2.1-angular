import { FilterService } from './../../services/filter.service';
import { DateService } from 'src/app/services/date.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { TablesService } from 'src/app/services/tables.service';
import { FilterModel } from 'src/app/models/filter.model';
import { MatDialog } from '@angular/material';
import { TablesFilterPanelComponent } from '../tables-filter-panel/tables-filter-panel.component';
import { Subscription } from 'rxjs';
import { faAngleUp, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { faCalendarAlt } from '@fortawesome/free-regular-svg-icons';
import { saveAs } from 'file-saver';
import { FilterPanelComponent } from 'src/app/shared/filter-panel/filter-panel.component';
import { TableDataModel } from 'src/app/models/table-data.model';
import { YearPickerComponent } from 'src/app/shared/year-picker/year-picker.component';
import { QuarterPickerComponent } from 'src/app/shared/quarter-picker/quarter-picker.component';
import { MonthPickerComponent } from 'src/app/shared/month-picker/month-picker.component';
import { EcpService } from 'src/app/services/ecp.service';
import { FilterNamesModel } from 'src/app/models/filterNames.model';
import { AuthService } from 'src/app/services/auth.service';
import { RolesModel } from 'src/app/models/roles-model';



@Component({
  selector: 'app-tables-panel',
  templateUrl: './tables-panel.component.html',
  styleUrls: ['./tables-panel.component.scss']
})
export class TablesPanelComponent implements OnInit, OnDestroy {

  constructor(private tablesService: TablesService, private dialog: MatDialog, private filterService: FilterService,
    private dateService: DateService, private ecpService: EcpService, private authService: AuthService) { }

  startDate: Date = new Date();
  endDate: Date = new Date();

  faAngleUp = faAngleUp;
  faAngleDown = faAngleDown;
  faCalendarAlt = faCalendarAlt;

  tableTypes = ['Projekty -> Użytkownicy', 'Projekty -> Zadania', 'Użytkownicy -> Zadania'];
  dateTypes = ['Okres Czasu', 'Miesiąc', 'Kwartał', 'Rok'];
  pickedType = 'Projekty -> Użytkownicy';
  sortTypes = ['Brak', 'Data Startu', 'Data Końca'];
  tableTypesExpanded = false;
  dateTypesExpanded = false;
  sortTypesExpanded = false;
  pickedTypeNumber = 0;
  pickedDateNumber = 1;
  pickedSortNumber = 0;

  filterNames = new FilterNamesModel();
  subFilterNames = new FilterNamesModel();

  showSubTable = true;

  filterChangeSubscription = new Subscription();

  yearPickerComponent = YearPickerComponent;
  monthPickerComponent = MonthPickerComponent;
  quarterPickerComponent = QuarterPickerComponent;
  tablesFilterPanelComponent = TablesFilterPanelComponent;

  filter = new FilterModel();
  subFilter = new FilterModel();

  tableData: any[] = [];

  casesWithUsersCleaned: any[] = [];
  casesWithTasksCleaned: any[] = [];
  userWithTasksCleaned: any[] = [];

  ngOnInit() {
    const date = new Date();
    this.startDate = new Date(date.getFullYear(), date.getMonth(), 1, 0, 0);
    this.endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0, 0, 0);
    this.filter.StartTime = this.dateService.convertToCdateWithHours(this.startDate);
    this.filter.FinishTime = this.dateService.convertToCdateWithHours(this.endDate);
    if (this.authService.roleMatch([RolesModel.TeamLeader])) {
      this.filter.TeamLeaderId = this.authService.decodedToken.nameid;
    }

    this.getTables();
    this.onFilterChange();
    this.onChangeDate();
  }

  getCasesWithUsersWithEcps() {
    this.tablesService.GetCasesWithUsersWithEcps(this.filter, this.subFilter).subscribe(
      res => {
        this.tableData = [];
        res.forEach(
          response => {
            let caseWithUsers = new TableDataModel(response.Case, response.Users, response.Ecps, 'User', false);
            caseWithUsers.EcpCounted > 0 ? this.tableData.push(caseWithUsers) : caseWithUsers = caseWithUsers;
          }
        );
      }
    );
  }

  getUsersWithTasksWithEcps() {
    this.tablesService.GetUsersWithTasksWithEcps(this.filter, this.subFilter).subscribe(
      res => {
        this.tableData = [];
        res.forEach(
          response => {
            let userWithTasks = new TableDataModel(response.User, response.Tasks, response.Ecps, 'Task', true);
            userWithTasks.EcpCounted > 0 ? this.tableData.push(userWithTasks) : userWithTasks = userWithTasks;
          }
        );
      });
  }

  getCasesWithTasks() {
    this.tablesService.GetCasesWithTasks(this.filter, this.subFilter).subscribe(
      res => {
        this.tableData = [];
        res.forEach(
          response => {
            let casesWithTasksTest = new TableDataModel(response.Case, response.Tasks, response.Ecps, 'Task', false);
            casesWithTasksTest.EcpCounted > 0 ? this.tableData.push(casesWithTasksTest) : casesWithTasksTest = casesWithTasksTest;
          }
        );
      }
    );
  }

  changeDate() {
    this.filter.StartTime = this.dateService.convertToCDate(this.startDate);
    this.filter.FinishTime = this.dateService.convertToCDate(this.endDate);
    this.getTables();
  }

  changeQuater() {

  }

  onChangeDate() {
    this.ecpService.ChangeDateSubject.subscribe(
      dates => {
        this.startDate = dates.StartDate;
        this.endDate = dates.EndDate;
        this.filter.StartTime = this.dateService.convertToCDate(dates.StartDate);
        this.filter.FinishTime = this.dateService.convertToCDate(dates.EndDate);
        this.getTables();
      }
    );
  }

  onFilterChange() {
    this.filterChangeSubscription = this.filterService.filterWithSubfilterSubject.subscribe(
      filters => {
        this.filterService.GetFilterNames(filters.Filter).subscribe(filterNames => this.filterNames = filterNames);
        this.filterService.GetFilterNames(filters.SubFilter).subscribe(filterNames => this.subFilterNames = filterNames);
        this.filter = filters.Filter;
        this.subFilter = filters.SubFilter;
        this.changeDate();
      }
    );
  }

  getTables() {
    switch (this.pickedTypeNumber) {
      case 0:
        this.getCasesWithUsersWithEcps();
        break;
      case 1:
        this.getCasesWithTasks();
        break;
      case 2:
        this.getUsersWithTasksWithEcps();
        break;
    }
  }
  changeDateType(number: number) {
    this.pickedDateNumber = number;
    this.dateTypesExpanded = false;
  }

  changeTableType(tableType: string, index: number) {
    this.pickedType = tableType;
    this.pickedTypeNumber = index;
    this.tableTypesExpanded = false;
    this.getTables();
  }

  exportCurrentTable() {
    this.tablesService.exportJsonToExcelSubject.next();
  }

  openFilterPanel() {
    let showUsers = true;
    let showCases = true;
    let userInFilter = false;
    let showTaskTags = false;
    switch (this.pickedTypeNumber) {
      case 0:
        showUsers = true; showCases = true; userInFilter = false; showTaskTags = true;
        break;
      case 1:
        showUsers = false; showCases = true; userInFilter = false;
        break;
      case 2:
        showUsers = true; showCases = false; userInFilter = true;
        break;
    }
    this.dialog.open(FilterPanelComponent, {
      width: '80%',
      maxWidth: '100vw',
      height: '85vw',
      panelClass: 'full-screen-modal',
      data: { ShowTaskTags: showTaskTags, ShowUsers: showUsers, ShowCases: showCases, Type: this.pickedType }
    });
  }

  openYearPicker() {
    this.dialog.open(this.yearPickerComponent, {
      width: '350px',
      maxWidth: '100vw',
      height: '200px',
      panelClass: 'full-screen-modal',
      data: {}
    });
  }

  openMonthPicker() {
    this.dialog.open(this.monthPickerComponent, {
      width: '350px',
      maxWidth: '100vw',
      height: '200px',
      panelClass: 'full-screen-modal',
      data: {}
    });
  }

  openQuarterPicker() {
    this.dialog.open(this.quarterPickerComponent, {
      width: '350px',
      maxWidth: '100vw',
      height: '200px',
      panelClass: 'full-screen-modal',
      data: {}
    });
  }

  ngOnDestroy(): void {
    this.filterChangeSubscription.unsubscribe();
  }
}

import { FilterService } from './../../services/filter.service';
import { CaseService } from 'src/app/services/case.service';
import { FilterModel } from 'src/app/models/filter.model';
import { TagService } from './../../services/tag.service';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TagModel } from 'src/app/models-database/tag.model';
import { UserService } from 'src/app/services/user.service';
import { GroupService } from 'src/app/services/group.service';
import { StatusModel } from 'src/app/models-database/status.model';

@Component({
  selector: 'app-tables-filter-panel',
  templateUrl: './tables-filter-panel.component.html',
  styleUrls: ['./tables-filter-panel.component.scss']
})
export class TablesFilterPanelComponent implements OnInit, OnDestroy {

  FilterForm;

  constructor(private tagService: TagService, private caseService: CaseService, private filterService: FilterService,
    private filtersDialogRef: MatDialogRef<TablesFilterPanelComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService, private groupService: GroupService) { }

  allowFilterUser = true;
  allowFilterCase = true;
  allowFilterTags = true;

  changeCaseParentIdSubscription = new Subscription();
  changeCaseTagsSubscription = new Subscription();
  changeTaskTagsSubscription = new Subscription();
  changeCaseIdSubscription = new Subscription();
  changeUserIdSubscription = new Subscription();
  changeGroupIdSubscription = new Subscription();

  filter = new FilterModel();
  subFilter = new FilterModel();
  statuses: StatusModel[];

  ngOnInit() {
    this.FilterForm = new FormGroup({
      'user' : new FormControl({value: null, disabled: false}, Validators.required),
      'group' : new FormControl({value: null, disabled: true}, Validators.required),
    });
    this.onChangeCaseParentTagId();
    this.onChangeCaseTagsId();
    this.onChangeTaskTagsId();
    this.onChangeUserId();
    this.onChangeGroupId();
    this.onChangeCaseId();
    this.filterService.GetStatuses().subscribe(
      statuses => this.statuses = statuses
    );
  }

  onChangeCaseParentTagId() {
    this.changeCaseParentIdSubscription = this.tagService.changeCaseParentIdSubject.subscribe(
      tagId => {
        this.filter.CaseTagParentId = tagId;
        this.filter.CaseTagChildId = null;
        this.subFilter.TaskTagMainId = null;
        this.subFilter.TaskTagAccessoryId = null;
        this.subFilter.TaskTagAccessoryMinorId = null;
      }
    );
  }

  onChangeCaseTagsId() {
    this.changeCaseTagsSubscription = this.tagService.addTagsToCaseSubject.subscribe(
      tags => {
        this.filter.CaseTagParentId = tags.ParentTagId;
        this.filter.CaseTagChildId = tags.ChildTagId;
      }
    );
  }

  onChangeTaskTagsId() {
    this.changeTaskTagsSubscription = this.tagService.addTagsToTaskSubject.subscribe(
      tags => {
        this.subFilter.TaskTagMainId = tags.MainTag.Id;
        this.subFilter.TaskTagAccessoryId = tags.AccessoryTag.Id;
        this.subFilter.TaskTagAccessoryMinorId = tags.AccessoryMinorTag.Id;
      }
    );
  }

  onChangeCaseId() {
    this.changeCaseIdSubscription = this.caseService.PickNewCaseSubject.subscribe(
       caseModel => this.filter.CaseId = caseModel.Id
    );
  }

  onChangeUserId() {
    this.changeUserIdSubscription = this.userService.AddUserSubject.subscribe(
      user => {
        this.data.UserInFilter ? this.filter.UserId = user.Id : this.subFilter.UserId = user.Id;
      }
    );
  }

  onChangeGroupId() {
    this.changeGroupIdSubscription = this.groupService.AddGroupSubject.subscribe(
      group => {
        this.data.UserInFilter ? this.filter.GroupId = group.Id : this.subFilter.GroupId = group.Id;
      }
    );
  }

  submitFilters() {
    this.filterService.filterWithSubfilterSubject.next({Filter: this.filter, SubFilter: this.subFilter});
    this.filtersDialogRef.close();
  }

  changeStatus(status: StatusModel) {
    status === null ? this.filter.StatusId = null : this.filter.StatusId = status.Id;
  }

  ngOnDestroy(): void {
    this.changeCaseParentIdSubscription.unsubscribe();
    this.changeCaseTagsSubscription.unsubscribe();
    this.changeTaskTagsSubscription.unsubscribe();
    this.changeCaseIdSubscription.unsubscribe();
    this.changeUserIdSubscription.unsubscribe();
    this.changeGroupIdSubscription.unsubscribe();
  }

  nullTags() {
    this.allowFilterTags = !this.allowFilterTags;
    const emptyTag = new TagModel();
    this.tagService.addTagsToCaseSubject.next({ParentTagId: null, ChildTagId: null});
    this.tagService.addTagsToTaskSubject.next({MainTag: emptyTag, AccessoryTag: emptyTag, AccessoryMinorTag: emptyTag});
  }

  nullCase() {
    this.allowFilterCase = !this.allowFilterCase;
  }


}

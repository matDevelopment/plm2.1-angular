import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolesModel } from '../models/roles-model';
import { AuthGuard } from '../guards/auth.guard';
import { TablesPanelComponent } from './tables-panel/tables-panel.component';


const appRoutes: Routes = [
    { path: 'tables',  component: TablesPanelComponent,
    data: { roles: [RolesModel.Admin, RolesModel.ProjectManager, RolesModel.TeamLeader]}, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class TablesRoutingModule {  }

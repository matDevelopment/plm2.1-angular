import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { TablesService } from 'src/app/services/tables.service';
import { Subscription } from 'rxjs';
import { TableDataModel } from 'src/app/models/table-data.model';

@Component({
  selector: 'app-tables-charts',
  templateUrl: './tables-charts.component.html',
  styleUrls: ['./tables-charts.component.scss']
})
export class TablesChartsComponent implements OnInit, OnChanges {

  constructor(private tablesService: TablesService) { }

  chartSubscription = new Subscription();
  @Input() TableData: any[] = [];
  chartStatus = true;
  pickedSubTableData = [];
  pickedTableData = [];

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges): void {
    this.changeTableData();
  }

  changeTableData() {
    this.pickedTableData = [];
    this.TableData.forEach(element => {
      let name = '';
      if (element.Order != null) {
        name = element.Order + ' - ' + element.Name;
      } else {
        name = element.Name;
      }
      const bar = this.pickedTableData.find(td => td.label === name);
      if (bar != null) {
        bar.data += element.EcpCounted;
      } else {
        this.pickedTableData.push({ label: name, data: element.EcpCounted });
      }
    });
  }

  changeSubTableData(tableData) {
    this.pickedSubTableData = [];
    this.tablesService.loadChartSubject.next(tableData);
    tableData.SubModels.forEach(element => {
      let name = '';
      if (element.Order != null) {
        name = element.Order + ' - ' + element.Name;
      } else {
        name = element.Name;
      }
      const bar = this.pickedSubTableData.find(td => td.label === name);
      if (bar != null) {
        bar.data += element.EcpCounted;
      } else {
        this.pickedSubTableData.push({ label: name, data: element.EcpCounted });
      }
    });
  }

}

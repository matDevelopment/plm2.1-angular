import { Component, OnInit, OnDestroy } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import { TablesService } from 'src/app/services/tables.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit, OnDestroy {

  constructor(private tablesService: TablesService) { }
  chartSubscription = new Subscription();
  chart: any;

  ngOnInit() {
    this.chartSubscription = this.tablesService.loadChartSubject.subscribe(
      data => {
        if (this.chart !== undefined) {
          this.chart.dispose();
        }
        setTimeout(() => { this.initCharts(data); }, 800);
      }
    );
  }

  initCharts(data) {
    this.chart = am4core.create('pieChart', am4charts.PieChart);
    data.SubModels.forEach(subModel => {
      if (subModel.EcpCounted !== 0) {
        this.chart.data.push({ 'project': subModel.Name, 'time': subModel.EcpCounted });
      }
    });
    const pieSeries = this.chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = 'time';
    pieSeries.dataFields.category = 'project';
    pieSeries.slices.template.stroke = am4core.color('#fff');
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;

    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;
  }

  ngOnDestroy(): void {
    if (this.chart !== undefined && this.chart !== null) {
      this.chart.dispose();
    }
    this.chartSubscription.unsubscribe();
  }
}

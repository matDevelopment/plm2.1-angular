import { Component, OnInit, OnDestroy, OnChanges, ElementRef, ViewChild, AfterViewInit, Input, SimpleChanges } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { TablesService } from 'src/app/services/tables.service';
import { Subscription } from 'rxjs';
import { Chart } from 'chart.js';
import { height } from '@amcharts/amcharts4/.internal/core/utils/Utils';

@Component({
  selector: 'app-column-chart',
  templateUrl: './column-chart.component.html',
  styleUrls: ['./column-chart.component.scss']
})
export class ColumnChartComponent implements OnInit, OnDestroy, OnChanges {


  constructor() { }
  @Input() LabelName = '';
  @Input() BarData = [];
  Labels = [];
  Datas = [];
  @Input() Height = 400;
  @Input() BarColor = '#3e95cd';
  @Input() BarThickness = 25;

  @ViewChild('canvas') canvasRef: ElementRef;
  ctx: Chart;
  columnChart: any;

  ngOnInit() {
    this.initCharts();
  }

  initCharts() {
    this.Labels = [];
    this.Datas = [];
    this.BarData.forEach(bar => {
      this.Labels.push(bar.label);
      this.Datas.push(bar.data);
    });
    this.ctx = this.canvasRef.nativeElement.getContext('2d');
    this.columnChart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        labels: this.Labels,
        datasets: [
          {
            label: this.LabelName,
            backgroundColor: this.BarColor,
            data: this.Datas
          }
        ]
      },
      options: {
        scales: {
          xAxes: [{
            barPercentage: 0.8,
            barThickness: this.BarThickness,
            maxBarThickness: this.BarThickness,
            minBarLength: 1,
            gridLines: {
              offsetGridLines: true
            }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
            }
          }],
        },
        responsive: true,
        maintainAspectRatio: false,
      }
    }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.ctx !== undefined) {
      this.columnChart.destroy();
    }
    this.initCharts();
  }

  ngOnDestroy(): void {
    this.columnChart.destroy();
  }
}

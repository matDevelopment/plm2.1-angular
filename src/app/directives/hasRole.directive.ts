import { Directive, Input, ViewContainerRef, TemplateRef, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective implements OnInit {

  @Input() appHasRole: string[];
  isVisible = false;

  constructor(private viewContainerRef: ViewContainerRef,
    private teamplateRef: TemplateRef<any>,
    private authService: AuthService) { }

    ngOnInit(): void {
      if (this.authService.decodedToken !== null) {
        const userRoles =  this.authService.decodedToken.role as Array<string>;

        if (!userRoles) {
          this.viewContainerRef.clear();
        }

        if (this.authService.roleMatch(this.appHasRole)) {
          if (!this.isVisible) {
            this.isVisible = true;
            this.viewContainerRef.createEmbeddedView(this.teamplateRef);
          } else {
            this.isVisible = false;
            this.viewContainerRef.clear();
          }
        }
      } else {
        this.isVisible = false;
        this.viewContainerRef.clear();
      }
    }

}

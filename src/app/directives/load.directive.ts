import { Directive, ElementRef, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appLoad]'
})
export class LoadDirective implements AfterViewInit {

  element: ElementRef;

  constructor(el: ElementRef) {
    this.element = el;
  }

  ngAfterViewInit(): void {
    this.element.nativeElement.innerHTML = 'Obciążenie: ' + (parseInt(this.element.nativeElement.innerHTML, 10) * 100).toString() + '%';
  }

}
